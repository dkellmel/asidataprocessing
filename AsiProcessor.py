
import os
import sys
import time
import shutil
import signal
# import stream
import logging
import argparse
# import numpy as np
# import subprocess

# import lib.lob as LOB
from lib.diskUtility import DiskUtility
from lib.aisUtility import compute_checksum
from lib.configUtility import Configuration
# from lib.spinner import SpinnerAsiAisProcessing
# import lib.asidfengine.AsiDfEngine.AsiDfE as AsiDfE

# from ProcessingLib.AIS import DemodAisProcessor as DemodAis
import lib.read_snapshot_files as ReadSnapshotFile

from tabulate import tabulate
# from ProcessingLib.AsiDfEngine import AsiDfE
from ProcessingLib.AIS.AisProcessor import AisProcessor

# import ProcessingLib.AIS.DecodeNmeaSentenceProcessor as DecodeNmeaSentences
from ProcessingLib.Protobuf.Sv3ProtobufProcessor import Sv3ProtobufProcessor
from ProcessingLib.Protobuf.AsiProcessorProtobufProcessor import AsiProcessorProtobufProcessor

# import signal processors
from ProcessingLib.DTV.DtvProcessor import DtvProcessor
from ProcessingLib.ARSR.ArsrProcessor import ArsrProcessor
from ProcessingLib.MARNAV.MarnavProcessor import MarnavProcessor
from ProcessingLib.AsiDfEngine.DfEngineProcessor import DfEngineProcessor

from argparse import Namespace

'''
bad NMEA sentence encountered:
!AIVDM,2,2,0,,8888888880,2*5D
'''

_logger = logging.getLogger('AsiProcessor')


class AsiProcessor:
    __version__ = '1.0.0'

    ais_config = {}
    marnav_config = {}
    arsr_config = {}
    dtv_config = {}
    iridium_config = {}
    dfengine_config = {}

    delete_after_processing = True
    show_results = False
    run_from_file_list = False
    use_fixed_gps = False

    number_of_files_processed = 0
    number_ais_signals = 0
    number_marnav_signals = 0
    number_arsr_signals = 0
    number_dtv_signals = 0

    processing_times = {'read_snapshot_file': 0.0,
                        'gnuradio': 0.0,
                        'gnuais': 0.0,
                        'decode_nmea_sentences': 0.0,
                        'DFengine': 0.0
                        }

    execDir = os.getcwd() + '/'

    target_type = None
    protobuf_processor = None
    protobuf_file = None
    debug = None
    info = None
    result_number = 0

    sensor_results = {
                      'sensor_lat': 0,
                      'sensor_lon': 0,
                      'sensor_yaw': 0,
                      'sensor_pitch': 0,
                      'sensor_roll': 0
                     }

    dfengine_results = {
                        'target_snr': [],
                        'target_lob': [],
                        'target_pwr': []
                       }

    ReadSnapshotFile_namespace = None
    results_channel_list = None
    results_time_windows_list = None
    asi_protobuf_processor = None
    sv3_protobuf_processor = None

    start_timestr = time.strftime("%Y%m%d-%H%M%S")

    def __init__(self):

        # self.sv3_report_protocol_types = self.init_sv3_report_protocol()
        # for key in self.sv3_report_protocol_types.keys():
        #     print('{}: {}'.format(key, self.sv3_report_protocol_types[key]))

        # ***********************************************************************************
        # set signal for CTRL-C and uncaught errors
        # ***********************************************************************************
        signal.signal(signal.SIGINT, self.exit_gracefully)

        print('\n' + 31 * '*' + ' AsiProcessor ' + 31 * '*' + '\n')
        print('Execution Folder: {}'.format(self.execDir))

        self.gps_latitude = None
        self.gps_longitude = None

        self.read_config()

        if self.ais_config['process_ais']:
            print('Importing: from ProcessingLib.AIS.AisProcessor import AisProcessor')
            from ProcessingLib.AIS.AisProcessor import AisProcessor

        self.show_processing_time = False
        # print('************************************************************')
        print(80 * '=')

        self.diskUtility = DiskUtility()

        self.number_signals = {}
        for target_type in self.target_types:
            self.number_signals[target_type] = 0

    def init(self):
        if self.debug:
            logging.basicConfig(level=logging.DEBUG)
            _logger.debug('Debug logging set.')

        if self.info:
            logging.basicConfig(level=logging.INFO)
            _logger.info('Info logging set.')

        if self.info or self.debug:
            print(80 * '=')
            print(30 * '*' + 'asi_processor_config' + 30 * '*')
        _logger.info('AsiProcessing started.')

        _logger.info('Test Run: {}'.format(self.test_run))
        _logger.info('Target Types: {}'.format(self.target_types))
        _logger.info('Write the Results to a File: {}'.format(self.write_results_to_file))
        _logger.info('Folder to check: "{}"'.format(self.folder_to_check))
        _logger.info('Extension to check: "*{}"'.format(self.data_files_ext))
        _logger.info('Revisit time: {0:2d} seconds'.format(self.revisit_time))
        _logger.info('Folder for results: "{}"'.format(self.folder_for_results))
        _logger.info('File for results: "{}"'.format(self.file_for_results))
        _logger.info('Debug: {}'.format(self.debug))
        _logger.info('Show Processing Time output: {}'.format(self.show_processing_time))

        _logger.info('Version: {}'.format(self.software_version))
        _logger.info('Sensor ID: {}'.format(self.sensor_id))

        _logger.info('Dfengine Configuration Path: {}'.format(self.dfengine_config['configuration_path']))
        _logger.info('Dfengine Configuration File: {}'.format(self.dfengine_config['configuration_file']))

        _logger.info('Use Fixed GPS: {}'.format(self.use_fixed_gps))
        _logger.info('GPS Latitude: {}'.format(self.gps_latitude))
        _logger.info('GPS Longitude: {}'.format(self.gps_longitude))

        _logger.info('Maximum Iridium Bytes: {}'.format(self.iridium_config['max_bytes']))
        _logger.info('Iridium Time Interval: {}'.format(self.iridium_config['time_interval']))
        _logger.info('Iridium Buffer Padding: {}'.format(self.iridium_config['buffer_padding']))
        _logger.info('Iridium Create Protobuf File: {}'.format(self.iridium_config['create_protobuf_file']))

        _logger.info('AIS Freqs: {}'.format(self.ais_config['freqs']))
        _logger.info('Process AIS: {}'.format(self.ais_config['process_ais']))

        _logger.info('MARNAV Freqs: {}'.format(self.marnav_config['freqs']))
        _logger.info('MARNAV Min Number of Pulses: {}'.format(self.marnav_config['min_number_of_pulses']))
        _logger.info('MARNAV Noise Threshold Multiplier: {}'.format(self.marnav_config['noise_threshold_multiplier']))
        _logger.info('MARNAV Time Window Pad: {}'.format(self.marnav_config['time_window_pad']))
        _logger.info('Process MARNAV: {}'.format(self.marnav_config['process_marnav']))

        _logger.info('ARSR Freqs: {}'.format(self.arsr_config['freqs']))
        _logger.info('Process ARSR: {}'.format(self.arsr_config['process_arsr']))

        _logger.info('DTV Freqs: {}'.format(self.dtv_config['freqs']))
        _logger.info('Process DTV: {}'.format(self.dtv_config['process_dtv']))

        # _logger.info('\n')

        if self.write_results_to_file:
            # results_filename = self.file_for_results + '_' + self.start_timestr
            # results_filename = self.file_for_results + '.txt'
            # self.full_path_filename = os.path.join(self.folder_for_results, results_filename)
            # self.results_file = open(os.path.join(self.folder_for_results, results_filename), 'w')
            if not self.test_run:
                asi_protobuf_file_name = 'data/AsiProcessorResults/AsiReports' + '_' + self.start_timestr + '.bin'
            else:
                asi_protobuf_file_name = 'data/AsiProcessorResults/AsiReports.bin'
            self.asi_protobuf_full_path_file = os.path.join(self.execDir, asi_protobuf_file_name)
            self.asi_protobuf_processor = AsiProcessorProtobufProcessor(software_version=self.software_version,
                                                                        sensor_id=self.sensor_id,
                                                                        outf=self.asi_protobuf_full_path_file
                                                                        )

        if self.iridium_config['create_protobuf_file']:
            if not self.test_run:
                protobuf_file_name = 'data/protobuf/Sv3Reports' + '_' + self.start_timestr + '.bin'
            else:
                protobuf_file_name = 'data/protobuf/Sv3Reports.bin'
            self.protobuf_file = os.path.join(self.execDir, protobuf_file_name)
            self.sv3_protobuf_processor = Sv3ProtobufProcessor(version=self.software_version,
                                                               sensor_id=self.sensor_id,
                                                               outf=self.protobuf_file
                                                               )

    def set_debug(self, debug):
        self.debug = debug

    def set_info(self, info):
        self.info = info

    def set_folder_to_check(self, folder_to_check):
        self.folder_to_check = folder_to_check

    def read_config(self):
        configFile = 'config/asi_processor_config'
        config = Configuration()
        config.setConfigFile(configFile)
        config.getConfig()
        sections = config.getSections()
        #       print('sections: {}'.format(sections))

        # test run
        self.test_run = config.getBool(sections[0], 'test_run')

        # target types
        self.target_types = config.getList(sections[0], 'target_types')

        # write results to file
        self.write_results_to_file = config.getBool(sections[0], 'write_results_to_file')

        # folder to check for files
        # asi data files should be dumped in this folder for processing
        # note they will be deleted once processed
        self.folder_to_check = config.getString(sections[0], 'folder_to_check')

        # extension of files to read
        # the extension of the data files
        # all other files are ignored
        self.data_files_ext = config.getString(sections[0], 'data_files_ext')
        if not self.data_files_ext.startswith('.'):
            self.data_files_ext = '.' + self.data_files_ext

        # wait time between folder checks
        # how long a time to sleep before re-checking the data folder
        self.revisit_time = config.getInt(sections[0], 'revisit_time')

        # folder for results
        # the folder that will contain the results file
        self.folder_for_results = config.getString(sections[0], 'folder_for_results')

        # file for results
        # the prefix for the results file
        # a date-time stamp will be appended
        self.file_for_results = config.getString(sections[0], 'file_for_results')

        # version
        self.software_version = config.getInt(sections[0], 'software_version')

        # sensor_id
        self.sensor_id = config.getInt(sections[0], 'sensor_id')

        # dfengine configuration
        self.dfengine_config['configuration_path'] = config.getString('DFengine', 'configuration_path')
        if self.dfengine_config['configuration_path'].endswith('/'):
            self.dfengine_config['configuration_path'] = self.dfengine_config['configuration_path'][:-1]
        if 'opt' not in self.dfengine_config['configuration_path']:
            self.dfengine_config['configuration_path'] = self.execDir + \
                                                         self.dfengine_config['configuration_path']
        self.dfengine_config['configuration_file'] = config.getString('DFengine', 'configuration_file')

        # gps
        # a preset lat/lon if known
        # this can be used to process specific files not containing valid gps data
        # example:
        # latitude = [32, 52, 1.0, 'n']
        # longitude = [117, 15, 26.9, 'w']
        try:
            self.use_fixed_gps = config.getBool('gps', 'use_fixed_gps')
            _latitude = config.getList('gps', 'latitude')
            self.gps_latitude = _latitude[0] + _latitude[1] / 60 + _latitude[2] / 3600
            if _latitude[3].upper() == 'S':
                self.gps_latitude = -self.gps_latitude
            _longitude = config.getList('gps', 'longitude')
            self.gps_longitude = _longitude[0] + _longitude[1] / 60 + _longitude[2] / 3600
            if _longitude[3].upper() == 'W':
                self.gps_longitude = -self.gps_longitude
        except:
            pass

        # iridium configuration
        # max_bytes - the maximum number of bytes allowed - usually 1960
        # time_interval - time between transmissions
        # buffer_padding - the number of bytes remaining in the protobuf buffer when considered full
        # create_protobuf_file - true/false (lc only)
        self.iridium_config['max_bytes'] = config.getInt('iridium', 'max_bytes')
        self.iridium_config['time_interval'] = config.getInt('iridium', 'time_interval')
        self.iridium_config['buffer_padding'] = config.getInt('iridium', 'buffer_padding')
        self.iridium_config['create_protobuf_file'] = config.getBool('iridium', 'create_protobuf_file')

        # AIS definition
        self.ais_config['name'] = config.getString('AIS', 'name')
        freqs = config.getList('AIS', 'freqs')
        self.ais_config['freqs'] = [int(freqs[0]) * 1e6, int(freqs[1]) * 1e6]
        self.ais_config['process_ais'] = config.getBool('AIS', 'process_ais')

        # MARNAV definition
        self.marnav_config['name'] = config.getString('MARNAV', 'name')
        freqs = config.getList('MARNAV', 'freqs')
        self.marnav_config['freqs'] = [[int(freqs[0][0]) * 1e9, int(freqs[0][1]) * 1e9],
                                      [int(freqs[1][0]) * 1e9, int(freqs[1][1]) * 1e9]]
        self.marnav_config['min_number_of_pulses'] = config.getInt('MARNAV', 'min_number_of_pulses')
        self.marnav_config['noise_threshold_multiplier'] = config.getFloat('MARNAV', 'noise_threshold_multiplier')
        self.marnav_config['pri_min'] = config.getFloat('MARNAV', 'pri_min')
        self.marnav_config['pri_max'] = config.getFloat('MARNAV', 'pri_max')
        self.marnav_config['time_window_pad'] = config.getFloat('MARNAV', 'time_window_pad')
        self.marnav_config['process_marnav'] = config.getBool('MARNAV', 'process_marnav')

        # ARSR definition
        self.arsr_config['name'] = config.getString('ARSR', 'name')
        freqs = config.getList('ARSR', 'freqs')
        self.arsr_config['freqs'] = [int(freqs[0]) * 1e9, int(freqs[1]) * 1e9]
        self.arsr_config['process_arsr'] = config.getBool('ARSR', 'process_arsr')

        # DTV definition
        self.dtv_config['name'] = config.getString('DTV', 'name')
        freqs = config.getList('DTV', 'freqs')
        self.dtv_config['freqs'] = [int(freqs[0]) * 1e9, int(freqs[1]) * 1e9]
        self.dtv_config['process_dtv'] = config.getBool('DTV', 'process_dtv')

    # return the size of a file on disk
    # this is used to determine if a file is complete during transfer
    def check_file_size(self, _file):
        file_size = 0
        while True:
            file_info = os.stat(_file)
            if file_info.st_size == 0 or file_info.st_size > file_size:
                file_size = file_info.st_size
                time.sleep(1)
            else:
                break
        return file_info.st_size  # file size in bytes

    def start(self):
        loop = True

        self.result_number = 0

        self.processing_times['read_snapshot_file'] = 0.0
        self.processing_times['gnuradio'] = 0.0
        self.processing_times['gnuais'] = 0.0
        self.processing_times['decode_nmea_sentences'] = 0.0
        self.processing_times['DFengine'] = 0.0

        while loop:
            if not self.run_from_file_list:
                files_list = self.diskUtility.get_full_path_file_list(self.folder_to_check,
                                                                      self.data_files_ext)
            else:
                list_file = os.path.join(self.execDir, 'data/gps_results.txt')
                with open(list_file, 'r') as f:
                    # file_list = f.read().split('\n')
                    files_list = sorted(f.read().split('\n'))
                    # files_list = list(filter(('').__ne__,file_list))
                    files_list = list(filter(lambda a: a != '', files_list))

            # print('Waiting for files...\tCTRL-c to exit', end=' ')
            _logger.info('Waiting for files...\tCTRL-c to exit')
            if not self.info:
                print('Waiting for files...\tCTRL-c to exit')
            sys.stdout.flush()

            if self.use_fixed_gps:
                self.sensor_results['sensor_lat'] = self.gps_latitude
                self.sensor_results['sensor_lon'] = self.gps_longitude

            # check for new files every revisit_time seconds
            while len(files_list) == 0 and not self.run_from_file_list:
                time.sleep(self.revisit_time)
                files_list = self.diskUtility.get_full_path_file_list(self.folder_to_check,
                                                                      self.data_files_ext)

            nread = 1
            for file_to_read in sorted(files_list):
                self.number_of_files_processed += 1
                # _filename = os.path.basename(file_to_read)
                head, _filename = os.path.split(file_to_read)

                # if not self.info and not self.debug:
                mbs = ['B', 'KB', 'MB', 'GB', '?']
                fsize = self.check_file_size(file_to_read)
                for _ in range(1, 5):
                    fsize /= 1000
                    if fsize < 1:
                        fsize *= 1000
                        unit = mbs[_ - 1]
                        break

                datafile_dict = self.datafile_processor(file_to_read)

                print('\nProcessing file {} of {} '.format(nread, len(files_list)))
                print(80 * '*')
                print('File Size: {:.1f} {}  {:,} samples/channel  {} channels'.
                      format(fsize, unit, len(datafile_dict['iq_data'][0]), len(datafile_dict['iq_data'])))
                print('File Directory: {}{}/'.format(self.execDir, head))
                print('File Name: {}'.format(_filename))

                nread += 1
                # print()

                radio_state = datafile_dict['radio_state']

                # print('\n***** start radio state *****')
                # for key in radio_state.keys():
                #     print('{}: {}'.format(key, radio_state[key]))
                # print('***** end radio state *****')

                data_nav = datafile_dict['data_nav']
                self.sensor_results['sys_time'] = data_nav['GPS']['systime']
                if not self.use_fixed_gps:
                    self.sensor_results['sensor_lat'] = data_nav['GPS']['latitude']
                    self.sensor_results['sensor_lon'] = data_nav['GPS']['longitude']
                    # self.sensor_results['sensor_pitch'] = int(data_nav['COMPASS']['pitch'])
                    # self.sensor_results['sensor_roll'] = int(data_nav['COMPASS']['roll'])
                    # self.sensor_results['sensor_yaw'] = int(data_nav['COMPASS']['yaw'])
                    self.sensor_results['sensor_pitch'] = data_nav['COMPASS']['pitch']
                    self.sensor_results['sensor_roll'] = data_nav['COMPASS']['roll']
                    self.sensor_results['sensor_yaw'] = data_nav['COMPASS']['yaw']

                # print('\n***** start datanav *****')
                # for key1 in data_nav.keys():
                #     # for key2 in data_nav[key1].keys():
                #     #     print('{}: {}'.format(key2, data_nav[key1][key2]))
                #     if isinstance(data_nav[key1], dict):
                #         print('\n' + 20 * '*' + str(key1) + 20 * '*')
                #         # print('\n' + 20 * '*' + key1 + 20 * '*')
                #         for key2 in data_nav[key1].keys():
                #             print('{}: {}'.format(key2, data_nav[key1][key2]))
                #     else:
                #         print('{}: {}'.format(key1, data_nav[key1]))
                # print('***** end data nav *****')

                self.target_type = None

                f00 = self.marnav_config['freqs'][0][0]
                f01 = self.marnav_config['freqs'][0][1]
                f10 = self.marnav_config['freqs'][1][0]
                f11 = self.marnav_config['freqs'][1][1]

                # self.set_snapshot_report(_filename, radio_state['data_rate'])
                # self.asi_protobuf_processor.snap_shot_report.asi_file_name = _filename
                # self.asi_protobuf_processor.snap_shot_report.sample_rate = radio_state['data_rate']
                # self.asi_protobuf_processor.snap_shot_report.sensor_lat = self.sensor_results['sensor_lat']
                # self.asi_protobuf_processor.snap_shot_report.sensor_lon = self.sensor_results['sensor_lon']
                # self.asi_protobuf_processor.snap_shot_report.sensor_pitch = self.sensor_results['sensor_pitch']
                # self.asi_protobuf_processor.snap_shot_report.sensor_roll = self.sensor_results['sensor_roll']
                # self.asi_protobuf_processor.snap_shot_report.sensor_yaw = self.sensor_results['sensor_yaw']
                # self.asi_protobuf_processor.snap_shot_report.utc_time = self.sensor_results['utc_time']

                # AIS

                if radio_state['frequency'][0] == self.ais_config['freqs'][0]:
                    self.target_type = 'AIS'
                    print('Target Type: {}'.format(self.target_type))
                    if self.ais_config['process_ais']:
                        st = time.time()
                        ais_processor = AisProcessor(_filename, datafile_dict)
                        ais_processor.setDFengineConfigurationPath(self.dfengine_config['configuration_path'])
                        ais_processor.setDFengineConfigurationFile(self.dfengine_config['configuration_file'])
                        ais_processor.setFixedGps(self.gps_latitude, self.gps_longitude)
                        ais_processor.setArgs(self.execDir, self.info, self.debug, self.use_fixed_gps)
                        processor_results = ais_processor.process_ais()
                        processor_results['target_type'] = self.target_type
                        et = time.time()
                        print('Total ProcessingTime: {0:3.1f} seconds'.format(et-st))

                # MARNAV

                elif f10 <= radio_state['frequency'][0] <= f11 or f00 <= radio_state['frequency'][0] <= f01:
                    self.target_type = 'MARNAV'
                    print('Target Type: {}'.format(self.target_type))
                    if self.marnav_config['process_marnav']:
                        st = time.time()
                        marnav_processor = MarnavProcessor()
                        marnav_processor.set_pri_min_max(
                            self.marnav_config['pri_min'], self.marnav_config['pri_max'])
                        marnav_processor.set_noise_threshold_multiplier(
                            self.marnav_config['noise_threshold_multiplier'])
                        # marnav_processor.set_min_number_of_pulse_widths(
                        #     self.marnav_config['min_number_of_pulse_widths'])
                        marnav_processor.set_min_number_of_pulses(
                            self.marnav_config['min_number_of_pulses'])
                        marnav_processor.set_datafile_dict(datafile_dict)
                        marnav_processor.init()
                        processor_results = marnav_processor.process_marnav()

                        del marnav_processor

                        # print('\n{}'.format(radio_state))
                        df_engine_processor = DfEngineProcessor(
                            configuration_path=self.dfengine_config['configuration_path'],
                            configuration_file=self.dfengine_config['configuration_file'])
                        target_freq = radio_state['frequency'][0]
                        bw = radio_state['bandwidth']
                        dfengine_results = {}
                        # process each ASI channel
                        for c in range(0, len(processor_results)):
                            time_windows = processor_results[c]['time_windows']
                            if not time_windows:
                                continue  # no valid signal in this channel
                            else:
                                dfengine_results[c] = {}
                                dfengine_results[c]['target_snr'] = []
                                dfengine_results[c]['target_pwr'] = []
                                dfengine_results[c]['target_lob'] = []
                                dfengine_results[c]['signal_start_time'] = []
                                dfengine_results[c]['signal_end_time'] = []
                                for j in range(0, len(time_windows)):
                                    index_start = int(radio_state['data_rate'] * time_windows[j][0])
                                    index_stop = int(radio_state['data_rate'] * time_windows[j][1])
                                    dfengine_results[c]['signal_start_time'].append(time_windows[j][0])
                                    dfengine_results[c]['signal_end_time'].append(time_windows[j][1])
                                    df_res = df_engine_processor.process_df(target_freq, bw,
                                                                            index_start, index_stop,
                                                                            datafile_dict['iq_data'],
                                                                            radio_state, data_nav)
                                    dfengine_results[c]['target_snr'].append(df_res[0]['snr'])
                                    dfengine_results[c]['target_pwr'].append(df_res[0]['instant_power'])
                                    dfengine_results[c]['target_lob'].append(df_res[0]['LoB'][0])

                        del df_engine_processor

                        processor_results['target_type'] = self.target_type
                        print('Processing complete.')
                        et = time.time()
                        print('Total ProcessingTime: {0:4.2f} seconds'.format(et-st))

                # ARSR

                elif self.arsr_config['freqs'][0] <= radio_state['frequency'][0] <= self.arsr_config['freqs'][1]:
                    self.target_type = 'ARSR'
                    print('Target Type: {}'.format(self.target_type))

                    if self.arsr_config['process_arsr']:
                        st = time.time()
                        arsr_processor = ArsrProcessor()
                        print('Processing complete.')
                        et = time.time()
                        print('Total ProcessingTime: {0:3.1f} seconds'.format(et-st))
                        # continue

                # DVT

                elif self.dtv_config['process_dtv'] and \
                        self.dtv_config['freqs'][0] <= radio_state['frequency'][0] <= self.dtv_config['freqs'][1]:
                    self.target_type = 'DTV'
                    print('Target Type: {}'.format(self.target_type))

                    if self.dtv_config['process_dtv']:
                        st = time.time()
                        dtv_processor = DtvProcessor()
                        print('Processing complete.')
                        et = time.time()
                        print('Total ProcessingTime: {0:3.1f} seconds'.format(et-st))
                        # continue

                # OTHER ?????

                else:
                    st = time.time()
                    self.target_type = 'OTHER'
                    print('Target Type: {}'.format(self.target_type))
                    print('Processing complete.')
                    et = time.time()
                    print('Total ProcessingTime: {0:3.1f} seconds'.format(et-st))
                    # continue

                # process the results ******************************

                self.results_channel_list = []
                self.results_time_windows_list = []

                if self.target_type == 'AIS' and self.ais_config['process_ais']:
                    # self.results_channel_list = []
                    # self.results_time_windows_list = []
                    for chan in processor_results.keys():
                        for ais_chan in ['A', 'B']:
                            # print(chan, ais_chan)
                            try:
                                if not len(processor_results[chan][ais_chan]) == 0:
                                    results = processor_results[chan][ais_chan]
                                    # self.results_processor(results)

                                    if self.iridium_config['create_protobuf_file']:
                                        number_of_bytes = self.sv3_protobuf_processor.add_sv3_report(results)

                                    if self.write_results_to_file:
                                        number_of_asi_report_bytes = self.asi_protobuf_processor.add_sv3_report(results)

                                    self.results_channel_list.append(chan)

                                    self.results_processor(results)
                            except Exception as e:
                                # print('\nself.target_type = AIS ERROR\n', e)
                                pass

                elif self.target_type == 'MARNAV' and self.marnav_config['process_marnav']:
                    for chan in range(0, datafile_dict['number_of_channels']):
                        results = {'data_rate': datafile_dict['data_rate'], 'target_type': self.target_type}
                        # results.update(self.sensor_results)
                        if len(processor_results[chan]['pris']) == 0:
                            continue
                        self.results_channel_list.append(chan)

                        results['marnav_pri'] = processor_results[chan]['pris']
                        results['marnav_pri_pwr'] = processor_results[chan]['pri_powers']
                        results['target_pwr'] = dfengine_results[chan]['target_pwr']
                        results['target_snr'] = dfengine_results[chan]['target_snr']
                        results['target_lob'] = dfengine_results[chan]['target_lob']
                        results['signal_start_time'] = dfengine_results[chan]['signal_start_time']
                        # print(results['signal_start_time'])
                        results['signal_end_time'] = dfengine_results[chan]['signal_end_time']

                        if self.iridium_config['create_protobuf_file']:
                            number_of_bytes = self.sv3_protobuf_processor.add_sv3_report(results)

                        if self.write_results_to_file:
                            number_of_asi_report_bytes = self.asi_protobuf_processor.add_sv3_report(results)

                        self.results_processor(results)

                elif self.target_type == 'ARSR' and self.arsr_config['process_arsr']:
                    pass

                elif self.target_type == 'DTV' and self.dtv_config['process_dtv']:
                    pass

                # delete the data file and temp directory
                if self.delete_after_processing:
                    if not self.run_from_file_list:
                        _logger.info('Deleting file "{}"'.format(os.path.basename(file_to_read)))
                        print('Deleting file "{}"'.format(os.path.basename(file_to_read)))
                        os.remove(file_to_read)
                    temp = os.path.join(self.execDir, 'data/snapshot_iq/') + datafile_dict['temp_dir_to_delete']
                    _logger.info('Deleting folder "{}/"'.format(temp))
                    print('Deleting folder "{}"'.format(temp))
                    shutil.rmtree(temp)

                if not self.asi_protobuf_processor.has_data:
                    print('No signals found.')

                if self.iridium_config['create_protobuf_file']:
                    if self.sv3_protobuf_processor.has_data:
                        self.sv3_protobuf_processor.write_file()
                        msg = 'Number of Sv3 Result Bytes Written: {}'
                        print(msg.format(self.sv3_protobuf_processor.get_number_of_bytes_this_write()))

                if self.asi_protobuf_processor.has_data and self.write_results_to_file:
                    self.set_snapshot_report(_filename, radio_state)

                    self.asi_protobuf_processor.write_file()
                    msg = 'Number of ASI Result Bytes Written: {}'
                    print(msg.format(self.asi_protobuf_processor.get_number_of_bytes_this_write()))

            if not self.delete_after_processing:
                self.exit_gracefully('', '')

            if self.run_from_file_list:
                self.exit_gracefully('', '')

    def set_snapshot_report(self, filename, radiostate):
        self.asi_protobuf_processor.snap_shot_report.asi_file_name = filename
        self.asi_protobuf_processor.snap_shot_report.sample_rate = radiostate['data_rate']
        self.asi_protobuf_processor.snap_shot_report.signal_freq_tasked = radiostate['frequency'][0]
        self.asi_protobuf_processor.snap_shot_report.sensor_lat = self.sensor_results['sensor_lat']
        self.asi_protobuf_processor.snap_shot_report.sensor_lon = self.sensor_results['sensor_lon']
        self.asi_protobuf_processor.snap_shot_report.sensor_pitch = self.sensor_results['sensor_pitch']
        self.asi_protobuf_processor.snap_shot_report.sensor_roll = self.sensor_results['sensor_roll']
        self.asi_protobuf_processor.snap_shot_report.sensor_yaw = self.sensor_results['sensor_yaw']
        self.asi_protobuf_processor.snap_shot_report.sys_time = self.sensor_results['sys_time']

        for i in range(0, len(self.results_channel_list)):
            self.asi_protobuf_processor.snap_shot_report.asi_chan.append(self.results_channel_list[i])

        for i in range(0, len(self.results_time_windows_list)):
            for j in range(0, len(self.results_time_windows_list[i])):
                self.asi_protobuf_processor.snap_shot_report. \
                    time_window_start.append(self.results_time_windows_list[i][j][0])
                self.asi_protobuf_processor.snap_shot_report. \
                    time_window_end.append(self.results_time_windows_list[i][j][1])

        x = self.asi_protobuf_processor.snap_shot_report.SignalType.Value(self.target_type)
        setattr(self.asi_protobuf_processor.snap_shot_report, 'signal_type_tasked', x)
        # self.asi_protobuf_processor.snap_shot_report.signal_type_tasked = self.target_type

    def datafile_processor(self, file_to_read):
        datafile_dict = {}

        file_size = self.check_file_size(file_to_read)

        # read asi snapshot file ******************************

        self.ReadSnapshotFile_namespace = Namespace(multiple=False,
                                                    file=file_to_read,
                                                    choose=False,
                                                    save=False,
                                                    info=self.info,
                                                    debug=self.debug)
        if self.info or self.debug:
            print('\n\n' + 30 * '*' + ' read_snapshot_file ' + 30 * '*')
        str = time.time()
        self.data_file = os.path.basename(file_to_read)
        _logger.info('Processing file: {}'.format(self.data_file))
        snapshots = ReadSnapshotFile.main(self.ReadSnapshotFile_namespace)
        datafile_dict['output_files'] = snapshots.get_output_files()
        datafile_dict['data_nav'] = snapshots.get_data_nav()
        datafile_dict['radio_state'] = snapshots.get_radio_state()
        datafile_dict['number_of_channels'] = snapshots.get_number_of_channels()
        data_rate = datafile_dict['radio_state']['data_rate']  # sample rate
        datafile_dict['data_rate'] = data_rate
        datafile_dict['temp_dir_to_delete'] = snapshots.get_file_read()
        datafile_dict['snapshot_iq_dir'] = snapshots.get_snapshot_iq_Dir()
        self.collection_lat = datafile_dict['data_nav']['GPS']['latitude']
        self.collection_lon = datafile_dict['data_nav']['GPS']['longitude']
        datafile_dict['iq_data'] = snapshots.get_iq_data()
        if self.info:
            if file_size > 1e9:
                _logger.info('File Size: {0:.1f} GB'.format(file_size / 1e9))
            elif file_size > 1e6:
                _logger.info('File Size: {0:.1f} MB'.format(file_size / 1e6))
            elif file_size > 1e3:
                _logger.info('File Size: {0:.1f} KB'.format(file_size / 1e3))
            else:
                _logger.info('File Size: {0:.1f} B'.format(file_size))
            # _logger.info('Using Channel: {0:1d}'.format(self.channel_to_read))
            _logger.info('Collect Latitude: {:.6f}'.format(self.collection_lat))
            _logger.info('Collect Longitude: {:.6f}'.format(self.collection_lon))
        etr = time.time()
        self.processing_times['read_snapshot_file'] += etr - str
        if self.info or self.debug:
            print(25 * '*' + ' read_snapshot_file complete ' + 26 * '*')
            print(80 * '=')

        return datafile_dict

    def results_processor(self, results):
        for target_type in self.target_types:
            if target_type == self.target_type:
                self.number_signals[target_type] += 1

        # if not self.write_results_to_file:
        #     return
        #
        # if self.info or self.debug:
        #     print('\n' + 30 * '*' + ' tabulating results ' + 30 * '*')
        # self.result_number += 1
        #
        # self.save_results(results)
        #
        # if self.show_results:
        #     self.print_results(results)
        #
        # if self.info or self.debug:
        #     print(25 * '*' + ' tabulating results complete ' + 26 * '*')
        #     print(80 * '=')
        #     print(80 * '=')

    # def save_results(self, results):
    #     self.results_file.write('result:{}'.format(self.result_number))
    #     for key in results.keys():
    #         self.results_file.write(',{}:{}'.format(key, results[key]))
    #     self.results_file.write('\n')
    #     self.results_file.flush()

    def print_results(self, results):
        for key in results.keys():
            if type(results[key]) is float:
                print('{}: {:.6f}'.format(key, results[key]))
            else:
                print('{}: {}'.format(key, results[key]))
        print(80 * '=')

    def print_rocessing_times(self):
        if self.show_processing_time:
            tabulate.PRESERVE_WHITESPACE = True
            table0 = ['time\n(sec)', 0.0]
            total_processing_time = 0.0
            for module in self.processing_times.keys():
                tt = self.processing_times[module]
                total_processing_time += tt
                table0.append(tt)
            table0[1] = total_processing_time
            headers = ['\ntotal  ', 'read  \nsnapshot\nfile  ', '\ngnuradio', '\ngnuais ',
                       'decode \nnmea  \nsentences', '\ndfengine']
            print('\n' + 33 * '*' + ' Processing Times (sec.) ' + 28 * '*')
            print(tabulate([table0], headers=headers, tablefmt="fancy_grid",
                           stralign='center', colalign=("center",), floatfmt=".6f"))

    def exit_gracefully(self, _signum='', _frame=''):
        # self.asi_protobuf_processor.total_files_processed = self.number_of_files_processed
        if self.show_processing_time:
            self.print_rocessing_times()

        # if self.write_results_to_file:
            # self.results_file.close()

            # f = open(self.full_path_filename, 'r')
            # lines = f.read().split('\n')
            # f.close()
            #
            # if not len(lines) == 1:
            #     print('\n{} signals were found in this run.'.format(len(lines) - 1))
            # else:
            #     print('\nNo signals were found in this run.')

        print('\nSignals Processed')
        print(80*'*')
        for target_type in self.target_types:
            print('{}: {}'.format(target_type, self.number_signals[target_type]))

        if self.iridium_config['create_protobuf_file']:
            self.sv3_protobuf_processor.close()
            msg = '\nTotal Sv3 Report Bytes Written: {}'
            print(msg.format(self.sv3_protobuf_processor.get_number_of_bytes()))

        if self.write_results_to_file:
            self.asi_protobuf_processor.close()
            msg = '\nTotal ASI Snapshot Report Bytes Written: {}'
            print(msg.format(self.asi_protobuf_processor.get_number_of_bytes()))

        print('\nTotal number of files processed: {:,}'.format(self.number_of_files_processed))
        print('\n\nAsiProcessing is exiting...\n\n')

        exit()


def main(args):
    asi_processor = AsiProcessor()
    asi_processor.set_info(args.info)
    asi_processor.set_debug(args.debug)
    asi_processor.show_results = args.results
    asi_processor.delete_after_processing = args.xdelete
    asi_processor.show_processing_time = args.processingtime

    if args.list:
        asi_processor.run_from_file_list = True

    if args.gps:
        asi_processor.use_fixed_gps = args.gps

    if args.test:
        print('Running from test folder: "data/test_files"')
        asi_processor.set_folder_to_check('data/test_files')

    asi_processor.init()
    asi_processor.start()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-d', '--debug', required=False,
                        help='enable debug logging',
                        action='store_true')

    parser.add_argument('-g', '--gps', required=False,
                        help='use fixed GPS from config',
                        action='store_true')

    parser.add_argument('-i', '--info', required=False,
                        help='enable info logging',
                        action='store_true')

    parser.add_argument('-l', '--list', required=False,
                        help='run from file list',
                        action='store_true')

    parser.add_argument('-p', '--processingtime', required=False,
                        help='show processing times',
                        action='store_true')

    parser.add_argument('-r', '--results', required=False,
                        help='show results in terminal',
                        action='store_true')

    parser.add_argument('-t', '--test', required=False,
                        help='run using test_files folder as input',
                        action='store_true')

    parser.add_argument('-x', '--xdelete', required=False,
                        help='do not delete files after processing',
                        action='store_false')

    args = parser.parse_args()

    main(args)
