'''
configUtility.py

Don Kellmel

20-April-2019
  updated to python3
12-May-2018
  added getDictionary
26-November-2017
  initial
'''

import os
import configparser

from os import path


class Configuration():

    def __init__(self):
        #     programPath = path.abspath(__file__)
        #     components = programPath.split(os.sep)
        #     aceDir = str.join(os.sep, components[:components.index("ACE")+1]) + '/'
        #     self.configFile = aceDir + 'Ace/ACE.conf'
        pass

    # *****************************************************************************
    # set the configuration file
    # configFile - configuration file name (full path)
    # *****************************************************************************

    def setConfigFile(self, configFile):
        self.configFile = configFile

    # *****************************************************************************
    # read the configuration file
    # self.configFile - configuration file name (full path)
    # *****************************************************************************

    def getConfig(self):
        self.config = configparser.ConfigParser()
        self.config.optionxform = str  # necessary to preserve capitalization
        fp = open(self.configFile)
        self.config.readfp(fp)

    # *****************************************************************************
    # print all config sections
    # *****************************************************************************
    def printSections(self):
        print('\n... Configuration Sections ...')
        for section in self.config.sections():
            print(section)
        print('... End of Configuration Sections ...\n')

    def getSections(self):
        return self.config.sections()

    # *****************************************************************************
    # get a config value (returned as a string)
    # section  - the section within the config file, i.e., [section]
    # key      - the key within the section, i.e., key=value
    # *****************************************************************************

    def getConfigValue(self, section, key):
        return self.config.get(section, key)

    # *****************************************************************************
    # get a config value (returned as a string)
    # section  - the section within the config file, i.e., [section]
    # key      - the key within the section, i.e., key=value
    # *****************************************************************************

    def get(self, section, key):
        return self.config.get(section, key)

    def getString(self, section, key):
        return self.config.get(section, key)

    # *****************************************************************************
    # get an integer config value
    # section  - the section within the config file, i.e., [section]
    # key      - the key within the section, i.e., key=value
    # *****************************************************************************

    def getInt(self, section, key):
        rawVal = self.getConfigValue(section, key)
        return int(rawVal)

    # *****************************************************************************
    # get a boolean config value
    # section  - the section within the config file, i.e., [section]
    # key      - the key within the section, i.e., key=value
    # *****************************************************************************

    def getBool(self, section, key):
        return self.config.getboolean(section, key)

    # *****************************************************************************
    # get a float config value
    # section  - the section within the config file, i.e., [section]
    # key      - the key within the section, i.e., key=value
    # *****************************************************************************

    def getFloat(self, section, key):
        rawVal = self.config.get(section, key)
        return float(rawVal)

    # *****************************************************************************
    # get a list config value (ex. [255, 0 ,254])
    # section  - the section within the config file, i.e., [section]
    # key      - the key within the section, i.e., key=value
    # *****************************************************************************

    def getList(self, section, key):
        exec('rawList = ' + self.get(section, key))
        return locals()['rawList']

    # *****************************************************************************
    # get an integer list config value (ex. [255, 0 ,254])
    # section  - the section within the config file, i.e., [section]
    # key      - the key within the section, i.e., key=value
    # *****************************************************************************

    def getIntegerList(self, section, key):
        integerList = []
        exec('integerList = ' + self.get(section, key))
        #     rawVal = self.get(section,key).replace('[','').replace(']','')
        #     s = rawVal.split(',')
        #     for i in s:
        #       integerList.append(int(i.strip()))
        return integerList

    # *****************************************************************************
    # get a string list config value (ex. ["I","\n"])
    # section  - the section within the config file, i.e., [section]
    # key      - the key within the section, i.e., key=value
    # *****************************************************************************

    def getStringList(self, section, key):
        stringList = []
        exec('stringList = ' + self.get(section, key))
        #     rawVal = self.get(section,key).replace('[','').replace(']','')
        #     s = rawVal.split(',')
        #     for i in s:
        #       stringList.append(i.strip())
        return stringList

    def getSection(self, section):
        return self.getSectionDictionary(section)

    def getSectionDictionary(self, section):
        dictionary = {k: v for k, v in self.config.items(section)}
        return dictionary

    def getDictionary(self, section, key):
        exec('dictionary =' + self.get(section, key))
        return locals()['dictionary']
