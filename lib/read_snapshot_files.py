"""
read_snapshot_files

usage: python3 read_snapshot_files.py [-h] [-f FILE] [-d] [-s]

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  FILE = file to read
  -d, --dir             select a directory containing ".dat" files to read
  -s, --sel             select a single file to read
"""

# **************************************************************************************
# required for inheritance
# **************************************************************************************
import os
import sys
# from os import path
#
# programPath = path.abspath(__file__)
# components = programPath.split(os.sep)
# aisDir = str.join(os.sep, components[:components.index("AsiDataProcessing") + 1]) + '/'
# sys.path.insert(0, aisDir)

# **************************************************************************************

from lib.asiUtility import read_mp
from lib.printUtility import printit
from lib.diskUtility import DiskUtility

import time
import shutil
import pickle
import logging
import argparse

from pathlib import Path
from sys import version_info
from multiprocessing import Process

if version_info.major == 2:
    # Python 2.x
    python_version = 2
    import Tkinter as tk
    import tkFileDialog
elif version_info.major == 3:
    # Python 3.x
    python_version = 3
    import tkinter as tk
    from tkinter import filedialog

    _logger = logging.getLogger(__name__)

execDir = os.getcwd()+'/'


def output_channel_data(data, out_file):
    st = time.time()
    data.tofile(out_file)
    et = time.time()
    # print('{} done => {:.2f} seconds'.format(os.path.basename(out_file),et-st))


class Snapshot:

    def __init__(self):
        self.set_debug(True)
        self.snapshotIqDir = execDir + 'data/snapshot_iq/'
        if not os.path.isdir(self.snapshotIqDir):
            os.mkdir(self.snapshotIqDir)
        self.ffs = DiskUtility()
        self.ffs.set_debug(self.debug)
        self.ffs.set_start_folder(execDir + 'data/')
        self.printit = printit()
        self.save_all_channels = False
        self.output_files = None

    def init(self):
        if self.info:
            logging.basicConfig(level=logging.INFO)
            _logger.info('INFO logging set.')

        if self.debug:
            logging.basicConfig(level=logging.DEBUG)
            _logger.debug('DEBUG logging set.')

    def set_debug(self, debug):
        self.debug = debug

    def get_debug(self):
        return self.debug

    # def set_channel_to_read(self, channel_to_read):
    #     self.channel_to_read = channel_to_read
    #
    # def get_channel_to_read(self):
    #     return self.channel_to_read

    def make_output_dir(self, outDir):
        _head, tail = os.path.split(outDir)
        self.snapshot_iq_Dir = self.snapshotIqDir + tail + '/'
        self.msg = ''
        if os.path.isdir(self.snapshot_iq_Dir):
            if self.debug:
                self.msg += '\nDeleting directory "{}"'.format(self.snapshot_iq_Dir)
            shutil.rmtree(self.snapshot_iq_Dir)
        if self.debug:
            self.msg += '\nCreating directory "{}"'.format(self.snapshot_iq_Dir)
        os.mkdir(self.snapshot_iq_Dir)

    def get_snapshot_iq_Dir(self):
        return self.snapshot_iq_Dir

    def select_folder(self):
        data_folder = self.ffs.select_folder()
        return data_folder

    def select_file(self, data_folder):
        filename = self.ffs.select_file(data_folder)
        return filename

    def get_file_list(self, folder):
        files = []
        for file_ in os.listdir(folder):
            fullPathFile = os.path.join(folder, file_)
            if not os.path.isdir(fullPathFile):
                if Path(fullPathFile).suffix == '.dat':
                    files.append(fullPathFile)
        return files

    def read_snapshots(self, filesToRead):
        for fileToRead in filesToRead:
            self.make_output_dir(fileToRead)
            self.printit = printit(filename=self.snapshot_iq_Dir + 'DebugOutput.txt')
            self.printit.print_to_file(self.msg)
            self.read_a_snapshot(fileToRead)

    def read_a_snapshot(self, snapshotFile):

        _logger.info('Reading file: "{}"'.format(snapshotFile))

        # read the data
        st1 = time.time()
        self.iq_data, self.data_nav, self.radio_state, _ = read_mp(snapshotFile)
        et1 = time.time()
        # print('Read time: {:.2f} seconds'.format(et1-st1))

        _logger.info('Founf {} channels with {:,} IQ values each\n'.format(len(self.iq_data),
                                                                     len(self.iq_data[0])))

        # # pickle the metadata
        # self.metadata_filename = self.snapshot_iq_Dir + 'metadata.pickle'
        # self.metadata_file = open(self.metadata_filename, 'wb')
        # pickle.dump(self.radio_state, self.metadata_file)
        # pickle.dump(self.data_nav, self.metadata_file)
        # self.metadata_file.close()

        if self.debug:
            # print the radio state to the output file
            # print(self.radio_state)
            _logger.debug(35 * '=' + 'radio_state' + 35 * '=')
            for k in self.radio_state.keys():
                _logger.debug('{}: {}'.format(k, self.radio_state[k]))

            # print the data nav to the output file
            # print(self.data_nav)
            for key1 in self.data_nav.keys():
                _logger.debug((35 * '*' + '{}' + 35 * '*').format(key1))
                if key1 == 'client_time':
                    _logger.debug('client_time: {}'.format(self.data_nav[key1]))
                    continue
                for key2 in self.data_nav[key1].keys():
                    _logger.debug('{}: {}'.format(key2, self.data_nav[key1][key2]))
            _logger.debug(80 * '=')

        if self.save_all_channels:
            self.save_channels()

        _logger.debug(35 * '=' + 'IQ FILES' + 35 * '=')
        # if self.debug:
        #     for f in self.output_files:
        #         _logger.debug(f)
        #     _logger.debug(80 * '=')

    def get_number_of_channels(self):
        return len(self.iq_data)

    def save_channels(self):
        self.output_files = []
        processes = []
        st2 = time.time()
        for chan in range(0, len(self.iq_data)):
            output_file = self.snapshot_iq_Dir + 'snapshot_iq' + str(chan) + '_iq'
            # self.iq_data[chan].tofile(output_file)
            self.output_files.append(output_file)
            p = Process(target=output_channel_data, args=(self.iq_data[chan], output_file))
            processes.append(p)
            p.start()
        for p in processes:
            p.join()
        et2 = time.time()
        # print('Write time: {:.2f} seconds'.format(et2-st2))

    def get_file_read(self):
        return os.path.basename(self.file_read)

    def get_output_files(self):
        return self.output_files

    def get_output_file(self, chan):
        return self.output_files[chan]

    def get_metadata(self):
        return self.radio_state, self.data_nav

    def get_data_nav(self):
        return self.data_nav

    def get_radio_state(self):
        return self.radio_state

    def get_iq_data(self):
        return self.iq_data

    # read the metadata pickle file

    def read_metadata(self, filename):
        with open(filename, "rb") as f:
            radio_state = pickle.load(f)
            data_nav = pickle.load(f)
            self.metadata_file.close()
            return radio_state, data_nav

    # print the metadata

    def print_metadata(self, radio_state, data_nav):
        print('\n\n***** metadata *****\n')

        print('.....radio_state.....')
        for k in radio_state.keys():
            print('{}: {}'.format(k, radio_state[k]))

        for k1 in data_nav.keys():
            print('\n.....{}.....'.format(k1))
            if k1 == 'client_time':
                print('client_time: {}', self.data_nav[k1])
                continue
            for k2 in data_nav[k1].keys():
                print('{}: {}'.format(k2, data_nav[k1][k2]))


def main(args):
    snapshots = Snapshot()

    fileToRead = None

    if not args.debug:
        snapshots.set_debug(False)

    if not args.save:
        pass
    else:
        snapshots.save_all_channels = True

    snapshotsinfo = args.info

    if args.file:
        fileToRead = args.file
        snapshots.file_read = fileToRead
        snapshots.make_output_dir(fileToRead)
        snapshots.read_a_snapshot(fileToRead)

    elif args.multiple:
        folderToRead = snapshots.select_folder()
        if folderToRead == None:
            print('No folder selected. Exiting...')
            exit()

        filesToRead = snapshots.get_file_list(folderToRead)
        if len(filesToRead) == 0:
            print('No files to read. Exiting...')
            exit()
        snapshots.read_snapshots(filesToRead)

    elif args.choose:
        folderToRead = snapshots.select_folder()
        if folderToRead == None:
            print('No folder selected. Exiting...')
            exit()

        fileToRead = snapshots.select_file(folderToRead)
        if fileToRead == None:
            print('No file selected. Exiting...')
            exit()

        snapshots.init()
        snapshots.make_output_dir(fileToRead)
        snapshots.printit = printit(filename=snapshots.snapshot_iq_Dir + 'output.txt')
        snapshots.printit.print_to_file(snapshots.msg)
        snapshots.read_a_snapshot(fileToRead)

    else:
        fileToRead = None
        print('Nothing to do! Use -s, -d or -h.')

    return snapshots


# import sys
if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('-c', '--choose',
                        help='choose a single file to read',
                        action='store_true')

    parser.add_argument('-d', '--debug',
                        help='enable debug logging',
                        action='store_true')

    parser.add_argument('-f', '--file',
                        help='FILE = file to read')

    parser.add_argument('-i', '--info',
                        help='enable info logging',
                        action='store_true')

    parser.add_argument('-m', '--multiple',
                        help='select a directory containing ".dat" files to read',
                        action='store_true')

    parser.add_argument('-s', '--save',
                        help='save all channels',
                        action='store_true')

    args = parser.parse_args()

    main(args)

    print('done...')
