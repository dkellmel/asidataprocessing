import os
from pathlib import Path
from sys import version_info
from ast import walk

if version_info.major == 2:
    # Python 2.x
    python_version = 2
    import Tkinter as tk
    import tkFileDialog
elif version_info.major == 3:
    # Python 3.x
    python_version = 3
    import tkinter as tk
    from tkinter import filedialog


class FolderFileSelector:

    def __init__(self):
        self.dbg = False
        self.start_folder = '../data/'

    def set_debug(self, dbg):
        self.dbg = dbg

    def set_start_folder(self, start_folder):
        self.start_folder = start_folder

    def select_folder(self, start_folder='/home/', title='Double click on folders, then click OK'):
        root = tk.Tk()
        root.withdraw()

        if python_version == 2:
            tk.Tk().withdraw()  # Close the root window
            folder_selected_ = tkFileDialog.askdirectory(initialdir=start_folder,
                                                         title=title)
        else:  # python3
            tk.Tk().withdraw()  # Close the root window
            folder_selected_ = filedialog.askdirectory(initialdir=start_folder,
                                                       title=title)
        if folder_selected_ == ():
            folder_selected = None
        else:
            folder_selected = folder_selected_
            if not folder_selected.endswith('/'):
                folder_selected += '/'

        if self.dbg:
            print('FolderFileSelector.Folder selected : "{}"'.format(folder_selected))

        return folder_selected

    def select_file(self, start_folder, multiple=False, title=''):
        if not title == '':
            _title = title
        elif not multiple:
            _title = 'Select file to read'
        else:
            _title = 'Select files to read'

        root = tk.Tk()
        root.withdraw()
        if python_version == 2:  # python2
            file_selected_ = tkFileDialog.askopenfilenames(initialdir=start_folder,
                                                           title=_title,
                                                           multiple=multiple)
        else:  # python3
            file_selected_ = filedialog.askopenfilenames(initialdir=start_folder,
                                                         title=_title,
                                                         multiple=multiple)
        if file_selected_ == () or file_selected_ == '':
            file_selected = None
        else:
            file_selected = file_selected_[0]

        if self.dbg:
            print('FolderFileSelector.File selected : "{}"'.format(file_selected))

        return file_selected

    def get_full_path_folder_list(self, start_folder):
        full_path_folders = []
        for f in os.listdir(start_folder):
            fp = os.path.join(start_folder, f) + '/'
            if os.path.isdir(fp):
                full_path_folders.append(fp)
        return full_path_folders

    def get_folder_list(self, start_folder):
        folders = []
        for f in os.listdir(start_folder):
            if os.path.isdir(os.path.join(start_folder, f)):
                folders.append(f)
        return folders

    def get_file_list(self, start_folder):
        _folders, _subfolders, files = os.walk(start_folder)
        return files

    def get_file_list_in_all_subfolders(self, start_folder):
        r = []
        for root, dirs, files in os.walk(start_folder):
            for name in files:
                r.append(os.path.join(root, name))
        return r
