"""
read_results_files.py

usage: python3 read_results_files.py [-h] [-f FILE] [-s] [-d] [-i]

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  FILE = file to read
  -s, --select          select a single file to read
  -d, --debug           enable debug logging
  -i, --info            enable info ligging

returns:
    bytes_read                  number of bytes read
    asi_processor_reports       the protobuf busser
    asi_processor_reports_pb2   the compiled .poroto file
"""

import os
import sys
import logging
import argparse

execDir = os.getcwd() + '/'
if 'lib' in execDir:
    sys.path.append(execDir + "..")  # python3 lib/read_results_files.py
    data_folder = os.path.join(execDir, '../../data/')
else:
    sys.path.append(execDir)  # cd tools/Protobuf/; python3 AsiProcessorReader.py
    data_folder = os.path.join(execDir, 'data/')

import lib
import ProcessingLib

_logger = logging.getLogger(__name__)


class Results:
    debug = False
    info = False
    select = None
    file = None
    file_to_read = None

    def __init__(self):
        self.ffs = lib.DiskUtility()
        self.ffs.set_debug(self.debug)
        self.ffs.set_start_folder(execDir + 'data/')

        self.asi_processor_reports = ProcessingLib.AsiProcessorReports_pb2.AsiProcessorReports()
        self.asi_processor_reports_pb2 = ProcessingLib.AsiProcessorReports_pb2

        package_name = self.asi_processor_reports_pb2.AsiProcessorReports.DESCRIPTOR.full_name
        print('Package Name: {}\n'.format(package_name.split('.')[0]))

    def init(self):
        if self.info:
            logging.basicConfig(level=logging.INFO)
            _logger.info('INFO logging set.')
            _logger.info('Execution Directory: {}'.format(execDir))

        if self.debug:
            logging.basicConfig(level=logging.DEBUG)
            _logger.debug('DEBUG logging set.')

    def select_file(self, start_folder=data_folder):
        file_to_read = self.ffs.select_file(start_folder)
        return file_to_read

    def read_file(self, file_to_read):
        _logger.info('Reading file: "{}"'.format(file_to_read))
        with open(file_to_read, 'rb') as f:
            buf = f.read()
            _logger.info('Number of bytes read: {}\n'.format(len(buf)))
            self.asi_processor_reports.ParseFromString(buf)
        return len(buf), self.asi_processor_reports, self.asi_processor_reports_pb2


def main(_args):
    _asi_processor_reports = None
    results = Results()

    if _args.debug:
        results.debug = True

    if _args.info:
        results.info = True

    results.init()

    if _args.file is not None:
        _file_to_read = _args.file
        # results.file_to_read = _file_to_read
        # _asi_processor_reports = results.read_file(_file_to_read)

    elif _args.select:
        _file_to_read = results.select_file()

    else:
        _file_to_read = results.select_file()

    if _file_to_read is None:
        return None, None, None

    _bytes_read, _asi_processor_reports, _asi_processor_reports_pb2 = results.read_file(_file_to_read)

    return _bytes_read, _asi_processor_reports, _asi_processor_reports_pb2


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('-s', '--select',
                        help='select a single file to read',
                        action='store_true')

    parser.add_argument('-d', '--debug',
                        help='enable debug logging',
                        action='store_true')

    parser.add_argument('-f', '--file',
                        help='FILE = file to read (full path)')

    parser.add_argument('-i', '--info',
                        help='enable info logging',
                        action='store_true')

    args = parser.parse_args()

    asi_processor_reports = main(args)

    if args.debug:
        # print all the reports
        print(asi_processor_reports)

    print('\ndone...')
