#
# **************************************************************************************
# required for inheritance
# **************************************************************************************
import os
import sys
import numpy as np
# from os import path
#
# programPath = path.abspath(__file__)
# components = programPath.split(os.sep)
# aisDir = str.join(os.sep, components[:components.index("AsiDataProcessing") + 1]) + '/'
# sys.path.insert(0, aisDir)


# **************************************************************************************

class printit():

    def __init__(self, filename=None):
        if filename is not None:
            self.filename = open(filename, 'w')
        else:
            self.filename = filename

    def set_filename(self, filename):
        if filename is not None:
            self.filename = open(filename, 'w')
        else:
            self.filename = filename

    def print_to_terminal(self, msg):
        print(msg)

    def print_to_file(self, msg, eol='\n'):
        if self.filename is not None:
            self.filename.write(msg + eol)
            self.filename.flush()

    def print_to_all(self, msg):
        self.print_to_terminal(msg)
        if self.filename is not None:
            self.print_to_file(msg)

    def close(self):
        if self.filename is not None:
            self.filename.close()
