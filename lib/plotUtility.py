import math
import time
import multiprocessing
import matplotlib.pyplot as plt


def plot_graph(x, y,
               x_min=float('nan'), x_max=float('nan'),
               y_min=float('nan'), y_max=float('nan'),
               title='data',
               x_label='X', y_label='Y'):
    print("entered plot_graph()")
    fig = plt.gcf()
    fig.canvas.set_window_title(title)
    fig.suptitle(title, fontsize=20)
    plt.xlabel(x_label, fontsize=14)
    plt.ylabel(y_label, fontsize=14)
    plt.plot(x, y)
    plt.xlim(min(x), max(x))
    if math.isnan(y_max):
        plt.ylim(min(y), max(y))
    else:
        plt.ylim(min(y), y_max)
    plt.ioff()
    plt.show()  # this will block and remain a viable process as long as the plot window is open
    print("exiting plot_graph() process")
    time.sleep(2)

def scatter(x, y,
            x_min=float('nan'), x_max=float('nan'),
            y_min=float('nan'), y_max=float('nan'),
            title='data',
            x_label='X', y_label='Y'):
    pass


class PlotUtility:

    def __init__(self):
        pass

    def plot_y(self, y,
               x_min=float('nan'), x_max=float('nan'),
               y_min=float('nan'), y_max=float('nan'),
               title='Y', x_label='X', y_label='Y'):
        x = range(0, len(y))
        mp = multiprocessing.Process(target=plot_graph, args=(x, y,
                                                              x_min, x_max,
                                                              y_min, y_max,
                                                              title, x_label, y_label))
        mp.start()

    def plot_y_vs_x(self, x, y,
                    x_min=float('nan'), x_max=float('nan'),
                    y_min=float('nan'), y_max=float('nan'),
                    title='Y vs X', x_label='X', y_label='Y'):
        mp = multiprocessing.Process(target=plot_graph, args=(x, y,
                                                              x_min, x_max,
                                                              y_min, y_max,
                                                              title, x_label, y_label))
        mp.start()

    def plot_scatter(self, x, y,
                    x_min=float('nan'), x_max=float('nan'),
                    y_min=float('nan'), y_max=float('nan'),
                    title='Y vs X', x_label='X', y_label='Y'):
        pass