import os

import lib

import getpass

class GetMetadata:

    def __init__(self, f):
        self.f = f
        user = getpass.getuser()
        self.start_folder = os.path.join('/media', user)
        self.ffs = lib.FolderFileSelector()

    def get_radio_state(self):
        # f = self.ffs.select_file(start_folder=self.start_folder)
        if self.f is None:
            return None, None
        radio_state = lib.read_mp_radio_state(self.f)
        return radio_state

    def get_data_nav(self):
        # f = self.ffs.select_file(start_folder=self.start_folder)
        if self.f is None:
            return None, None
        header, radio_state, data_nav = lib.read_mp_all_metadata(self.f)

        return data_nav

    def get_all(self):
        # f = self.ffs.select_file(start_folder=self.start_folder)
        if self.f is None:
            return None, None
        header, radio_state, data_nav = lib.read_mp_all_metadata(self.f)

        return header, radio_state, data_nav
