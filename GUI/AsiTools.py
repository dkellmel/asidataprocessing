#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys
import time
import signal
import subprocess
import numpy as np

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from multiprocessing import Process, Queue

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

# **************************************************************************************
# required for lib importing
# **************************************************************************************
import os
import sys
from os import path

# programPath = path.abspath(__file__)
# components = programPath.split(os.sep)
# aisDir = str.join(os.sep, components[:components.index("AsiAisProcessing") + 1]) + '/'
# sys.path.insert(0, aisDir)

# **************************************************************************************

# import lib
import guilib
import utilities

from ReadSnapshotFile import Window
from get_metadata import GetMetadata
from FileProcessor import ReadSnapshotWidget
from FileProcessor import file_menu_processor
from MetadataProcessor import metadata_menu_processor
# from PlottingProcessor import main
from PlottingProcessor import PlotPower
from PlottingProcessor import PlotVoltage
from PlottingProcessor import PlotSpectrum
import PlottingProcessor as PlottingProcessor
from ProcessorsProcessor import ProcessorsProcessor


class AsiTools(QMainWindow):
    def __init__(self, parent=None):
        self.execution_path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/'
        self.data_folder = os.path.normpath(self.execution_path + '../data')
        print(self.execution_path)
        print(self.data_folder)
        # styles = [name.lower() for name in QStyleFactory.keys()]
        # for s in styles:
        #     print(s)
        super(AsiTools, self).__init__(parent=parent)
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setStyleSheet("background-color: grey;")
        self.title = 'ASI Tools'
        self.setWindowTitle(self.title)
        self.setWindowIcon(QIcon('asi_logo.png'))
        self.print_metadata = False

        self.statusbar = self.statusBar()
        self.progressBar = QProgressBar()
        self.progressBar.setGeometry(30, 40, 100, 25)
        self.statusBar().addPermanentWidget(self.progressBar)
        # This is simply to show the bar
        # self.progressBar.setRange(0, 0)
        # self.progressBar.setValue(50)

        self.init()
        self.initUI()
        self.addMenus()

        win_width, win_height = guilib.get_full_screen(app)
        self.setGeometry(10, 10, win_width, win_height)
        guilib.center(self)

        # self.set_size()
        self.layout_widget = QWidget(self)
        self.layout_widget.setLayout(self.hbox)
        self.setCentralWidget(self.layout_widget)
        # QApplication.setStyle(QStyleFactory.create('Cleanlooks'))
        QApplication.setStyle(QStyleFactory.create('fushion'))

        # self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.WindowType_Mask)
        # self.showFullScreen()
        self.showMaximized()

    def init(self):
        self.iq_data = None
        self.snapshot_file = None
        self.plot_type = None
        # self.exitAct = QAction(QIcon('exit24.png'), 'Exit', self)
        # self.exitAct.setShortcut('Ctrl+Q')
        # self.exitAct.setStatusTip('Exit application')
        # self.exitAct.triggered.connect(self.close)

    def initUI(self):
        # create top left frame
        self.topleftframe = QFrame()
        self.topleftframe.setStyleSheet("background-color: lightgrey;")
        self.topleftframe.setFrameShape(QFrame.StyledPanel)
        self.topleftframe_layout = QVBoxLayout(self.topleftframe)
        # self.topleftframe.hide()

        # create the bottom left frame
        self.bottomleftframe = QFrame()
        self.bottomleftframe.setFrameShape(QFrame.StyledPanel)
        self.bottomleftframe.setStyleSheet("background-color: lightgrey;")
        self.bottomleftframe_layout = QVBoxLayout(self.bottomleftframe)
        # self.bottomleftframe.hide()

        # create canvases for plotting
        self.create_canvas1()
        self.create_canvas2()

        # create top right frame
        self.toprightframe = QFrame()
        self.toprightframe.setFrameShape(QFrame.StyledPanel)
        self.toprightframe.setStyleSheet("background-color: lightgrey;")
        self.toprightframe_layout = QVBoxLayout(self.toprightframe)

        # create bottom right frame
        self.bottomrightframe = QFrame()
        self.bottomrightframe.setFrameShape(QFrame.StyledPanel)
        self.bottomrightframe.setStyleSheet("background-color: lightgrey;")
        self.bottomrightframe_layout = QVBoxLayout(self.bottomrightframe)

        # create a text edit widget and add it to the top right frame
        self.textedit = QTextEdit()
        self.textedit.setLineWrapMode(QTextEdit.NoWrap)
        self.textedit.setStyleSheet("background-color: lightblue;")
        self.toprightframe_layout.addWidget(self.textedit)

        # create a text edit widget and add it to the bottom right frame
        self.textedit2 = QTextEdit()
        self.textedit2.setLineWrapMode(QTextEdit.NoWrap)
        self.textedit2.setStyleSheet("background-color: lightblue;")
        self.bottomrightframe_layout.addWidget(self.textedit2)

        # create a splitter and add the right frames
        splitter0 = QSplitter(Qt.Vertical)
        splitter0.setHandleWidth(7)
        splitter0.addWidget(self.toprightframe)
        splitter0.addWidget(self.bottomrightframe)

        # create a splitter and add the left frames
        splitter1 = QSplitter(Qt.Vertical)
        splitter1.setHandleWidth(7)
        splitter1.addWidget(self.topleftframe)
        splitter1.addWidget(self.bottomleftframe)

        # create a horizontal splitter and add splitter0 and splitter1
        splitter2 = QSplitter(Qt.Horizontal)
        splitter2.addWidget(splitter1)
        splitter2.addWidget(splitter0)
        splitter2.setHandleWidth(7)
        splitter2.setStretchFactor(0, 4)

        # create a layout and add splitter2
        self.hbox = QHBoxLayout()
        self.hbox.addWidget(splitter2)

    def create_canvas1(self):
        # create plot frame and add it to the top left frame
        self.plot_frame = QFrame()
        self.topleftframe_layout.addWidget(self.plot_frame)

        # create a matplotlib figure
        # a figure instance to plot on
        self.figure1 = Figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas1 = FigureCanvas(self.figure1)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas1, self.plot_frame)

        from functools import partial

        # add hide button
        def hide_plot(caller):
            caller.plot_frame.hide()
            caller.plot_frame2.hide()

        # add zoom button
        def zoom_plot(caller):
            self.figure2.clear()
            choices = []
            try:
                for i in range(0, len(self.plot_voltage.data)):
                    choices.append(i)
            except:
                try:
                    for i in range(0, len(self.plot_spectrum.data)):
                        choices.append(i)
                except:
                    for i in range(0, len(self.plot_power.data)):
                        choices.append(i)
            list_selector = guilib.ListSelector(choices)
            list_selector.exec_()
            item = list_selector.get_item()
            if self.plot_type == 'Magnitude':
                self.plot_voltage.zoom_plot(caller.figure2, int(item))
            elif self.plot_type == 'Spectrum':
                self.plot_spectrum.zoom_plot(caller.figure2, int(item))
            elif self.plot_type == 'Power':
                self.plot_power.zoom_plot(caller.figure2, int(item))
            self.canvas2.draw_idle()
            caller.bottomleftframe.show()
            caller.plot_frame2.show()

        # add a hide button
        self.hide_button = QPushButton('Hide')
        self.hide_button.setStyleSheet("background-color: grey;border:2px solid rgb(0, 0, 0);")
        self.hide_button.clicked.connect(partial(hide_plot, self))

        # add a zoom button
        self.zoom_button = QPushButton('Zoom')
        self.zoom_button.setStyleSheet("background-color: grey;border:2px solid rgb(0, 0, 0);")
        self.zoom_button.clicked.connect(partial(zoom_plot, self))

        self.plot_label = QLabel()
        self.plot_label.setText(100 * ' ')
        # self.plot_label.resize(200, 25)

        # create the button frame
        self.button_frame = QFrame()
        # self.button_frame.setStyleSheet("background-color: yellow;border:2px solid rgb(0, 0, 0);")
        self.button_frame_layout = QHBoxLayout(self.button_frame)
        self.button_frame_layout.addWidget((self.hide_button))
        self.button_frame_layout.addWidget((self.zoom_button))
        self.button_frame_layout.addWidget((self.plot_label))
        self.button_frame.setFixedSize(self.button_frame_layout.sizeHint())

        # set the layout
        layout = QVBoxLayout(self.plot_frame)

        # layout.addWidget(self.toolbar)
        layout.addWidget(self.button_frame)
        layout.addWidget(self.canvas1)
        # layout.addWidget(self.hide_button)
        # layout.addWidget(self.zoom_button)
        self.plot_frame.hide()

    def create_canvas2(self):
        # create plot frame and add it to the top left frame
        self.plot_frame2 = QFrame()
        self.bottomleftframe_layout.addWidget(self.plot_frame2)

        # create a matplotlib figure
        # a figure instance to plot on
        self.figure2 = Figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas2 = FigureCanvas(self.figure2)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar2 = NavigationToolbar(self.canvas2, self.plot_frame2)

        # set the layout
        layout = QVBoxLayout(self.plot_frame2)

        layout.addWidget(self.toolbar2)
        layout.addWidget(self.canvas2)
        self.plot_frame2.hide()

    def addMenus(self):
        menubar = self.menuBar()

        file_menu = menubar.addMenu('&File')
        file_menu.addAction('Open File')
        file_menu_save_menu = file_menu.addMenu('Save...')
        file_menu_save_menu.addAction('Save Frequency List')
        file_menu.addSeparator()
        file_menu.addAction('Exit')
        file_menu.triggered[QAction].connect(self.file_menu_processor)

        option_menu = menubar.addMenu('Options')
        option_menu.addAction(QAction('PrintMetadata', option_menu, checkable=True))
        option_menu.triggered[QAction].connect(self.option_menu_processor)

        utility_menu = menubar.addMenu('Utilities')
        utility_menu.triggered[QAction].connect(self.utility_menu_processor)
        utility_menu.addAction('Get Frequency List')
        utility_menu.addAction('Get List of Files with Specific Frequency')

        metadata_menu = menubar.addMenu('Metadata')
        # radio_state = metadata_menu.addAction('RadioState')
        # data_nav = metadata_menu.addAction('DataNav')
        metadata_menu.addAction('RadioState')
        metadata_menu.addAction('DataNav')
        metadata_menu.addAction('All')
        metadata_menu.triggered[QAction].connect(self.metadata_menu_processor)

        plotting_menu = menubar.addMenu('Plotting')
        plotting_menu.triggered[QAction].connect(self.plotting_menu_processor)
        plotting_menu.addAction('Magnitude')
        plotting_menu.addAction('Spectrum')
        plotting_menu.addAction('Power')

        processor_menu = menubar.addMenu('Processors')
        processor_menu.triggered[QAction].connect(self.processors_menu_processor)
        processor_menu.addAction('AIS')
        processor_menu.addAction('Radar')
        processor_menu_iridium_menu = processor_menu.addMenu('Iridium')
        processor_menu_iridium_menu.addAction('Generate Sv3 Report Message')
        processor_menu_iridium_menu.addAction('Read Sv3 Report Message')

        processor_menu = menubar.addMenu('Results')
        processor_menu.triggered[QAction].connect(self.results_menu_processor)
        processor_menu.addAction('Open Results File')

        help_menu = menubar.addMenu('&Help')
        help_menu.triggered[QAction].connect(self.help_menu_processor)
        help = help_menu.addAction('Help')
        help.setStatusTip('Show Help')
        about = help_menu.addAction('About')
        about.setStatusTip('About AsiTools')

        show_menu = menubar.addMenu('Show')
        show_plots = show_menu.addAction('Show Plots')
        show_menu.triggered[QAction].connect(self.show_menu_processor)

    def results_menu_processor(self, q):
        # print(q.text())
        self.statusbar.showMessage(q.text())
        menu_item = q.text()

    def processors_menu_processor(self, q):
        # print(q.text())
        self.statusbar.showMessage(q.text())
        menu_item = q.text()

        processors_processor = ProcessorsProcessor()

        if False:
            pass

        elif menu_item == 'Generate Sv3 Report Message':
            res_file = QFileDialog.getOpenFileName(self, 'Open Data Results File',
                                                  '.', "bin files (*.res)")[0]
            # if res_file is not '':
            #     save_file = QFileDialog.getSaveFileName(self, 'Save Iridium Message File',
            #                                             'message.bin')[0]
            # if res_file is '' or save_file is '':
            #     return
            #
            # print('Data Results File: {}\nSave File: {}'.format(res_file, save_file))
            # with open(res_file, 'r') as f:
            #     lines = f.read().split('\n')
            # dict = {}
            # for line in lines:
            #     if line == '':
            #         break
            #     entries = line.split(',', 16)
            #     for entry in entries:
            #         keyvalue = entry.split(':', 1)
            #         dict[keyvalue[0]] = keyvalue[1]
            #
            # for key in dict.keys():
            #     print('{}: {}'.format(key, dict[key]))

            processors_processor.generate_sv3_report_protobuf_message()

        elif menu_item == 'Read Sv3 Report Message':
            filename = QFileDialog.getOpenFileName(self, 'Open Iridium Message File',
                                                   '.', "bin files (*.bin)")[0]
            if filename is not '':
                print('filename: {}'.format(filename))
                processors_processor.read_sv3_report_protobuf_message()

        del processors_processor

    def show_menu_processor(self, q):
        # print(q.text())
        # self.statusbar.showMessage(q.text())
        menu_item = q.text()

        if False:
            pass

        elif menu_item == 'Show Plots':
            self.plot_frame.show()
            self.plot_frame2.show()

    def dummy(self):
        for i in range(1, 16):
            print(i)
            time.sleep(0.25)

    def file_menu_processor(self, q):
        # print(q.text())
        self.statusbar.showMessage(q.text())
        menu_item = q.text()

        if False:
            pass

        elif menu_item == 'Open File':
            self.snapshot_file = PlottingProcessor.get_iq_file()
            if self.snapshot_file is None:
                return

            self.canvas1.figure.clf()
            self.canvas1.draw_idle()
            self.canvas2.figure.clf()
            self.canvas2.draw_idle()

            cmd = os.path.dirname(os.path.abspath(__file__)) + '/ProgressbarDialogUtility.py'
            proc1 = subprocess.Popen(['python3', cmd, 'Opening File',
                                      os.path.basename(self.snapshot_file)]).pid

            self.iq_data, self.data_nav, self.radio_state, self.junk = guilib.read_mp(self.snapshot_file)

            self.plot_label.setText('File = ' + os.path.basename(self.snapshot_file))
            self.plot_label.resize(self.plot_label.sizeHint())

            os.kill(proc1, signal.SIGTERM)

        elif menu_item == 'Save Frequency List':
            print(self.textedit2.toPlainText())

        elif menu_item == 'Exit':
            self.exit_gracefully()

    def utility_menu_processor(self, q):
        # print(q.text())
        self.statusbar.showMessage(q.text())

        if False:
            pass

        elif q.text() == 'Get Frequency List':
            base_folder = guilib.select_folder(self.data_folder)
            freq_counts = guilib.find_all_frequencies(base_folder=base_folder, status_bar=self.statusbar)
            self.textedit2.setText('Frequency Results...')
            self.textedit2.append('Found {} unique frequencies.'.format(len(freq_counts)))
            for _freq in freq_counts.keys():
                freq = float(_freq)
                if freq > 1e9:
                    self.textedit2.append('{} files @ {:.5f} GHz'.format(freq_counts[_freq], freq / 1e9))
                else:
                    self.textedit2.append('{} files @ {:.5f} MHz'.format(freq_counts[_freq], freq / 1e6))

        elif q.text() == 'Get List of Files with Specific Frequency':
            text, okPressed = QInputDialog.getText(self, "Enter Frequency", "Frequency",
                                                   QLineEdit.Normal, "1.2475e9")
            if okPressed and text != '':
                base_folder = guilib.select_folder(self.data_folder)
                try:
                    frequency = float(text)
                    print(frequency)
                    file_list = guilib.get_file_list_with_specific_frequency(frequency,
                                                                             base_folder=base_folder,
                                                                             status_bar=self.statusbar)
                except:
                    return

            for f in file_list:
                self.textedit.append(f)
                print(f)

        return

    def metadata_menu_processor(self, q):
        # print(q.text())
        # self.statusbar.showMessage(q.text())

        text = metadata_menu_processor(self.snapshot_file, q.text(), self.print_metadata)
        if text is not None:
            self.textedit.clear()
            self.textedit.insertPlainText('Metadata for File:\n')
            file_text = '{}\n'.format(os.path.basename(self.snapshot_file))
            self.textedit.insertPlainText(file_text)
            self.textedit.insertPlainText(text)

    def plotting_menu_processor(self, q):
        # print(q.text())
        # self.statusbar.showMessage(q.text())

        menu_item = q.text()
        self.plot_type = menu_item

        if False:
            pass

        elif menu_item == 'Magnitude':
            if self.iq_data is None:
                QMessageBox.about(self, "Error", "You have not opened a file yet.")
                return

            self.statusbar.showMessage('Plotting Magnitude...')
            cmd = os.path.dirname(os.path.abspath(__file__)) + '/ProgressbarDialogUtility.py'
            proc1 = subprocess.Popen(['python3', cmd, 'Plotting Magnitude', 'Processing...']).pid

            self.canvas1.figure.clf()
            self.canvas1.draw_idle()
            self.canvas2.figure.clf()
            self.canvas2.draw_idle()

            self.topleftframe.show()
            self.plot_frame.show()

            self.plot_voltage = PlotVoltage(self.figure1, self.canvas1)

            times = []
            delta_time = 1.0 / self.radio_state['data_rate']

            for i in range(0, len(self.iq_data[0])):
                times.append(i * delta_time)

            abs_iq_data = np.absolute(self.iq_data)
            # self.plot_voltage.plot(abs_iq_data / np.max(abs_iq_data), times)
            self.plot_voltage.plot(abs_iq_data, times)

            self.canvas1.draw_idle()

            os.kill(proc1, signal.SIGTERM)

            self.statusbar.showMessage('')

        elif menu_item == 'Spectrum':
            if self.iq_data is None:
                QMessageBox.about(self, "Error", "You have not opened a file yet.")
                return

            cmd = os.path.dirname(os.path.abspath(__file__)) + '/ProgressbarDialogUtility.py'
            proc1 = subprocess.Popen(['python3', cmd, 'Plotting Spectrum', 'Processing...']).pid

            self.canvas1.figure.clf()
            self.canvas1.draw_idle()
            self.canvas2.figure.clf()
            self.canvas2.draw_idle()

            self.topleftframe.show()
            self.plot_frame.show()

            self.plot_spectrum = PlotSpectrum(self.figure1, self.canvas1)

            freqs = []
            sample_interval = 1.0 / self.radio_state['data_rate']

            # for i in range(0, len(self.iq_data[0])):
            #     freqs.append(i*sample_interval)

            print('Computing FFT...')
            self.statusbar.showMessage('Computing FFT...')
            # spectrum = np.fft.fftshift(np.fft.fft(self.iq_data))
            spectrum = np.fft.fft(np.absolute(self.iq_data))

            # freqs = np.arange(-spectrum.shape[1] // 2,
            #                   spectrum.shape[1] // 2) * self.radio_state['data_rate'] / spectrum.shape[1]
            # f1 += self.radio_state['frequency'][0]

            x = np.array(range(len(self.iq_data[0]))) * sample_interval
            freqs = np.fft.fftfreq(len(x), d=sample_interval)
            # print(sample_interval, len(self.iq_data[0]))

            db_spectrum = 20 * np.log10(np.absolute(spectrum) / len(self.iq_data[0]))

            print('Plotting Spectrum...')
            self.statusbar.showMessage('Plotting Spectrum...')
            self.plot_spectrum.plot(db_spectrum, freqs)
            print('Plot Finished...')

            self.canvas1.draw_idle()

            os.kill(proc1, signal.SIGTERM)

            self.statusbar.showMessage('')

        elif menu_item == 'Power':
            if self.iq_data is None:
                QMessageBox.about(self, "Error", "You have not opened a file yet.")
                return

            self.statusbar.showMessage('Plotting Power...')
            cmd = os.path.dirname(os.path.abspath(__file__)) + '/ProgressbarDialogUtility.py'
            proc1 = subprocess.Popen(['python3', cmd, 'Plotting Power', 'Processing...']).pid

            self.canvas1.figure.clf()
            self.canvas1.draw_idle()
            self.canvas2.figure.clf()
            self.canvas2.draw_idle()

            self.topleftframe.show()
            self.plot_frame.show()

            self.plot_power = PlotPower(self.figure1, self.canvas1)

            times = []
            delta_time = 1.0 / self.radio_state['data_rate']

            for i in range(0, len(self.iq_data[0])):
                times.append(i * delta_time)

            pwr_iq_data = self.iq_data.real**2 + self.iq_data.imag**2
            # pwr_iq_data = 10 * np.log10(self.iq_data.real**2 + self.iq_data.imag**2)

            self.plot_power.plot(pwr_iq_data, times)

            self.canvas1.draw_idle()

            os.kill(proc1, signal.SIGTERM)

            self.statusbar.showMessage('')

    def help_menu_processor(self, q):
        # print(q.text())
        # self.statusbar.showMessage(q.text())

        if False:
            pass

        elif q.text() == 'Help':
            self.statusbar.showMessage(q.text())

        elif q.text() == 'About':
            dialog = QDialog()
            dialog.ui = guilib.Ui_About()
            dialog.ui.setupUi(dialog)
            dialog.exec_()
            dialog.show()

    def option_menu_processor(self, q):
        # print(q.text())
        # self.statusbar.showMessage(q.text())

        if False:
            pass

        elif q.text() == 'PrintMetadata':
            if q.isChecked():
                self.print_metadata = True
            else:
                self.print_metadata = False

    def exit_gracefully(self):
        print('AsiTools is exiting.')
        self.close()
        sys.exit(0)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    asi_tools = AsiTools()
    asi_tools.show()
    sys.exit(app.exec_())
