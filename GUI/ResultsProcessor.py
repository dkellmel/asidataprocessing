from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QProgressBar
import guilib


def results_menu_processor(menu_item):
    def read_a_snapshot(snapshotFile):
        iq_data, data_nav, radio_state, _ = read_mp(snapshotFile)
        return iq_data, se.data_nav, radio_state, _

    if False:
        pass
    elif menu_item == 'Open File':
        print('Open File:')


class ResultsProcessor:
    def __init__(self):
        pass


class ReadResultsWidget(QWidget):

    def __init__(self, parent=None):
        super(ReadResultsWidget, self).__init__(parent)
        layout = QVBoxLayout(self)

        # create a progress bar and a button and add them to the main layout
        self.progressBar = QProgressBar(self)
        self.progressBar.setRange(0,1)
        layout.addWidget(self.progressBar)
        button = QtGui.QPushButton("Start", self)
        layout.addWidget(button)

        button.clicked.connect(self.onStart)

        self.ReadResultsTask = TaskThread()
        self.ReadResultsTask.taskFinished.connect(self.onFinished)

    def onStart(self):
        self.progressBar.setRange(0,0)
        self.myLongTask.start()

    def onFinished(self):
        # Stop the pulsation
        self.progressBar.setRange(0, 1)
        self.progressBar.setValue(1)


class TaskThread(QtCore.QThread):
    taskFinished = QtCore.pyqtSignal()

    def __init__(self, resultsFile, parent=None):
        QtCore.QThread.__init__(self, parent)
        self.resultsFile = resultsFile

    def run(self):
        iq_data, data_nav, radio_state, _ = guilib.read_mp(self.resultsFile)
        self.taskFinished.emit()
        return iq_data, data_nav, radio_state, _


