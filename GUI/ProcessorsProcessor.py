import os
import sys
import time
import numpy as np
import multiprocessing
from sliplib import encode, decode

sys.path.append("..")  # Adds higher directory to python modules path.
import lib
import guilib

class ProcessorsProcessor:
    def __init__(self):
        pass

    def generate_sv3_report_protobuf_message(self):
        sv3_gen = Sv3ReportGenerator()
        sv3_gen.generate_report()

    def read_sv3_report_protobuf_message(self):
        sv3_read = Sv3ReportReader()
        sv3_read.read_report()


class Sv3ReportGenerator:
    def __init__(self):
        pass

    def generate_report(self):
        sv3_report = guilib.Sv3Report()

        # print(sv3_report)

        with open('Sv3Report.bin', 'wb') as f:
            # sv3_report.version = 101
            # sv3_report.sensor_id = 121
            # bytesAsString = sv3_report.SerializeToString()
            #
            # f.write(bytesAsString)
            # f.write('\n'.encode(encoding='UTF-8'))
            sv3_report.version = 201
            sv3_report.sensor_id = 221
            bytesAsString = sv3_report.SerializeToString()
            data_to_send = encode(bytesAsString) # + '\n'.encode(encoding='UTF-8')
            f.write(encode(data_to_send))
            # f.write('\n'.encode(encoding='UTF-8'))
            # data_to_send = encode(bytes(bytesAsString))
            print('bytesAsString:       ', bytesAsString)
            print('raw data to send:', data_to_send)


class Sv3ReportReader:
    def __init__(self):
        pass

    def read_report(self):
        sv3_report = guilib.Sv3Report()

        with open('Sv3Report.bin', 'rb') as f:
            # lines = f.read().split('\n'.encode(encoding='UTF-8'))
            # print('lines:', decode(lines))
            # # lines.pop()
            # for line in lines:
            #     sv3_report.ParseFromString(line)
            #     print(sv3_report)
            line = decode(f.read())
            sv3_report.ParseFromString(line)
            print(sv3_report)
