import sys
from PyQt5 import QtCore, QtWidgets

sys.path.append("..")  # Adds higher directory to python modules path.
import guilib

class ProgressbarDialogUtility(QtWidgets.QWidget):

    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.setWindowTitle('Main Window')

        layout = QtWidgets.QGridLayout()

        self.line_edit = QtWidgets.QLineEdit()
        layout.addWidget(self.line_edit)

        self.button = QtWidgets.QPushButton('Switch Window')
        # self.button.clicked.connect(self.switch)
        layout.addWidget(self.button)

        guilib.center(self)

        self.setLayout(layout)

        self.show()

def main():
    app = QtWidgets.QApplication(sys.argv)
    pbd = ProgressbarDialogUtility()
    pbd.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()