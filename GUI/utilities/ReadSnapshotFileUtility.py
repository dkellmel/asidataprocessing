import sys
import threading
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QProgressBar, QPushButton, QApplication

sys.path.append("..")  # Adds higher directory to python modules path.

import guilib


class ReadSnapshotWidget(QWidget):

    def __init__(self, title, snapshot_file, parent=None):
        self.iq_data, self.data_nav, self.radio_state, self.junk = None, None, None, None
        super(ReadSnapshotWidget, self).__init__()
        self.setWindowTitle(title)
        self.resize(500, 25)
        layout = QVBoxLayout(self)

        # create a progress bar
        self.progressBar = QProgressBar(self)
        self.progressBar.setRange(0, 1)
        layout.addWidget(self.progressBar)

        # add a button
        button = QPushButton("Abort", self)
        layout.addWidget(button)
        button.clicked.connect(self.onAbort)

        guilib.center(self)
        self.show()
        QApplication.processEvents()

        # self.ReadSnapshotTask = TaskThread(file_to_read)
        self.ReadSnapshotThread = threading.Thread(target=self.read_snapshot_file, args=(snapshot_file,))
        self.ReadSnapshotThread.start()
        self.ReadSnapshotThread.join()
        self.close()

    def read_snapshot_file(self, snapshot_file):
            self.iq_data, self.data_nav, self.radio_state, self.junk = guilib.read_mp(snapshot_file)

    def get_data(self):
        return self.iq_data, self.data_nav, self.radio_state, self.junk
        self.close()

    def onAbort(self):
        self.close()
