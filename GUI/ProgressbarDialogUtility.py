
import sys
from PyQt5 import QtWidgets

import guilib

class ProgressbarDialogUtility(QtWidgets.QWidget):

    def __init__(self, title, info):
        QtWidgets.QWidget.__init__(self)
        self.setWindowTitle(title)
        self.resize(400, 100)
        progressbar = QtWidgets.QProgressDialog(labelText=info,
                                                minimum=0, maximum=0)
        progressbar.setCancelButton(None)
        layout = QtWidgets.QGridLayout()
        layout.addWidget(progressbar)
        guilib.center(self)
        self.setLayout(layout)

def main(argv):
    app = QtWidgets.QApplication(sys.argv)
    if len(argv) > 1:
        pbd = ProgressbarDialogUtility(argv[0], argv[1])
    else:
        pbd = ProgressbarDialogUtility('Processing...', 'Please wait!')
    pbd.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main(sys.argv[1:])
