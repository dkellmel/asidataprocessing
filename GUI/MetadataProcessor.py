from PyQt5.QtWidgets import *

from get_metadata import GetMetadata

def metadata_menu_processor(snapshot_file, metadata_type, print_metadata):
    def get_radio_state():
        text = 20 * '*' + 'radio_state' + 20 * '*' + '\n'
        for key in radio_state.keys():
            text += '{}: {}\n'.format(key, radio_state[key])

        if print_metadata:
            print('\nFile: {}\n'.format(f))
            print(20 * '*' + 'radio_state' + 20 * '*')
            for key in radio_state.keys():
                print('{}: {}'.format(key, radio_state[key]))

        return text

    def get_data_nav():
        text = '\n' + 20 * '*' + 'data_nav' + 20 * '*' + '\n'
        for key1 in data_nav.keys():
            if isinstance(data_nav[key1], dict):
                text += '\n' + 20 * '*' + str(key1) + 20 * '*' + '\n'
                for key2 in data_nav[key1].keys():
                    text += '{}: {}\n'.format(key2, data_nav[key1][key2])
            else:
                text += '{}: {}\n'.format(key1, data_nav[key1])

        if print_metadata:
            print('\n' + 20 * '*' + 'data_nav' + 20 * '*')
            for key1 in data_nav.keys():
                if isinstance(data_nav[key1], dict):
                    print('\n' + 20 * '*' + str(key1) + 20 * '*')
                    for key2 in data_nav[key1].keys():
                        print('{}: {}'.format(key2, data_nav[key1][key2]))
                else:
                    print('{}: {}'.format(key1, data_nav[key1]))

        return text

    text = None

    if snapshot_file is None:
        QMessageBox.about(None, "Error", "You have not opened a file yet.")
        return text

    get_metadata = GetMetadata(snapshot_file)
    if False:
        pass
    elif metadata_type == 'RadioState':
        radio_state = get_metadata.get_radio_state()
    elif metadata_type == 'DataNav':
        data_nav = get_metadata.get_data_nav()
    elif metadata_type == 'All':
        header, radio_state, data_nav = get_metadata.get_all()

    if False:
        pass

    elif metadata_type == 'All':
        text = get_radio_state()
        text += get_data_nav()

    elif metadata_type == 'RadioState':
        text = get_radio_state()

    elif metadata_type == 'DataNav':
        text = get_data_nav()

    return text
