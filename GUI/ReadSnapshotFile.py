from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

from threading import Thread

import guilib


class ReadSnapshotFile(QThread):
    taskFinished = pyqtSignal()

    def __init__(self, snapshotFile):
        self.snapshotFile = snapshotFile
        self.iq_data = None
        self.data_nav = None
        self.radio_state = None
        super(ReadSnapshotFile, self).__init__()

    def run(self):
        self.iq_data, self.data_nav, self.radio_state, _ = guilib.read_mp(self.snapshotFile)
        self.taskFinished.emit()

class Window(QMainWindow):
    def __init__(self, snapshotFile):
        self.snapshotFile = snapshotFile
        super(Window, self).__init__()
        self.setGeometry(750, 450, 400, 200)
        self.setFixedSize(self.size())
        self.progbar()

    def progbar(self):
        self.prog_win = QDialog()
        self.prog_win.resize(400, 100)
        self.prog_win.setFixedSize(self.prog_win.size())
        self.prog_win.setWindowTitle("Processing request")
        self.lbl = QLabel(self.prog_win)
        self.lbl.setText("Please Wait.  .  .")
        self.lbl.move(15, 18)
        self.progressBar = QProgressBar(self.prog_win)
        self.progressBar.resize(410, 25)
        self.progressBar.move(15, 40)
        self.progressBar.setRange(0, 1)

        self.readSnapshotFile = ReadSnapshotFile(self.snapshotFile)
        self.prog_win.show()
        self.onStart()
        self.readSnapshotFile.taskFinished.connect(self.onFinished)

    def onStart(self):
        self.progressBar.setRange(0, 0)
        self.readSnapshotFile.start()
        self.readSnapshotFile.wait()
        self.iq_data = self.readSnapshotFile.iq_data
        self.data_nav = self.readSnapshotFile.data_nav
        self.radio_state = self.readSnapshotFile.radio_state



    def onFinished(self):
        self.progressBar.setRange(0,1)
        self.prog_win.close()
        self.close()
