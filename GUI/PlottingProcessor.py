import os
import sys
import time
import numpy as np
import multiprocessing
import matplotlib.pyplot as plt

sys.path.append("..")  # Adds higher directory to python modules path.
import lib

class PlottingProcessor:
    def __init__(self, figures=[], canvases=[]):
        self.figures = figures
        self.canvases = canvases
        for figure in self.figures:
            figure.clear()


class PlotVoltage:
    def __init__(self, figure, canvas):
        self.figure = figure
        self.canvas = canvas
        self.figure.clear()

    def plot(self, data, times):
        self.data = data
        self.times = times
        num_channels = len(self.data)

        n1 = int(num_channels/2)
        if (num_channels % 2) != 0:
            n1 += 1
        n2 = int(num_channels/2)

        axs = []
        gs = self.figure.add_gridspec(n1, n2)

        for i1 in range(0, n1):
            for i2 in range(0, n2):
                axs.append(self.figure.add_subplot(gs[i1, i2]))

        for chan in range(0, num_channels):
            axs[chan].plot(self.times, self.data[chan])
            title = 'Channel {}'.format(chan)
            axs[chan].set_title(title)
            axs[chan].set(xlabel='time (sec.)', ylabel='Magnitude')

        for i in range(num_channels, len(axs)):
            self.figure.delaxes(axs[i])

        self.figure.tight_layout()

    def zoom_plot(self, figure2, chan):
        # print('zooming')
        gs = figure2.add_gridspec(1, 1)
        ax = figure2.add_subplot(gs[0, 0])
        ax.plot(self.times, self.data[chan])
        title = 'Channel {}'.format(chan)
        ax.set_title(title)
        ax.set(xlabel='time (sec.)', ylabel='Magnitude')
        figure2.tight_layout()

class PlotSpectrum:
     def __init__(self, figure, canvas):
        self.figure = figure
        self.canvas = canvas
        self.figure.clear()

     def plot(self, data, freqs):
        self.data = data
        self.freqs = freqs
        num_channels = len(self.data)

        n1 = int(num_channels/2)
        if (num_channels % 2) != 0:
            n1 += 1
        n2 = int(num_channels/2)

        axs = []
        gs = self.figure.add_gridspec(n1, n2)

        for i1 in range(0, n1):
            for i2 in range(0, n2):
                axs.append(self.figure.add_subplot(gs[i1, i2]))

        for chan in range(0, num_channels):
            axs[chan].plot(self.freqs[0:len(self.freqs)//2], self.data[chan][0:len(self.freqs)//2])
            title = 'Channel {}'.format(chan)
            axs[chan].set_title(title)
            axs[chan].set(xlabel='frequency', ylabel='Magnitude (dB)')

        for i in range(num_channels, len(axs)):
            self.figure.delaxes(axs[i])

        self.figure.tight_layout()

     def zoom_plot(self, figure2, chan):
        # print('zooming')
        gs = figure2.add_gridspec(1, 1)
        ax = figure2.add_subplot(gs[0, 0])
        ax.plot(self.freqs[0:len(self.freqs)//2], self.data[chan][0:len(self.freqs)//2])
        title = 'Channel {}'.format(chan)
        ax.set_title(title)
        ax.set(xlabel='frequency', ylabel='Magnitude (dB)')
        figure2.tight_layout()

class PlotPower:
    def __init__(self, figure, canvas):
        self.figure = figure
        self.canvas = canvas
        self.figure.clear()

    def plot(self, data, times):
        self.data = data
        self.times = times
        num_channels = len(self.data)

        n1 = int(num_channels/2)
        if (num_channels % 2) != 0:
            n1 += 1
        n2 = int(num_channels/2)

        axs = []
        gs = self.figure.add_gridspec(n1, n2)

        for i1 in range(0, n1):
            for i2 in range(0, n2):
                axs.append(self.figure.add_subplot(gs[i1, i2]))

        for chan in range(0, num_channels):
            axs[chan].plot(self.times, self.data[chan])
            title = 'Channel {}'.format(chan)
            axs[chan].set_title(title)
            axs[chan].set(xlabel='time (sec.)', ylabel='Power')

        for i in range(num_channels, len(axs)):
            self.figure.delaxes(axs[i])

        self.figure.tight_layout()

    def zoom_plot(self, figure2, chan):
        # print('zooming')
        gs = figure2.add_gridspec(1, 1)
        ax = figure2.add_subplot(gs[0, 0])
        ax.plot(self.times, self.data[chan])
        title = 'Channel {}'.format(chan)
        ax.set_title(title)
        ax.set(xlabel='time (sec.)', ylabel='Power')
        figure2.tight_layout()

def get_iq_file():
    fs = lib.FolderFileSelector()
    file_selected = fs.select_file('../data/', title='Select complex data file')
    return file_selected


def read_file(iq_file):
    print('\nReading file: "{}"'.format(iq_file))
    iq_data, data_nav, data_radio_state, _ = lib.read_mp(iq_file)
    return iq_data, data_nav, data_radio_state, _
