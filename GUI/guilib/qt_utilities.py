from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QWidget, QGridLayout, QDialog
import time


class WorkerSignals(QtCore.QObject):
    """
    Defines the signals available from a running worker thread.
    Supported signals are:
    finished
        No data
    error
        `tuple` (exctype, value, traceback.format_exc() )
    result
        `object` data returned from processing, anything
    progress
        `int` indicating % progress
    """
    finished = QtCore.pyqtSignal()
    error = QtCore.pyqtSignal(tuple)
    result = QtCore.pyqtSignal(object)
    progress = QtCore.pyqtSignal(int)


class BusyProgressBar(object):
    def __init__(self, fn):
        self.execute_this_fn = fn

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 50)
        self.progressBar = QtWidgets.QProgressBar(Dialog)
        self.progressBar.setGeometry(QtCore.QRect(20, 10, 361, 23))
        # self.progressBar.setProperty("value", 24)
        # self.progressBar.setObjectName("progressBar")

        self.worker = Worker(self.execute_this_fn)
        self.worker.updateProgress.connect(self.setProgress)
        self.worker.finished.connect(self.finished)

        # self.retranslateUi(Dialog)
        Dialog.setWindowTitle("Processing ...")
        # self.progressBar.setValue(0)
        # QtCore.QMetaObject.connectSlotsByName(Dialog)

        # self.progressBar.minimum = 1
        # self.progressBar.maximum = 100
        self.progressBar.setRange(0, 0)

    # def retranslateUi(self, Dialog):
    #     Dialog.setWindowTitle("Processing...")
    #     # self.pushButton.setText("PushButton")
    #     self.progressBar.setValue(0)
    #     # self.pushButton.clicked.connect(self.worker.start)

    def setProgress(self, progress):
        self.progressBar.setValue(progress)

    def finished(self):
        Dialog.close()

# Inherit from QThread
class Worker(QtCore.QThread):
    signals = WorkerSignals()

    # This is the signal that will be emitted during the processing.
    # By including int as an argument, it lets the signal know to expect
    # an integer argument when emitting.
    updateProgress = signals.progress
    finished = signals.finished

    # You can do any extra things in this init you need, but for this example
    # nothing else needs to be done expect call the super's init
    def __init__(self, fn, *args, **kwargs):
        QtCore.QThread.__init__(self)
        self.fn = fn

    # A QThread is run by calling it's start() function, which calls this run()
    # function in it's own "thread".
    def run(self):
        self.fn()
        # Notice this is the same thing you were doing in your progress() function
        # for i in range(1, 5):
        #     # Emit the signal so it can be received on the UI side.
        #     self.updateProgress.emit(20*i)
        #     time.sleep(0.25)
        self.finished.emit()


class ListSelector(QDialog):
    def __init__(self, choices, title="Double click you're choice."):
        self.item = None
        QDialog.__init__(self)
        self.setWindowTitle(title)
        layout = QGridLayout(self)
        self.setLayout(layout)
        self.listwidget = QtWidgets.QListWidget(self)
        for i in range(0, len(choices)):
            self.listwidget.insertItem(i, str(choices[i]))
        self.listwidget.clicked.connect(self.clicked)
        layout.addWidget(self.listwidget)

    def clicked(self):
        self.item = self.listwidget.currentItem()
        # print(self.item.text())
        self.close()

    def get_item(self):
        return self.item.text()


def execute_this_fn():
    # signals = WorkerSignals()
    # updateProgress = signals.progress
    # finished = signals.finished
    for i in range(1, 5):
        # Emit the signal so it can be received on the UI side.
        # updateProgress.emit(20*i)
        print(i)
        time.sleep(0.25)
    # finished.emit()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    progress_bar = BusyProgressBar()
    progress_bar.setupUi(Dialog)
    Dialog.show()
    progress_bar.worker.start()
    sys.exit(app.exec_())
