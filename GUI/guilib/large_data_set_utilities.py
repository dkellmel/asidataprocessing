#
# **************************************************************************************
# required for lib importing
# **************************************************************************************
import os
import sys
from os import path

programPath = path.abspath(__file__)
components = programPath.split(os.sep)
aisDir = str.join(os.sep, components[:components.index("AsiDataProcessing") + 1]) + '/'
sys.path.insert(0, aisDir)

# **************************************************************************************

from lib.asiUtility import read_mp
from lib.asiUtility import read_mp_radio_state
from lib.asiUtility import read_mp_all_metadata
from lib.folder_file_selector import FolderFileSelector

from PyQt5.QtWidgets import QInputDialog

import time
import signal
import shutil
import msgpack
import argparse
import getpass
import msgpack_numpy


max_buffer_size = 16 * 1024 * 1024 * 1024
user = getpass.getuser()
ffs = FolderFileSelector()
start_folder = os.path.join('/media', user)

def find_all_frequencies(base_folder='.',status_bar=None):
    # out_file = open('/home/don/results.txt', 'w')
    # out_file = open(os.path.join(aisDir, 'data/frequency_results.txt'), 'w')

    print('Base Folder: {}'.format(base_folder))
    # print('\nBase Folder: {}'.format(base_folder), file=out_file)

    st = time.time()

    flist = ffs.get_file_list_in_all_subfolders(base_folder)
    len_flist = len(flist)

    print('Need to look at {0:,} files.'.format(len_flist))
    # print('Need to look at {0:,} files.'.format(len_flist), file=out_file)

    frequencies = []
    freq_counts = {}
    i = 1
    for f in flist:
        if status_bar is not None:
            msg = 'Processed {:,}/{:,} files'.format(i, len_flist)
            status_bar.showMessage(msg)
        print('Processed {:,}/{:,}'.format(i, len_flist), end='\r')
        i += 1

        if 'lost+found' in f:
            continue

        # read the data
        try:
            radio_state = read_mp_radio_state(f)
            frequency = int(float(radio_state['frequency'][0]))
            got_it = False
            if frequency in frequencies:
                got_it = True
            if not got_it:
                frequencies.append(frequency)
            if str(frequency) not in freq_counts:
                freq_counts[str(frequency)] = 1
            else:
                freq_counts[str(frequency)] += 1

        except:
            # print('Bad file: {}'.format(f))
            # print('Bad file: {}'.format(f), file=out_file)
            pass

    print('')
    et = time.time()
    elapsed_time = et - st
    h = int(elapsed_time // (3600))
    m = int((elapsed_time - h * (3600)) // 60)
    s = elapsed_time - h * (3600) - (m * 60)
    print('Elapsed time to read all files: {} hours {} minutes {:.3f} seconds'.format(h, m, s))
    # print('Elapsed time to read all files: {} hours {} minutes {:.3f} seconds'.format(h, m, s), file=out_file)

    print('Found {} frequencies.'.format(len(freq_counts)))
    # print('Found {} frequencies.\n'.format(len(freq_counts)), file=out_file)

    # for _freq in freq_counts.keys():
    #     freq = float(_freq)
    #     if freq > 1e9:
    #         print('{} @ {:.5f} GHz'.format(freq_counts[_freq], freq / 1e9))
    #         print('{} @ {:.5f} GHz'.format(freq_counts[_freq], freq / 1e9), file=out_file)
    #     else:
    #         print('{} @ {:.5f} MHz'.format(freq_counts[_freq], freq / 1e6))
    #         print('{} @ {:.5f} MHz'.format(freq_counts[_freq], freq / 1e6), file=out_file)
    # out_file.close()

    return freq_counts


def get_file_list_with_specific_frequency(frequency,base_folder='.', status_bar=None):
    print('Looking for frequency: {}'.format(frequency))
    flist = ffs.get_file_list_in_all_subfolders(base_folder)
    len_flist = len(flist)

    print('Need to look at {0:,} files.'.format(len_flist))

    file_list = []
    i = 1
    for f in flist:
        if status_bar is not None:
            msg = 'Processed {:,}/{:,} files'.format(i, len_flist)
            status_bar.showMessage(msg)

        print('Processed {:,}/{:,}'.format(i, len_flist), end='\r')
        i += 1

        if 'lost+found' in f:
            continue

        # read the data
        try:
            radio_state = read_mp_radio_state(f)
            this_frequency = int(float(radio_state['frequency'][0]))
            if frequency == this_frequency:
                file_list.append(f)
        except:
            # print('Bad file: {}'.format(f))
            pass
    return file_list
