import sys
from PyQt5.QtWidgets import QMainWindow, QWidget, QDesktopWidget, \
                            QApplication, QVBoxLayout, QTabWidget, \
                            QPushButton, QPlainTextEdit, QDialog, QTextEdit
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QSize

def center(window):
    # geometry of the main window
    qr = window.frameGeometry()

    # center point of screen
    cp = QDesktopWidget().availableGeometry().center()

    # move rectangle's center point to screen's center point
    qr.moveCenter(cp)

    # top left of rectangle becomes top left of window centering it
    window.move(qr.topLeft())


def get_full_screen(app, printit=False):
    screen = app.primaryScreen()
    size = screen.size()
    rect = screen.availableGeometry()
    win_width = int(0.8 * rect.width())
    win_height = int(0.8 * rect.height())
    if printit:
        print('Screen: %s' % screen.name())
        print('Size: %d x %d' % (size.width(), size.height()))
        print('Available: %d x %d' % (rect.width(), rect.height()))
        print(win_width, win_height)
    return win_width, win_height


class TextWindow(QDialog):
    def __init__(self, parent):
        super().__init__(parent)
        self.width = 600
        self.height = 600
        self.setMinimumSize(QSize(self.width, self.height))
        self.setWindowTitle("")

        # Add text field
        self.b = QTextEdit(self)
        self.b.setStyleSheet("background-color: yellow;")
        self.b.setReadOnly(True)
        # self.b.insertPlainText("You can write text here.\n")
        self.b.move(10, 10)
        # self.b.resize(int(0.9*self.width), int(0.9*self.height))
        self.b.resize(self.width-20, self.height-20)

    def set_title(self, title):
        self.setWindowTitle(title)

    def insert_text(self, txt):
        self.b.insertPlainText(txt)


