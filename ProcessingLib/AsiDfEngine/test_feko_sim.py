from AsiDfEngine.AsiDfE import AsiDfE
import SignalProcessing.read_feko_data as rfd
from SignalProcessing.process_array import ArrayProcessing
from AsiDfEngine.df_target.DfTarget import DfTarget
import numpy as np


def process_data(ap, snr=None, iterations=1, number_of_ports=None, reference=None):

    my_dfe = AsiDfE()

    if snr is None:
        sigma = 0
    else:
        sigma = 10 ** (-snr / 20)

    n = ap.sim_parameters['number_ports']
    # Account for reference signal
    ports = np.ones(n, np.bool)
    if reference is not None and reference < n:
        ports[reference] = False
        sigma /= (np.sqrt(2) * np.sqrt(n - 1))
    else:
        sigma /= (np.sqrt(2) * np.sqrt(n))

    results = dict()
    for p in ap.sim_parameters['polarity']:
        results.update({p: []})
        for frequency in ap.sim_parameters['frequency']:
            data = rfd.query_feko_yaml_data(ap.sim_data, frequency, p, number_of_ports=number_of_ports)
            aoa_results = np.zeros((iterations, len(ap.sim_parameters['depression']),
                                    len(ap.sim_parameters['azimuth'])))
            dep_results = np.zeros((iterations, len(ap.sim_parameters['depression']),
                                    len(ap.sim_parameters['azimuth'])))
            my_dfe.add_target(frequency, 100, DfTarget.Techniques.time_domain)
            for dep_count in range(len(ap.sim_parameters['depression'])):
                # d1 = af * data[dep_count]
                d1 = (data[dep_count].T / np.linalg.norm(data[dep_count, :, ports], axis=0)).T
                for az_count in range(len(ap.sim_parameters['azimuth'])):
                    noise = sigma * np.random.randn(iterations, n) + 1j * sigma * np.random.randn(iterations, n)
                    d2 = (noise + d1[az_count]).T
                    for k in range(iterations):
                        # noise = sigma * np.random.randn(n, 1) + 1j * sigma * np.random.randn(n, 1)
                        # d2 = d1[az_count].reshape(n, 1) + noise
                        # aoa, dep = ap.estimate_angles_from_basis(p, frequency, d2, depression=depression,
                        #                                            reference=reference)
                        rst = my_dfe.get_df_results(d2[:, k].reshape(n, 1), frequency, polarity=p)
                        aoa_results[k, dep_count, az_count] = rst[-1]['AoA']
                        dep_results[k, dep_count, az_count] = rst[-1]['dep']

            results[p].append({'frequency': frequency, 'AoA': aoa_results.copy(), 'dep': dep_results.copy()})
            my_dfe.remove_target(frequency)
            print("Polarization: {}, Frequency: {}".format(p, frequency))
    return results


def test_on_data(testing_filename, snr=25, iterations=100, number_of_ports=None, reference=None):

    ap = ArrayProcessing()

    ap.load_sim_file(testing_filename, number_of_ports=number_of_ports)
    if ap.sim_data is None:
        print("Testing failed due to bad testing filename")
        return None

    result = process_data(ap, snr=snr, iterations=iterations, number_of_ports=number_of_ports, reference=reference)
    x = np.array(ap.sim_parameters['depression'], ndmin=2).T.repeat(len(ap.sim_parameters['azimuth']), axis=1)

    data_out = dict()
    for pol in result:
        n = len(result[pol])
        # n = 10
        frequencies = np.zeros(n)
        peak_err_az = np.zeros(n)
        peak_err_el = np.zeros(n)
        data_out.update({pol: {}})
        for m in range(n):
            frequencies[m] = result[pol][m]['frequency']
            err_az = (result[pol][m]['AoA'] - ap.sim_parameters['azimuth']) % 360
            err_az[err_az < -180] += 360
            err_az[err_az > 180] -= 360
            peak_err_az[m] = abs(err_az).max()
            err_el = result[pol][m]['dep'] % 360 - x
            err_el[err_el < -180] += 360
            err_el[err_el > 180] -= 360
            peak_err_el[m] = abs(err_el).max()
        data_out[pol].update({'frequencies': frequencies, 'peak_err_az': peak_err_az, 'peak_err_el': peak_err_el})
    ap.test = False

    return result, data_out


if __name__ == "__main__":
    import os

    path = '/home/tim/ASIGINT Dropbox/Timothy Miller/Local_Workspace/PMSAI_B200'
    path = os.path.join(path, 'B200b_SourceData/4_00_in_White_Array_0deg-aligned_Band_00')
    filename = 'B200_3DPA_DEP_all_AZ_all_Freq_all_6p.yml'

    result_o, data_o = test_on_data(os.path.join(path, filename))
