from ProcessingLib.AsiDfEngine.df_target.DfTarget import DfTarget
import numpy as np


class DpaDfTarget(DfTarget):
    """
    A class that extends DfTarget with fields and algorithms specific to the ASI vector sensor.

    :param array_pointer:
    :type array_pointer: DpaArray
    :param frequency:
    :param bandwidth:
    :param target_index:
    :param technique:
    :type technique: DfTarget.Techniques

    """

    def __init__(self, array_pointer, frequency, bandwidth, target_index=0,
                 technique=DfTarget.Techniques.frequency_domain):

        super(DpaDfTarget, self).__init__(array_pointer, frequency, bandwidth, target_index, technique)

        # Add fields to dictionaries
        self.target_parameters.update({'azimuth_lookup': None,  # Interpolation function to adjust aoa estimate
                                       'depression_lookup': None,  # Interpolation function to adjust
                                                                   #   depression angle estimate
                                       'e_field_corrections': None,  # Storage for E-field calibration
                                                                     #   corrections
                                       })
        self.target_parameters.update({'poynting': [],  # Poynting vector from most recent DF
                                       'Ex': 0,
                                       'Ey': 0,
                                       'Ea': 0,
                                       'Eb': 0,
                                       'Er': 0,
                                       'El': 0,
                                       })

    def compute_aoa(self, data, polarity=None):
        """
        Compute angle of arrival for a target data set.

        :param data:
        :param polarity:

        :return: None
        """

        # Apply e_field correction for the frequency
        for m in range(self.sensor_array.array_parameters['number_of_elements']):
            self.sensor_array.antenna_array[m].antenna_parameters['e_field_correction'] = \
                self.target_parameters['e_field_corrections'][m]

        # Compute the fields
        e, h = self.sensor_array.compute_fields(data)

        # Compute initial angle so we can take out spatial separations
        poynting, _ = self._compute_poynting(e, h)
        aoa = np.rad2deg(np.arctan2(poynting[1], poynting[0]) + np.pi) % 360

        # Correct phase differences due to spatial distribution of the array
        if not self.sensor_array.correct_spatial_phases(data, self.target_parameters['frequency'], aoa):
            self.logger_target.warning("compute_aoa: Failed to correct DPA spatial phase differences")

        # Compute E and H fields after all phase corrections
        if self.sensor_array.array_parameters['reference_active']:
            e, h = self.sensor_array.compute_fields(data, self.sensor_array.array_parameters['reference_channel'])
        else:
            e, h = self.sensor_array.compute_fields(data)

        # Compute Poynting and Stokes parameters
        self.target_parameters['poynting'], e1 = self._compute_poynting(e, h)

        # Compute stokes parameters
        self._compute_stokes(e1)

        # Compute angles of arriving signal
        self.compute_angles()

    def _compute_stokes(self, e1):

        poynting = self.target_parameters['poynting']

        # Compute the offset, a
        a = (e1[0] * poynting[0] + e1[1] * poynting[1] + e1[2] * poynting[2]).mean()

        if abs(poynting[0]) > abs(poynting[1]):
            cx = np.array([[(a - poynting[1]) / (poynting[0] * np.sqrt(1 + ((a - poynting[1]) / poynting[0]) ** 2))],
                           [1.0 / np.sqrt(1 + ((a - poynting[1]) / poynting[0]) ** 2)], [0.0]])
            cy = np.array([[(a - poynting[2]) / (poynting[0] * np.sqrt(1 + ((a - poynting[2]) / poynting[0]) ** 2))],
                           [0.0], [1.0 / np.sqrt(1 + ((a - poynting[2]) / poynting[0]) ** 2)]])
        else:
            cx = np.array([[1 / np.sqrt(1 + ((a - poynting[0]) / poynting[1]) ** 2)],
                           [(a - poynting[0]) / (poynting[1] * np.sqrt(1 + ((a - poynting[0]) / poynting[1]) ** 2))],
                           [0.0]])
            cy = np.array([[0.0],
                           [(a - poynting[2]) / (poynting[1] * np.sqrt(1 + ((a - poynting[2]) / poynting[1]) ** 2))],
                           [1.0 / np.sqrt(1 + ((a - poynting[2]) / poynting[1]) ** 2)]])

        a = np.matrix([poynting, cx[:, 0], cy[:, 0]])
        v1 = np.matrix([[0.0], [1.0 / np.sqrt(2)], [1.0 / np.sqrt(2)]])
        v2 = np.matrix([[0.0], [-1.0 / np.sqrt(2)], [1.0 / np.sqrt(2)]])
        gx = np.array(np.linalg.inv(a) * v1)
        gy = np.array(np.linalg.inv(a) * v2)
        v1 = np.matrix([[0.0], [1.0 / np.sqrt(2)], [-1j / np.sqrt(2)]])
        v2 = np.matrix([[0.0], [1.0 / np.sqrt(2)], [1j / np.sqrt(2)]])
        rx = np.array(np.linalg.inv(a) * v1)
        ry = np.array(np.linalg.inv(a) * v2)

        self.target_parameters['Ex'] = (abs((cx * e1).sum(0)) ** 2).mean()
        self.target_parameters['Ey'] = (abs((cy * e1).sum(0)) ** 2).mean()
        self.target_parameters['Ea'] = (abs((gx * e1).sum(0)) ** 2).mean()
        self.target_parameters['Eb'] = (abs((gy * e1).sum(0)) ** 2).mean()
        self.target_parameters['Er'] = (abs((rx * e1).sum(0)) ** 2).mean()
        self.target_parameters['El'] = (abs((ry * e1).sum(0)) ** 2).mean()

    def compute_angles(self):

        # Rotate Poynting vector to take out pitch and roll
        # pt = np.deg2rad(pitch)
        # r_p = np.matrix([[np.cos(pt), 0, -np.sin(pt)], [0, 1, 0], [np.sin(pt), 0, np.cos(pt)]])
        # rl = np.deg2rad(roll)
        # r_r = np.matrix([[1, 0, 0], [0, np.cos(rl), np.sin(rl)], [0, -np.sin(rl), np.cos(rl)]])
        # r = r_r * r_p
        # self.target_results['poynting'] = np.array(r.T * np.matrix(self.target_results['poynting'])).reshape(3)

        # Compute angles from pointing vector
        aoa = np.rad2deg(np.arctan2(self.target_parameters['poynting'][1],
                                    self.target_parameters['poynting'][0]) + np.pi) % 360
        if self.target_parameters['azimuth_lookup'] is not None:
            try:
                aoa = self.target_parameters['azimuth_lookup'](aoa)
                aoa %= 360
            except ValueError:
                pass
        self.target_results['AoA'] = aoa

        elev = np.rad2deg(np.arctan2(self.target_parameters['poynting'][2],
                                     np.sqrt(self.target_parameters['poynting'][0] ** 2 +
                                             self.target_parameters['poynting'][1] ** 2))) % 360
        if self.target_parameters['depression_lookup'] is not None:
            try:
                elev = self.target_parameters['depression_lookup'](elev)
            except ValueError:
                pass
        self.target_results['dep'] = -elev

        # self.target_results['LoB'] = (yaw + aoa) % 360

    def _compute_poynting(self, e, h):
        """
        Compute the Poynting vector given the e and h fields

        :param e: E-field from DPA's
        :param h: H-field from DPA's

        :return: Tuple of Poynting vector and e-field
        """

        # Map fields to cardinal directions relative to the array
        e = np.matrix(e)
        if e.shape[0] == 1:
            e = e.T
        e = np.array(self.sensor_array.array_parameters['e_field_matrix'] * e)

        h = np.matrix(h)
        if h.shape[0] == 1:
            h = h.T
        h = np.array(self.sensor_array.array_parameters['h_field_matrix'] * h)

        # Compute Poynting vector and manifold vector
        self.target_parameters['feature_vector'] = np.array([np.inner(e[1], h[2].conj()),
                                                             np.inner(e[2], h[1].conj()),
                                                             np.inner(e[2], h[0].conj()),
                                                             np.inner(e[0], h[2].conj()),
                                                             np.inner(e[0], h[1].conj()),
                                                             np.inner(e[1], h[0].conj())
                                                             ])
        # poynting = np.zeros(3)
        # poynting[0] = (np.inner(e[1], h[2].conj()) - np.inner(e[2], h[1].conj())).real
        # poynting[1] = (np.inner(e[2], h[0].conj()) - np.inner(e[0], h[2].conj())).real
        # poynting[2] = (np.inner(e[0], h[1].conj()) - np.inner(e[1], h[0].conj())).real
        poynting = (self.target_parameters['feature_vector'][::2] - self.target_parameters['feature_vector'][1::2]).real

        return poynting, e
