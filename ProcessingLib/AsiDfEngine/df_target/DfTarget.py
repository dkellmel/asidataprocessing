import logging
from enum import Enum
import numpy as np


class DfTarget(object):
    """
    Class constructor of a DF target

    :param array_pointer: Pointer to the sensor array that provides data for the target
    :param frequency: Frequency of the target
    :param bandwidth: Bandwidth of target signal
    :param target_index: Identifier for the target
    :param technique: Indicates the required processing technique for the target
    :type technique: Techniques
    """

    class Techniques(Enum):
        """
        Enumeration that defines the different target processing techniques
        """

        #: DF processing of target takes place in the time-domain
        time_domain = 0
        #: DF processing of target takes place in the frequency-domain
        frequency_domain = 1
        #: Joint time-frequency processing of the target
        joint_time_frequency = 2

    def __init__(self, array_pointer, frequency, bandwidth, target_index=0, technique=Techniques.frequency_domain):

        # Set up the logger for the class
        self.logger_target = logging.getLogger(name="DfTarget")

        # Define critical parameters
        self.sensor_array = array_pointer
        self.target_parameters = {'records_per_aoa': 1, 'power_threshold': 0.0, 'power_forget_factor': 2,
                                  'data_timeout': 1.5, 'taper_ratio': 0.1, 'taper_window': 'hanning',
                                  'frequency': frequency, 'bandwidth': bandwidth, 'noise_filter_offset': 100e3,
                                  'associated_id': [], 'feature_vector': None,
                                  }
        """
        Dictionary that defines key parameters that govern the processing of the DF target.
        
        :param records_per_aoa: Number of data records used to make an AoA estimate, default: 1
        :param power_threshold: Percent of max-power under which new data is ignored, default: 0
        :param power_forget_factor: Number of data records after which to reset the maximum power if no updates occur,
                                    default: 2
        :param data_timeout: Number of seconds to permit without an update due to low power levels, default: 1.5
        :param taper_ratio: How much larger to make the filter window to allow for taper, default: 0.1
        :param taper_window: Taper window to use at edges of filter window to avoid large time-domain side-lobes, 
                             default: 'hanning'
        :param frequency: Frequency of target
        :param bandwidth: Bandwidth of target
        :param noise_filter_offset: Offset above and below band of interest where noise will be estimated, 
                                    default: 100e3
        :param associated_id: List of target id's at same transmit location, default: []
        :param feature_vector: Set of features used to help determine DF information
        """

        self.processing_technique = technique

        self.target_status = {'maximum_power': 0.0, 'power_low_count': 0, 'instant_power': 0.0, 'low_power': False,
                              }
        """
        Dictionary that reports the status of DF processing for the target.
        
        :param maximum_power: Maximum power registered while computing AoA over multiple data captures
        :param power_low_count: Count of the number of records that the power has fallen beneath the AoA 
                                estimation threshold
        :param instant_power: Maximum power over available DF channels (including any available reference channels)
        :param low_power: Flag that indicates that the signal power falls below parameter: power_threshold
        """

        # Define a set of results reported by the engine
        self.target_results = {'target_index': target_index,  # A reference into the data collection queue
                               'timestamp': None,  # Timestamp for the last valid data set that exceeded the
                                                   #   power threshold
                               'LoB': float('nan'),  # The Line of Bearing to the target
                               'AoA': float('nan'),  # Azimuthal angle of arrival of signal relative
                                                     #   to a level platform
                               'dep': float('nan'),  # Depression angle of arrival of signal relative
                                                     #   to a level platform
                               'snr': 0,  # Un-integrated signal snr
                               'confidence': None,  #
                               }
        """
        Dictionary that reports DF results for the target.
        
        :param target_index: A reference into the to the target id
        :param timestamp: Timestamp for the last valid data set that exceeded the power threshold
        :param LoB: The Line of Bearing to the target, which is the direction of arrival estimate relative to true north 
        :param AoA: Azimuthal angle of arrival of signal relative to the platform orientation
        :param dep: Depression angle of arrival of signal relative to a level platform
        :param snr: Target signal-to-noise ratio
        :param confidence: Place-holder for a calculation of the confidence in the DF estimate
        """

        # Storage for frequency-domain filtering windows
        self.filter_window = np.ones(1)
        self.filter_noise_low = np.ones(1)
        self.filter_noise_high = np.ones(1)

    def filter_cb_data(self, data_in, frequency_vector):
        """
        Filter complex baseband data

        :param data_in: Complex baseband frequency-domain data
        :param frequency_vector: Associated frequency vector
        :type frequency_vector: np.ndarray

        :return: The filtered data
        """

        # Make the filters, if we need to
        if len(self.filter_window) != len(frequency_vector):
            self.filter_window = self._make_filter(self.target_parameters['frequency'], frequency_vector)
            fo = self.target_parameters['frequency'] - self.target_parameters['bandwidth'] - \
                 self.target_parameters['noise_filter_offset']
            self.filter_noise_low = self._make_filter(fo, frequency_vector)
            fo = self.target_parameters['frequency'] + self.target_parameters['bandwidth'] + \
                 self.target_parameters['noise_filter_offset']
            self.filter_noise_high = self._make_filter(fo, frequency_vector)

        # Filter the data
        data_out = data_in * self.filter_window
        data_low = data_in * self.filter_noise_low
        data_high = data_in * self.filter_noise_high

        # Compute powers
        p_signal = (data_out * data_out.conj()).sum(1).max().real
        self.target_status['instant_power'] = p_signal
        p_low = (data_low * data_low.conj()).sum(1).max().real
        p_high = (data_high * data_high.conj()).sum(1).max().real
        p_noise = min(p_low, p_high)
        self.target_results['snr'] = 10 * np.log10(p_signal / p_noise)

        # Transfer back to time-domain if we are processing there
        if self.processing_technique == self.Techniques.time_domain:
            data_out = np.fft.ifft(np.fft.fftshift(data_out, axes=1), axis=1)

        return data_out

    def _make_filter(self, fo, frequency_vector):
        """
        Find noise components of complex-baseband data

        :param fo: Center frequency of filter
        :param frequency_vector: Associated frequency vector
        :type frequency_vector: np.ndarray

        :return: The filter window
        """

        c = len(frequency_vector)

        # Make main part of window
        filter_window = np.zeros(c)
        fs = np.diff(frequency_vector).mean() * c
        filter_size = int((len(frequency_vector) * self.target_parameters['bandwidth']) // fs)
        if filter_size == 0:
            filter_size = 2
        if filter_size % 2 == 1:
            filter_size += 1
        center_index = abs(frequency_vector - fo).argmin()
        index_start = center_index - filter_size // 2
        if index_start < 0:
            index_start = 0
        index_end = center_index + filter_size // 2
        if index_end >= c:
            index_end = c - 1
        filter_window[index_start:index_end] = 1

        # Figure out taper
        taper_length = int(np.ceil(filter_size * self.target_parameters['taper_ratio']))
        if taper_length % 2 == 1:
            taper_length += 1
        try:
            window = getattr(np, self.target_parameters['taper_window'])(taper_length)
            f_2 = filter_size // 2
            t_2 = taper_length // 2
            if center_index - f_2 - t_2 > 0:
                filter_window[center_index - f_2 - t_2:center_index - f_2] = window[:t_2]
            if center_index + f_2 + t_2 < c:
                filter_window[center_index + f_2:center_index + f_2 + t_2] = window[t_2:]
        except AttributeError:
            self.logger_target.error("_make_filter: Invalid window specification: {}. "
                                     "Ignoring taper window".format(self.target_parameters['taper_window']))
        except TypeError:
            self.logger_target.error("_make_filter: Invalid window specification: {}. "
                                     "Ignoring taper window".format(self.target_parameters['taper_window']))

        return filter_window

    def compute_aoa(self, data, polarity=None):
        """
        Stub for calculation of aoa. Each antenna array will approach this differently
        :param data:
        :param polarity:
        :return:
        """

    def process_target(self, data, frequency_vector, timestamp, polarity=None):
        """
        Stub for processing particular targets
        :param data:
        :param frequency_vector:
        :param timestamp:
        :param polarity:
        :return:
        """

        self.target_results['timestamp'] = timestamp

        # Thin the incoming data to remove FFT bins that are not required
        data, frequency_vector = self.thin_data(data, frequency_vector)

        # Filter the data
        # data_filtered = self.filter_cb_data(data, frequency_vector)
        if self.processing_technique != self.Techniques.time_domain:
            data_filtered = self.filter_cb_data(data, frequency_vector)
        else:
            data_filtered = data

        # Compute the angle of arrival
        self.compute_aoa(data_filtered, polarity=polarity)

    def thin_data(self, data, frequency_vector):
        """
        Method to thin the data samples required to process target. The minimum
        data must include the high and low noise filter locations as well,
        which is determined from target bandwidth and noise_filter_offset.
        :param data:
        :param frequency_vector:
        :return (data, frequency_vector)
        """

        low_noise_filter = self.target_parameters['frequency'] - \
                           self.target_parameters['bandwidth'] - \
                           self.target_parameters['noise_filter_offset']
        high_noise_filter = self.target_parameters['frequency'] + \
                            self.target_parameters['bandwidth'] + \
                            self.target_parameters['noise_filter_offset']
        target_bandwidth = self.target_parameters['bandwidth']

        target_start_freq = low_noise_filter - target_bandwidth
        target_stop_freq = high_noise_filter + target_bandwidth
        valid_indices = (frequency_vector >= target_start_freq) & \
                        (frequency_vector <= target_stop_freq)
        target_indices = np.argwhere(valid_indices)[:, 0]

        return data[:, target_indices], frequency_vector[target_indices]

