from ProcessingLib.AsiDfEngine.df_target.DfTarget import DfTarget
import numpy as np
from copy import copy


class ManifoldDfTarget(DfTarget):
    """
    A class that extends DfTarget with fields and algorithms specific to the ASI vector sensor.

    :param array_pointer:
    :type array_pointer: DpaArray
    :param frequency:
    :param bandwidth:
    :param target_index:
    :param technique:
    :type technique: DfTarget.Techniques

    """

    def __init__(self, array_pointer, frequency, bandwidth, target_index=0,
                 technique=DfTarget.Techniques.frequency_domain):

        super(ManifoldDfTarget, self).__init__(array_pointer, frequency, bandwidth, target_index, technique)

        # Add fields to dictionaries
        self.target_parameters.update({'azimuth_lookup': None,  # Interpolation function to adjust aoa estimate
                                       'depression_lookup': None,  # Interpolation function to adjust
                                       #   depression angle estimate
                                       # 'reference_channel': None,  # Index of the reference channel in the data
                                       # 'reference_pair': [0, 1],  # Pair of ports to add to derive a reference if
                                       #   none is provided
                                       })
        self.feature_set = None

    def compute_aoa(self, data, polarity='v_pol'):
        """
        Compute angle of arrival for a target data set.

        :param data:
        :param polarity:
        :return: None
        """

        # Compute angles of arriving signal
        v = self._compute_features(data)
        self.target_results['AoA'], self.target_results['dep'] = self.sensor_array.estimate_angles_from_basis(
            self.target_parameters['frequency'], v, polarity=polarity)

    def _apply_phase_reference(self, data):

        ref_ports = np.array(self.sensor_array.antenna_configuration['reference_ports'])
        ref_weights = np.array(self.sensor_array.antenna_configuration['reference_weights'])

        ports = copy(self.sensor_array.array_parameters['df_channel_ports'])
        ports = np.array(ports).flatten()

        reference = None
        if len(ref_ports) > 1:
            try:
                reference = ref_weights @ data[ref_ports]
            except ValueError:
                self.logger_target.critical("Could not create reference signal for the manifold")
        elif len(ref_ports) == 1:
            try:
                reference = ref_weights * data[ref_ports]
            except IndexError:
                self.logger_target.critical("Could not create reference signal for the manifold")

        if reference is None:
            # reference = np.ones(1, np.complex128)
            data_norm = np.linalg.norm(data, axis=1)
            index = data_norm.argmax()
            reference = data[index]

        v = data[ports] @ reference.conj().T

        if len(v.shape) == 1:
            v = v.reshape(len(v), 1)

        return v

    def _compute_features(self, data):

        # v = self._apply_phase_reference(data)
        ports = self.sensor_array.array_parameters['df_channel_ports']
        v = data
        covariance = v[ports] @ v[ports].conj().T

        indices = np.triu_indices(len(ports))

        features = covariance[indices]
        features = features.reshape(len(features), 1)

        # features = v
        return features
