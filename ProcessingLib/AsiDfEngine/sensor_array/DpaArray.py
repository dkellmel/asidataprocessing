#!/usr/bin/env python
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved
from ProcessingLib.AsiDfEngine.sensor_array.antenna.Dpa import Dpa
import os
import numpy as np
from ProcessingLib.AsiDfEngine.df_target.DpaDfTarget import DpaDfTarget
from ProcessingLib.AsiDfEngine.df_target.DfTarget import DfTarget
from ProcessingLib.AsiDfEngine.sensor_array.SensorArray import SensorArray


class DpaArray(SensorArray):
    """
    A class that extends the SensorArray to arrays of DPA antennas.

    :param configuration_index:
    :param df_channel_ports:
    :param platform:
    :param reference_channel:
    :param reference_active:
    :param load_angle_cal:
    :param lowest_frequency:
    :param highest_frequency:

    """

    antenna_parameter_file = 'dpa_array_config.yml'
    module_path = os.path.dirname(os.path.realpath(__file__))
    calibration_directory = 'dpa_array_calibration'
    default_number_dpa = 3
    default_df_ports = [[0, 1], [2, 3], [4, 5]]

    def __init__(self, configuration_file=None, calibration_base_directory='/opt/asi/settings/',
                 configuration_index=12, df_channel_ports=None,
                 platform='pedestal', reference_channel=7, reference_active=False,
                 load_angle_cal=True, lowest_frequency=2e6, highest_frequency=950e6):

        if configuration_file is not None:
            self.antenna_parameter_file = configuration_file

        if calibration_base_directory is not None:
            self.module_path = calibration_base_directory

        # Call the constructor for the base class
        super(DpaArray, self).__init__(configuration_index, df_channel_ports, platform, reference_channel,
                                       reference_active, load_angle_cal, lowest_frequency, highest_frequency)

        # Update the target definition
        self.target_class = DpaDfTarget

        # Allocate a frequency identifier
        self.center_frequency = None

    def set_antenna_configuration(self, configuration_index, platform, df_channel_ports=None, load_aoa_angle_cal=True):
        """
        Set and initialize the antenna and platform configuration for the antenna array.

        :param configuration_index: Index into the configuration list contained in the antenna configuration file. Or,
            a dictionary that describes a new array configuration.
        :param platform: Identification of the platform on which the antenna array is contained.
        :param df_channel_ports: List of ports associated with corresponding antennas.
            Example for DPA: [[0, 1], [2, 3], [4, 5]]
        :param load_aoa_angle_cal: Flag indicating whether or not to load available AoA calibration data

        :return: True on success, False on Failure
        """

        # Determine the channel ports
        if df_channel_ports is None:
            df_channel_ports = self.default_df_ports

        # Define the default number of DPA's in the array
        self.array_parameters['number_of_elements'] = self.default_number_dpa

        # Define the antenna
        self.antenna = Dpa

        # Call the base level function
        super(DpaArray, self).set_antenna_configuration(configuration_index, platform,
                                                        df_channel_ports, load_aoa_angle_cal)

        # Update fields specific to vector sensor
        self.array_parameters.update({
            'e_field_matrix': np.matrix(self.antenna_configuration['EfieldOrientation']),
            'h_field_matrix': np.matrix(self.antenna_configuration['HfieldOrientation'])
        })

        # Load antenna pattern calibration info
        if 'antenna' in self.antenna_configuration:
            self.antenna_array[0].set_antenna_type(self.antenna_configuration['antenna'])
            for m in range(1, self.array_parameters['number_of_elements']):
                self.antenna_array[m].antenna_parameters['antenna'] = self.antenna_configuration['antenna']

    def process_data(self, data, frequency, timestamp, polarity=None):

        if not isinstance(frequency, (list, np.ndarray)):
            frequency = [frequency]
        frequency = np.array(frequency)

        if len(data.shape) > 2:

            value = False
            poynting = []
            for m in range(data.shape[1]):
                # Do data phase adjustments
                if not self._correct_data(data[:, m], frequency):
                    self.logger_array.error("process_data: Unsuccessful at correcting phase across the band")
                    return False

                for count, target in enumerate(self.target_info['target_list']):
                    if frequency[0] <= target.target_parameters['frequency'] <= frequency[-1]:
                        target.process_target(data[:, m], frequency, timestamp)
                        if m == 0:
                            poynting.append(target.target_parameters['poynting'])
                        else:
                            poynting[count] += target.target_parameters['poynting']
                        value = True
            # count = 0
            for count, target in enumerate(self.target_info['target_list']):
                if frequency[0] <= target.target_parameters['frequency'] <= frequency[-1]:
                    target.target_parameters['poynting'] = poynting[count]
                    target.compute_angles()
        else:
            # Do data phase adjustments
            if not self._correct_data(data, frequency):
                self.logger_array.error("process_data: Unsuccessful at correcting phase across the band")
                return False

            value = super(DpaArray, self).process_data(data, frequency, timestamp)
        return value

    def _correct_data(self, data, frequency):
        """
        Do the processing for the current data capture to fix patterns and remove line-length delays

        :param data: Data from the antenna ports
        :param frequency: Current frequency being processed, can be a scalar or a vector.
            If the frequency is a vector, it is assumed that data is in the frequency-domain, and the frequency
            vector corresponds to the frequencies of the data fft.

        :return: True on success, False when problems were encountered
        """

        output_value = True

        # Determine center frequency of the data
        if isinstance(frequency, list):
            frequency = np.array(frequency)
        elif not isinstance(frequency, np.ndarray):
            frequency = np.array([frequency])
        center_frequency = frequency[len(frequency) // 2]

        # Correct DPA patterns
        if not self._correct_pattern(data, frequency, center_frequency):
            self.logger_array.warning("process_data: Failed to correct DPA patterns")
            output_value = False

        # Correct DPA line phases
        if not self._correct_line_phases(data, frequency):
            self.logger_array.warning("process_data: Failed to correct DPA line phases")
            output_value = False

        return output_value

    def add_target(self, frequency, bandwidth, technique=DfTarget.Techniques.frequency_domain):

        if super(DpaArray, self).add_target(frequency, bandwidth, technique):
            if self.target_info['target_list'][-1].target_parameters['e_field_corrections'] is None:
                self.target_info['target_list'][-1].target_parameters['e_field_corrections'] = \
                    self._get_e_field_correction(frequency)
        else:
            return False
        return True

    def _get_e_field_correction(self, frequency):

        # Determine e_field correction for the frequency
        f1 = frequency
        correct_return = np.ones(self.array_parameters['number_of_elements'])
        pattern_cal = self.antenna_array[0].pattern_cal
        if 'frequency' in pattern_cal and 'efieldInterpFunction' in pattern_cal:
            if f1 < pattern_cal['frequency'][0]:
                f1 = pattern_cal['frequency'][0]
            elif f1 > pattern_cal['frequency'][-1]:
                f1 = pattern_cal['frequency'][-1]
            correct = pattern_cal['efieldInterpFunction'](f1)
            if len(correct) != self.array_parameters['number_of_elements']:
                self.logger_array.warning("compute_aoa: Length of e-field correction coefficients "
                                          "does not match the number of antenna elements.")
            else:
                correct_return = correct

        return correct_return

    def _correct_pattern(self, data, frequency, center_frequency):
        """
        Correct the patterns of the antennas in the array

        :param data: Data from the antenna ports
        :param frequency: Current frequency being processed, can be a scalar or a vector.
            If the frequency is a vector, it is assumed that data is in the frequency-domain, and the frequency
            vector corresponds to the frequencies of the data fft.
        :param center_frequency: The center of the frequency band being processed.

        :return: True on success, False on failure
        """

        # Redetermine correction coefficients, if frequency range has changed
        if center_frequency != self.center_frequency:
            self.center_frequency = center_frequency
            if 'frequency' in self.antenna_array[0].pattern_cal:
                f1 = self.antenna_array[0].pattern_cal['frequency']
                frequency[frequency < f1[0]] = f1[0]
                frequency[frequency > f1[-1]] = f1[-1]

                if self.antenna_array[0].pattern_cal['dataInterpFunction'] is not None:
                    c1 = self.antenna_array[0].pattern_cal['dataInterpFunction'](frequency).T
                    for m in range(self.array_parameters['number_of_elements']):
                        self.antenna_array[m].set_pattern_coefficients(c1[0::2] + 1j * c1[1::2])

        # Perform pattern correction on each DPA
        for m in range(self.array_parameters['number_of_elements']):
            if not self.antenna_array[m].correct_pattern(data):
                self.logger_array.error("_correct_pattern: Could not process data for dpa: {}".format(m))
                return False

        return True

    # TODO: Move this up to SensorArray
    # def _correct_line_phases(self, data, frequency):
    #     """
    #     Do frequency dependent phase correction to remove the cable length effects
    #     :param data: Data from the antenna ports
    #     :param frequency: the frequency that we are tuned to for this calculation, can be a scalar or a vector.
    #         If the frequency is a vector, it is assumed that data is in the frequency-domain,
    #         and the frequency vector corresponds to the frequencies of the data fft.
    #
    #     :return: True on success, False on failure
    #     """
    #     for m in range(self.array_parameters['number_of_elements']):
    #         if not self.antenna_array[m].correct_line_phase(data, frequency):
    #             self.logger_array.error("_correct_line_phases: Could not correct line phases for dpa:{}".format(m))
    #             return False
    #     return True

    # TODO: Modify this to account for vertical offset and multi-dimensional arrays
    def correct_spatial_phases(self, data, frequency, aoa):
        """
        Correct phase difference due to spatial separations on the array

        :param data: Data from the antenna ports
        :param frequency: Target frequency
        :param aoa: Estimate of the angle of arrival of the signal

        :return: True on success, False on failure
        """
        if 'rotation' in self.antenna_configuration:
            rotation = self.antenna_configuration['rotation']
        else:
            rotation = 0
        for m in range(self.array_parameters['number_of_elements']):
            if not self.antenna_array[m].correct_spatial_phase(data, frequency, aoa, rotation):
                self.logger_array.error("_correct_spatial_phases: Could not correct spatial "
                                        "phase for dpa:{}".format(m))
                return False
        return True

    def compute_fields(self, data, reference_index=None):
        """
        Compute E and H fields for all DPA's
        :param data: Data associated with current target
        :param reference_index: Index of reference channel to use. Default: None

        :return: tuple of E and H field components
        """

        e = []
        h = []
        for m in range(self.array_parameters['number_of_elements']):
            x, y = self.antenna_array[m].compute_fields(data, reference_index)
            if x is None or y is None:
                self.logger_array.error("_compute_fields: Failed to compute fields")
                return None, None
            e.append(x)
            h.append(y)
        return np.array(e), np.array(h)


# THIS FUNCTION uses dpa_emulate which is not in scope of the function and needs to be fixed
# before being uncommented
#
# def simulate_turn(domain):
#     angles = np.arange(0, 360, 10)
#     err = []
#     for true_aoa in angles:
#         # da, s_v, f_v = dpa_emulate.make_vertical_tone(fa, ang, int(5.12e3 * 10))
#         da, s_v, f_v = dpa_emulate.make_wideband_vertical_signal(810e6, fa, 5e6, true_aoa, int(5.12e3 * 10))
#         if domain == 'time':
#             dpa_array.process_data(da, [fa], 0.0)
#         else:
#             dpa_array.process_data(s_v, f_v, 0.0)
#         angle_of_arrival = dpa_array.target_info['target_list'][0].target_results['AoA']
#         err.append(angle_of_arrival - true_aoa)
#         print("angle: {:03d}".format(true_aoa))
#
#     err = np.array(err)
#     err[err < -180] += 360
#     err[err > 180] -= 360
#     print("RMS error: {}".format(np.sqrt((err ** 2).mean())))
#     i_err = abs(err).argmax()
#     print("Max error: {0} degrees @ {1} degrees".format(err[i_err], angles[i_err]))


if __name__ == "__main__":

    dpa_array = DpaArray()
    dpa_array.set_antenna_configuration(12, 'pedestal', load_aoa_angle_cal=False)

    # dpa_emulate = DpaArrayEmulator(dpa_array.antenna_configuration)
    # d_nav = {'COMPASS': {'yaw_corrected': 0, 'pitch_corrected': 0, 'roll_corrected': 0}}
    #
    fa = 820e6
    dpa_array.add_target(fa, 15e3, technique=DfTarget.Techniques.time_domain)
    print("\nTime-Domain Processing")
    data1 = np.array([[1.0 + 1j*0.0], [2.0 + 1j*0.0], [3.0 + 1j*0.0], [4.0 + 1j*0.0], [5.0+1j*0.0], [6.0+1j*0.0]])
    dpa_array.process_data(data1, fa, 0)

    # simulate_turn('time')
    #
    # dpa_array.remove_target(fa)
    # dpa_array.add_target(fa, 5e6)
    # print("\nFrequency-Domain Processing")
    # simulate_turn('frequency')
