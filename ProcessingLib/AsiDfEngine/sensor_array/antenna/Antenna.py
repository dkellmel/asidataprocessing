# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved
import logging
import numpy as np


class Antenna(object):
    """
    Class that defines a general antenna.

    :param number_of_ports: Number of ports that the antenna has
    :param cal_directory: Directory that contains the calibration files associated with the antenna
    :param antenna_type: A description of the antenna type or model

    """

    def __init__(self, number_of_ports=1, cal_base_directory=None, cal_directory='', antenna_type=None):

        # Set up logger
        self.logger_antenna = logging.getLogger(name='Antenna')

        self.number_of_ports = number_of_ports  # Number of ports contained within the antenna

        self.antenna_parameters = {'antenna': '',  # Antenna model
                                   'channel_indices': [],  # Indices of the data rows for the antenna port(s)
                                   'line_delay': np.zeros((self.number_of_ports, 1)),  # Delays from cable lengths
                                                                                       #   running to each port
                                   'spatial_delay': 0,  # Spacial delay to the antenna element
                                   }
        """
        Dictionary that stores key antenna 

        :param antenna: Antenna model
        :param channel_indices: 
        """

        self.calibration_base_directory = cal_base_directory
        self.calibration_directory = cal_directory

        if antenna_type is not None:
            self.set_antenna_type(antenna_type)

    def set_antenna_type(self, antenna_type):
        """
        Set the antenna type

        :param antenna_type: Descriptor for the antenna. Example: '4inCyl003'

        :return: Antenna type

        """

        self.antenna_parameters['antenna'] = antenna_type
        return antenna_type

    def set_channel_indices(self, port_indices):
        """
        Set the indices of the data array that correspond to the antenna ports

        :param port_indices: Indices of the data rows that correspond to the antenna ports

        :return: True on success, false on failure

        """

        if not isinstance(port_indices, list) and not isinstance(port_indices, np.ndarray):
            port_indices = [port_indices]

        if len(port_indices) != self.number_of_ports:
            self.logger_antenna.error("set_channel_indices: the length of the port indices vector must match the "
                                      "number of ports for the antenna element")
            return False
        self.antenna_parameters['channel_indices'] = port_indices

        return True

    def set_line_delays(self, delays):
        """
        Set the line delays for the antenna element

        :param delays: Vector of delays, in seconds, corresponding to each port. If a single delay is passed in, it will be applied to all ports.

        :return: The antenna parameter dictionary

        """

        if not isinstance(delays, list) and not isinstance(delays, np.ndarray):
            delays = [delays]

        if len(delays) == 1:
            delays = [delays[0]] * self.number_of_ports

        if len(delays) != self.number_of_ports:
            self.logger_antenna.error("set_line_delays: number of delays mismatched to number of ports")
        else:
            self.antenna_parameters['line_delay'] = np.array(delays)

        if len(self.antenna_parameters['line_delay'].shape) == 1:
            self.antenna_parameters['line_delay'] = \
                self.antenna_parameters['line_delay'].reshape(self.number_of_ports, 1)

        return self.antenna_parameters

    def correct_line_phase(self, data, frequency):
        """
        Do frequency dependent phase correction to remove the cable length effects

        :param data: Data from the antenna ports

        :param frequency: the frequency that we are tuned to for this calculation, can be a scalar or a vector.
            If the frequency is a vector, it is assumed that data is in the frequency-domain,
            and the frequency vector corresponds to the frequencies of the data fft.

        :return: True on success, False on failure

        """

        if not isinstance(frequency, (list, np.ndarray)):
            frequency = [frequency]

        if len(frequency) != data.shape[1]:
            # noinspection PyTypeChecker
            frequency = np.full(data.shape[1], frequency)

        try:
            data[self.antenna_parameters['channel_indices']] *= np.exp(1j * 2 * np.pi * frequency *
                                                                       self.antenna_parameters['line_delay'])
        except IndexError or TypeError:
            self.logger_antenna.error("correct_line_phases: data has the wrong dimensions")
            return False

        return True

    # TODO: Modify this to account for vertical offset and multi-dimensional arrays
    def correct_spatial_phase(self, data, frequency, aoa, rotation):
        """
        Correct phase difference in this element due to position in array

        :param data: Data from the antenna ports

        :param frequency: Target frequency

        :param aoa: Estimate of the angle of arrival of the signal

        :param rotation: Angle array is rotated off of direction with maximum linear separation

        :return: True on success, False on failure

        """

        phase_adjust = 2 * np.pi * frequency * self.antenna_parameters['spatial_delay'] * \
                       np.cos(np.deg2rad(aoa - rotation))

        try:
            data[self.antenna_parameters['channel_indices']] *= np.exp(1j * phase_adjust)
        except IndexError:
            self.logger_antenna.error("correct_spatial_phase: data has the wrong dimensions")
            return False

        return True
