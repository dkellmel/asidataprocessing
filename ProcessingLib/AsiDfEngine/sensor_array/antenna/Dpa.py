# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved
import numpy as np
import logging
import os
import yaml
try:
    from yaml import CSafeLoader as SafeLoader, CSafeDumper as SafeDumper
except ImportError:
    from yaml import SafeLoader, SafeDumper
from scipy.interpolate import interp1d
from ProcessingLib.AsiDfEngine.sensor_array.antenna.Antenna import Antenna


class Dpa(Antenna):
    """
    Define the base class for a DPA

    :param number_of_ports: Number of port that the antenna has
    :param cal_directory: Directory that contains the calibration information for the antenna
    :param antenna_type: Descriptor for the antenna. Example: '4inCyl003'. Default: None
    """

    module_path = os.path.dirname(os.path.realpath(__file__))
    """
    Stores the path to the module
    """

    pattern_calibration_suffix = '_pattern_cal.yml'
    """
    Define last component of the filename for pattern calibration data
    """

    def __init__(self, number_of_ports=None, cal_base_directory=None, cal_directory='', antenna_type=None):

        # Set up class logger
        self.logger_dpa = logging.getLogger(name='Dpa')

        if cal_base_directory is not None:
            self.module_path = cal_base_directory

        # Initialize some parameters
        self.pattern_cal = dict()  # A dictionary used to store pattern calibration data

        # Initialize the base class
        if number_of_ports is None:
            number_of_ports = 2
        super(Dpa, self).__init__(number_of_ports, cal_base_directory, cal_directory, antenna_type)

        # Update and extend definitions of key parameter dictionary
        self.antenna_parameters.update({
            'pattern_correction_coefficients': [],  # Correction factors applied to the ports
            'e_field_correction': 1,  # Gain factor for the e-field so that it matches h-field
        })

    def set_antenna_type(self, antenna_type):
        """
        Set the antenna type and load the associated pattern calibration data

        :param antenna_type: Descriptor for the antenna. Example: '4inCyl003'
        :return: Antenna type
        """
        self.antenna_parameters['antenna'] = antenna_type
        self._load_dpa_calibration()
        return antenna_type

    def compute_fields(self, data, reference_index=None):
        """
        Compute the e and h fields for the antenna

        :param data: Data from the antenna ports
        :param reference_index: Index of the reference channel

        :return: e and h fields
        """

        # Compute fields
        try:
            e = (data[self.antenna_parameters['channel_indices'][1]] +
                 data[self.antenna_parameters['channel_indices'][0]]) * self.antenna_parameters['e_field_correction']
            h = data[self.antenna_parameters['channel_indices'][1]] - \
                data[self.antenna_parameters['channel_indices'][0]]
        except TypeError as err:
            self.logger_dpa.error("compute_fields: {}".format(err))
            e = h = None
        except IndexError:
            self.logger_dpa.error("compute_fields: data has the wrong dimensions")
            e = h = None

        if reference_index is not None and e is not None and h is not None:
            e = np.inner(data[reference_index].conj(), e)
            h = np.inner(data[reference_index].conj(), h)

        return e, h

    """
    The following functions go to DPA pattern correction 
    """

    def set_pattern_coefficients(self, pattern_coefficients):
        """
        Set the pattern correction coefficients for the antenna ports.

        :param pattern_coefficients: The pattern correction parameters for the port

        :return: Returns the pattern coefficients that were stored.
        """

        if len(pattern_coefficients.shape) == 1:
            pattern_coefficients = pattern_coefficients.reshape(len(pattern_coefficients), 1)
        self.antenna_parameters['pattern_correction_coefficients'] = pattern_coefficients

        return self.antenna_parameters['pattern_correction_coefficients']

    def correct_pattern(self, data):
        """
        Correct the pattern of the antenna by applying coefficients to the ports

        :param data: Data from the antenna ports

        :return: True on success, False on failure
        """

        if len(self.antenna_parameters['pattern_correction_coefficients']) == 0:
            return True

        try:
            data[self.antenna_parameters['channel_indices']] *= \
                self.antenna_parameters['pattern_correction_coefficients']
        except ValueError:
            self.logger_dpa.error("correct_pattern: data has the wrong dimensions")
            return False

        return True

    def _load_dpa_calibration(self):
        """
        Load the calibration data associated with the current DPA

        :return: None
        """

        filename = self.antenna_parameters['antenna'] + self.pattern_calibration_suffix
        pattern_calibration_file = os.path.join(self.module_path, self.calibration_directory, filename)

        try:
            with open(pattern_calibration_file, 'r') as stream:
                pattern_cal = yaml.load(stream, Loader=SafeLoader)
                if 'frequency' in pattern_cal and 'data_weight' in pattern_cal:
                    self.pattern_cal.update({'frequency': pattern_cal['frequency']})
                    self.pattern_cal.update({'dataInterpFunction': interp1d(self.pattern_cal['frequency'],
                                                                            pattern_cal['data_weight'],
                                                                            axis=0)})
                else:
                    self.pattern_cal.update({'dataInterpFunction': None})

                if 'frequency' in self.pattern_cal and 'E_weight' in pattern_cal:
                    self.pattern_cal.update({'efieldInterpFunction': interp1d(self.pattern_cal['frequency'],
                                                                              pattern_cal['E_weight'],
                                                                              axis=0)})
                else:
                    self.pattern_cal.update({'efieldInterpFunction': None})
            self.logger_dpa.info("Loaded pattern calibration file: {}".format(filename))
        except IOError:
            self.pattern_cal = dict()
            self.logger_dpa.warning(' Problem loading pattern calibration file: {0}'.format(pattern_calibration_file))
        except KeyError as err:
            self.pattern_cal = dict()
            self.logger_dpa.warning('Problem loading pattern calibration file. Error: {}'.format(err))
