import numpy as np
from ProcessingLib.AsiDfEngine.sensor_array.SensorArray import SensorArray
from ProcessingLib.AsiDfEngine.df_target.ManifoldDfTarget import ManifoldDfTarget
from ProcessingLib.AsiDfEngine.sensor_array.antenna.Antenna import Antenna
from scipy.interpolate import interp2d, interp1d
from copy import copy

try:
    from yaml import CSafeLoader as SafeLoader, CSafeDumper as SafeDumper
except ImportError:
    from yaml import SafeLoader, SafeDumper
import os


class Basis(object):

    def __init__(self):

        self.basis = None
        self.angle_lookup = []
        self.p = None
        self.weight = np.diag(np.ones(1))

        self.est_parameters = {'azimuth': np.zeros(1), 'depression': np.zeros(1),
                               'az_res': 0.0, 'dep_res': 0.0,
                               }

    def generate_basis(self, data_in, azimuth, depression, az_spacing_up=0.5, dep_spacing_up=0.5):

        # Compute resulting azimuth-depression grid
        self.est_parameters['azimuth'] = np.arange(0, 360 + az_spacing_up / 2, az_spacing_up)
        self.est_parameters['az_res'] = az_spacing_up
        if len(depression) > 1:
            self.est_parameters['depression'] = np.arange(depression[0], depression[-1] + dep_spacing_up / 2,
                                                          dep_spacing_up)
        else:
            self.est_parameters['depression'] = depression
        self.est_parameters['dep_res'] = dep_spacing_up

        data = data_in.copy()
        basis = data.copy()

        az = copy(azimuth)
        if azimuth[0] > 0:
            basis = np.insert(basis, 0, data[:, -1], axis=1)
            az = np.insert(az, 0, azimuth[-1] - 360)
        if azimuth[-1] < 360:
            basis = np.insert(basis, len(az), data[:, 0], axis=1)
            az = np.insert(az, len(az), azimuth[0] + 360)

        azimuth = az

        # Interpolate the basis sets
        m1 = len(self.est_parameters['azimuth'])
        n = len(self.est_parameters['depression'])
        mn = m1 * n
        self.basis = np.zeros((basis.shape[-1], mn), np.complex128)
        self.weight = np.diag(np.ones(basis.shape[-1]))

        for m in range(basis.shape[-1]):
            if n > 1:
                self.basis[m] = interp2d(azimuth, depression, basis[:, :, m].real)(
                    self.est_parameters['azimuth'], self.est_parameters['depression']).reshape(mn)
                self.basis[m] += 1j * interp2d(azimuth, depression, basis[:, :, m].imag)(
                    self.est_parameters['azimuth'], self.est_parameters['depression']).reshape(mn)
            else:
                self.basis[m] = interp1d(azimuth, basis[:, :, m].real,
                                         fill_value="extrapolate")(self.est_parameters['azimuth']).reshape(mn)
                self.basis[m] += 1j * interp1d(azimuth, basis[:, :, m].imag, fill_value="extrapolate")(
                    self.est_parameters['azimuth']).reshape(mn)

        self.basis = self.weight @ self.basis
        self.basis /= np.linalg.norm(self.basis, axis=0)
        self.basis = self.basis.T

        # Create an azimuth-depression angle lookup table
        self.angle_lookup = []
        for d in self.est_parameters['depression']:
            for a in self.est_parameters['azimuth']:
                self.angle_lookup.append((a, d))
        self.angle_lookup = np.array(self.angle_lookup)

    def estimate_angles(self, port_voltages, depression=None):

        v = port_voltages

        # Now do high-resolution projection
        v1 = self.weight @ v
        if depression is None:
            self.p = (self.basis @ v1.conj()).real
            index = self.p.argmax(axis=0)
            values = self.angle_lookup[index]
        else:
            i1 = np.where(self.angle_lookup[:, 1] == depression)[0]
            self.p = (self.basis[i1] @ v1.conj()).real
            # Find the max projection
            index = self.p.argmax(axis=0)
            values = self.angle_lookup[i1[index]]

        if len(values.shape) == 2:
            return values[:, 0], values[:, 1]
        else:
            return values[0], values[1]


class ManifoldArray(SensorArray):
    antenna_parameter_file = 'dpa_array_config.yml'
    # module_path = os.path.dirname(os.path.realpath(__file__))
    module_path = '/opt/asi/settings'
    calibration_directory = 'manifold_array_calibration'
    default_df_ports = [0, 1, 2, 3, 4, 5]

    def __init__(self, configuration_file=None, configuration_index=12, df_channel_ports=None,
                 platform='pedestal', reference_channel=7, reference_active=False,
                 load_angle_cal=True, lowest_frequency=2e6, highest_frequency=950e6):

        if configuration_file is not None:
            self.antenna_parameter_file = configuration_file

        self.manifold_data = {}
        self.manifold_parameters = {'manifold_file': ''}

        # Call the constructor for the base class
        super(ManifoldArray, self).__init__(configuration_index, df_channel_ports, platform, reference_channel,
                                            reference_active, load_angle_cal, lowest_frequency, highest_frequency)

        # Update the target definition
        self.target_class = ManifoldDfTarget

        # Allocate a frequency identifier
        self.center_frequency = None

        self.basis = {'v_pol': {'frequency': [], 'basis': []},
                      'h_pol': {'frequency': [], 'basis': []}}

    def estimate_angles_from_basis(self, frequency, v, polarity='v_pol',
                                   az_spacing_up=0.5, dep_spacing_up=0.5, depression=None):

        if self.manifold_data is None:
            self.logger_array.warning("No manifold calibration data available")
            return None, None

        if polarity not in self.manifold_data['basis']:
            self.logger_array.warning("The requested polarity: {}, was not in the provided manifold data: {}".format(
                polarity, self.manifold_parameters['manifold_file']))
            return None, None

        if polarity not in self.basis:
            self.basis.update({polarity: {'frequency': [], 'basis': []}})

        if frequency not in self.basis[polarity]['frequency']:

            n_d = len(self.manifold_data['cal_depression'])
            n_a = len(self.manifold_data['cal_azimuth'])
            data = np.zeros((n_d, n_a, self.manifold_data['number_features']), np.complex128)
            for m in range(n_d):
                for n in range(n_a):
                    try:
                        x = self.manifold_data['basis'][polarity][m][n]['w_r'](frequency)
                        y = self.manifold_data['basis'][polarity][m][n]['w_i'](frequency)
                    except KeyError:
                        self.logger_array.warning("Mismatch in calibration data, polarity: {0}, "
                                                  "depression: {1}, and azimuth: {2}.".format(polarity, m, n))
                        return None, None
                    data[m, n] = (x + 1j * y).T

            self.basis[polarity]['frequency'].append(frequency)
            self.basis[polarity]['basis'].append(Basis())
            self.basis[polarity]['basis'][-1].generate_basis(data, self.manifold_data['cal_azimuth'],
                                                             self.manifold_data['cal_depression'],
                                                             az_spacing_up=az_spacing_up,
                                                             dep_spacing_up=dep_spacing_up)

        i = np.where(np.array(self.basis[polarity]['frequency']) == frequency)[0][0]

        az, el = self.basis[polarity]['basis'][i].estimate_angles(v, depression)

        return az, el

    def set_antenna_configuration(self, configuration_index, platform, df_channel_ports=None, load_aoa_angle_cal=True):
        """
        Set and initialize the antenna and platform configuration for the antenna array.

        :param configuration_index: Index into the configuration list contained in the antenna configuration file. Or,
            a dictionary that describes a new array configuration.
        :param platform: Identification of the platform on which the antenna array is contained.
        :param df_channel_ports: List of ports associated with corresponding antennas.
            Example for DPA: [[0, 1], [2, 3], [4, 5]]
        :param load_aoa_angle_cal: Flag indicating whether or not to load available AoA calibration data

        :return: True on success, False on Failure
        """

        # Determine the channel ports
        if df_channel_ports is None:
            df_channel_ports = self.default_df_ports

        # Define the default number of antennas in the array
        self.array_parameters['number_of_elements'] = len(df_channel_ports)

        # Define the antenna
        self.antenna = Antenna

        # Call the base level function
        super(ManifoldArray, self).set_antenna_configuration(configuration_index, platform,
                                                             df_channel_ports, load_aoa_angle_cal)

        # Load array calibration info and set antenna type
        if 'antenna' in self.antenna_configuration:
            for m in range(self.array_parameters['number_of_elements']):
                self.antenna_array[m].antenna_parameters['antenna'] = self.antenna_configuration['antenna']
        manifold_file = self.antenna_configuration['antenna'] + '_{}'.format(self.array_parameters['platform'])
        manifold_file += '_manifold.npz'
        self.manifold_parameters['manifold_file'] = os.path.join(self.module_path, self.calibration_directory,
                                                                 manifold_file)
        try:
            data = np.load(self.manifold_parameters['manifold_file'],allow_pickle=True)
            self.manifold_data['cal_depression'] = data['cal_depression']
            self.manifold_data['cal_azimuth'] = data['cal_azimuth']
            if 'number_ports' in data:
                self.manifold_data['number_ports'] = data['number_ports']
            else:
                self.manifold_data['number_ports'] = self.array_parameters['number_of_elements']
            self.manifold_data['basis'] = data['basis'].item()
            n_ports = self.manifold_data['number_ports']
            self.manifold_data['number_features'] = (n_ports * (n_ports + 1)) // 2
            # self.manifold_data['number_features'] = n_ports

        # try:
        #     with open(self.manifold_parameters['manifold_file'], 'r') as stream:
        #         self.manifold_data = yaml.load(stream, Loader=SafeLoader)
        except IOError:
            self.logger_array.critical("Failed to load manifold data: {}".format(
                self.manifold_parameters['manifold_file']))

    def process_data(self, data, frequency, timestamp, polarity='v_pol'):

        if not isinstance(frequency, (list, np.ndarray)):
            frequency = [frequency]
        frequency = np.array(frequency)

        if not self._correct_data(data, frequency):
            self.logger_array.warning("process_data: Unsuccessful at correcting phase across the band")
            return False

        value = super(ManifoldArray, self).process_data(data, frequency, timestamp, polarity=polarity)

        return value

    def _correct_data(self, data, frequency):

        output_value = True

        # Determine frequency vector for the data
        if isinstance(frequency, list):
            frequency = np.array(frequency)
        elif not isinstance(frequency, np.ndarray):
            frequency = np.array([frequency])

        # Correct line phases
        if not self._correct_line_phases(data, frequency):
            self.logger_array.warning("process_data: Failed to correct DPA line phases")
            output_value = False

        return output_value
