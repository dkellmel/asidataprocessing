import logging
from ProcessingLib.AsiDfEngine.df_target.DfTarget import DfTarget
from ProcessingLib.AsiDfEngine.sensor_array.antenna.Antenna import Antenna
from scipy.interpolate import interp1d
import numpy as np
import threading
import time
import glob
import os
import yaml
try:
    from yaml import CSafeLoader as SafeLoader, CSafeDumper as SafeDumper
except ImportError:
    from yaml import SafeLoader, SafeDumper


class SensorArray(object):
    """
    Class constructor for the SensorArray base class

    :param configuration_index: Index of entry in array configuration yaml that describes the array.
    :param df_channel_ports:
    :param platform:
    :param reference_channel:
    :param reference_active:
    :param load_angle_cal:
    :param lowest_frequency:
    :param highest_frequency:
    """

    initial_target_id = 1000
    antenna_parameter_file = ''  # Should be overwritten by inheriting class
    module_path = ''             # Should be overwritten by inheriting class
    calibration_directory = ''   # Should be overwritten by inheriting class

    def __init__(self, configuration_index=12, df_channel_ports=None,
                 platform='pedestal', reference_channel=7, reference_active=False,
                 load_angle_cal=True, lowest_frequency=2e6, highest_frequency=950e6):

        # Set up the logger for the class
        self.logger_array = logging.getLogger(name='SensorArray')

        # Define parameters for the DpaArray
        self.array_parameters = {
            'platform': None,
            'number_of_elements': 0,  # Number elements in an array
            'df_channel_ports': [],  # Channel pairings used for df
            'reference_channel': reference_channel,  # Reference channel used to increase SNR of estimate
            'reference_active': reference_active,  # Flag that indicates if reference channel is to be used
            'frequency_coverage': {'f_low': lowest_frequency,
                                   'f_high': highest_frequency, }  # Frequency range covered by array
        }

        # Storage for AoA calibration data
        self.aoa_calibration = dict()

        # Load the parameters associated with the sensor array
        self.antenna_configuration = dict()
        filename = os.path.join(self.module_path, self.antenna_parameter_file)
        try:
            with open(filename, 'r') as stream:
                self.antenna_configuration_list = yaml.load(stream, Loader=SafeLoader)
        except IOError:
            self.logger_array.warning("Could not load parameter file: {}".format(filename))
            self.antenna_configuration_list = None

        # Define the antenna used in the array
        self.antenna = Antenna

        # Define a set of parameters for storing target information
        self.target_info = {
            'next_target_id': self.initial_target_id,
            'target_list': [],
            'frequency_list': []
        }
        self.target_class = DfTarget

        # Define array of antenna elements
        self.antenna_array = []

        # Configure the array
        self.set_antenna_configuration(configuration_index, platform, df_channel_ports, load_angle_cal)

    def set_antenna_configuration(self, configuration_index, platform, df_channel_ports, load_aoa_angle_cal=True):
        """
        Set and initialize the antenna and platform configuration for the antenna array.

        :param configuration_index: Index into the configuration list contained in the antenna configuration file. Or a
            new array configuration dictionary.
        :param platform: Identification of the platform on which the antenna array is contained.
        :param df_channel_ports: List of ports associated with corresponding antennas.
            Example for DPA: [[0, 1], [2, 3], [4, 5]]
        :param load_aoa_angle_cal: Flag indicating whether or not to load available AoA calibration data

        :return: True on success, False on Failure
        """

        # Store the platform
        self.array_parameters['platform'] = platform

        # Select the specified configuration
        if isinstance(configuration_index, dict):
            self.antenna_configuration = configuration_index
        elif self.antenna_configuration_list is not None:
            if configuration_index < len(self.antenna_configuration_list):
                self.antenna_configuration = self.antenna_configuration_list[configuration_index]
                self.antenna_configuration.update({'configuration_index': configuration_index})
            else:
                self.logger_array.error("set_antenna_configuration: configuration index can be no greater than {}. "
                                        "A value of {} was entered".format(len(self.antenna_configuration_list) - 1,
                                                                           configuration_index))
                return False
        else:
            self.antenna_configuration = \
                {'description': "Parameters for 8 inch ground station test setup: 3D",
                 'configuration_index': 12,
                 'dfDimension': 3,
                 'number_elements': 3,
                 'antenna': '8inCyl002',
                 'rotation': 0,
                 'antennaSpacialDelays': [0, 0, 0],
                 'antennaLineDelays': [9.955e-9, 7.945e-9, 5.0e-9],
                 'EfieldOrientation': [[0, 0, -1],
                                       [0, -1, 0],
                                       [1, 0, 0]],
                 'HfieldOrientation': [[0, -1, 0],
                                       [-1, 0, 0],
                                       [0, 0, 1]]
                 }

        # Update the number of elements
        if 'number_elements' in self.antenna_configuration:
            self.array_parameters['number_of_elements'] = self.antenna_configuration['number_elements']
        self.array_parameters['df_channel_ports'] = [None] * self.array_parameters['number_of_elements']

        # Define array of antennas
        self.antenna_array = []
        for m in range(self.array_parameters['number_of_elements']):
            self.antenna_array.append(self.antenna(cal_base_directory=self.module_path,
                                                   cal_directory=self.calibration_directory))

        # Assign ports to each element in the array
        if not self.set_df_channel_ports(df_channel_ports):
            self.logger_array.warning("init: DF channel ports not properly set up")

        # Set the line-length delays for each of the antenna elements
        if 'antennaLineDelays' in self.antenna_configuration:
            if len(self.antenna_configuration['antennaLineDelays']) != self.array_parameters['number_of_elements']:
                self.logger_array.warning("set_antenna_configuration: line delay vector is not the same "
                                          "length as the number of DPAs")
            else:
                for m in range(self.array_parameters['number_of_elements']):
                    self.antenna_array[m].set_line_delays(self.antenna_configuration['antennaLineDelays'][m])

        # Set the spacial delays for each element in the array
        if 'antennaSpacialDelays' in self.antenna_configuration:
            if len(self.antenna_configuration['antennaSpacialDelays']) != self.array_parameters['number_of_elements']:
                self.logger_array.warning("set_antenna_configuration: spacial delay vector is not the same "
                                          "length as the number of DPAs")
            else:
                for m in range(self.array_parameters['number_of_elements']):
                    self.antenna_array[m].antenna_parameters['spatial_delay'] = \
                        self.antenna_configuration['antennaSpacialDelays'][m]

        # Load AoA calibration data
        if load_aoa_angle_cal:
            self.load_aoa_cal_data()
        else:
            self.aoa_calibration = dict()

    def load_aoa_cal_data(self):
        """
        Load available angle calibration data

        :return: None
        """

        # Load AoA calibration data
        aoa_cal_load = CalibrationLoad(os.path.join(self.module_path, self.calibration_directory),
                                       self.antenna_configuration['antenna'], self.array_parameters['platform'],
                                       self.aoa_calibration, self.logger_array)
        aoa_cal_load.start()
        aoa_cal_load.join()

    def process_data(self, data, frequency, timestamp, polarity=None):
        """

        :param data:
        :param frequency:
        :param timestamp:
        :param polarity:
        :return:
        """
        value = False
        for target in self.target_info['target_list']:
            if frequency[0] <= target.target_parameters['frequency'] <= frequency[-1]:
                target.process_target(data, frequency, timestamp, polarity=polarity)
                value = True
        return value

    def set_df_channel_ports(self, channel_ports):
        """
        Set receiver ports associated with each antenna

        :param channel_ports: List of port pairs associated with corresponding antennas.
            example:  [[0, 1], [2, 3], [4, 5]] for DPA, could be [[0], [1], [2], [3]] for
            a different array type.

        :return: True on success, False on failure
        """

        if len(channel_ports) != self.array_parameters['number_of_elements']:
            self.logger_array.error("set_df_channel_ports: The number of channel port sets must match "
                                    "the number of elements: {}".format(self.array_parameters['number_of_elements']))
            return False

        try:
            for m in range(self.array_parameters['number_of_elements']):
                if not self.antenna_array[m].set_channel_indices(channel_ports[m]):
                    self.logger_array.error("set_df_channel_ports: Unable to set ports for "
                                            "DPA {}".format(m))
                    return False
                self.array_parameters['df_channel_ports'][m] = channel_ports[m]
        except TypeError:
            self.logger_array.error("set_df_channel_ports: Each member of channel ports must be a list of ports")
            return False

        return True

    def add_target(self, frequency, bandwidth, technique=DfTarget.Techniques.frequency_domain):
        """
        Stub for adding a target to the target array. This code should be overridden in
        inheriting class if the DfTarget class has been overridden.
        :param frequency:
        :param bandwidth:
        :param technique:
        :return:
        """
        # Make sure target is within array frequency range
        if frequency < self.array_parameters['frequency_coverage']['f_low'] or \
                frequency > self.array_parameters['frequency_coverage']['f_high']:
            self.logger_array.warning("add_target: Frequency {} is out of range for array".format(frequency))
            return False

        # Add target if it's not already on the list
        if frequency not in self.target_info['frequency_list']:
            self.target_info['frequency_list'].append(frequency)
            self.target_info['target_list'].append(self.target_class(self, frequency, bandwidth,
                                                                     self.target_info['next_target_id'],
                                                                     technique))
            self.target_info['next_target_id'] += 1
            tt = self.target_info['target_list'][-1].target_parameters
            # TODO: Fix following statements
            tt['azimuth_lookup'] = self._get_aoa_interpolation(frequency)
            # tt['depression_lookup'] = self._get_depression_interpolation(frequency)
            # tt['e_field_corrections'] = self._get_e_field_correction(frequency)
        else:
            self.logger_array.info("add_target: Target with frequency identifier {} "
                                   "already in target list.".format(frequency))
        return True

    def remove_target(self, frequency):

        if frequency in self.target_info['frequency_list']:
            self.target_info['frequency_list'].remove(frequency)
            for target in self.target_info['target_list']:
                if target.target_parameters['frequency'] == frequency:
                    self.target_info['target_list'].remove(target)
                    break
        else:
            self.logger_array.info("remove_target: No target in list with frequency {}.".format(frequency))

        return True

    def _get_aoa_interpolation(self, frequency):
        """
        Returns the pointer to an aoa interpolation function

        :param frequency: Frequency for which the interpolation function is derived

        :return: Pointer to an aoa interpolation function
        """
        g1 = None
        if 'frequency' in self.aoa_calibration and 'cal_vectors' in self.aoa_calibration:
            f1 = self.aoa_calibration['frequency']
            fx = frequency / 1e6

            # Use primary cal vectors to create a cal lookup
            p1 = np.where(f1 <= fx)[0]
            if len(p1) == 0:
                p1 = 0
            else:
                p1 = p1[-1]
            f_low = f1[p1]
            cal_low = self.aoa_calibration['cal_vectors'][p1]

            p2 = np.where(f1 > fx)[0]
            if len(p2) == 0:
                p2 = len(f1) - 1
            else:
                p2 = p2[0]
            f_high = f1[p2]
            cal_high = self.aoa_calibration['cal_vectors'][p2]

            if f_low != f_high:
                a = (fx - f_low) / (f_high - f_low)
                if a < 0:
                    a = 0.0
                elif a > 1:
                    a = 1.0
            else:
                a = 0.0

            cal = (1.0 - a) * cal_low + a * cal_high
            self.logger_array.info("Interp. cal. vector {0}<-> MHz".format(f_low, f_high))

            # Now use adjustment calibration as necessary
            if 'adjust_frequency' in self.aoa_calibration and 'adjust_vectors' in self.aoa_calibration:
                f1 = self.aoa_calibration['adjust_frequency']
                p1 = np.where(f1 == int(fx))[0]
                if len(p1) > 0:
                    cal_adjust = self.aoa_calibration['adjust_vectors'][p1[0]]
                    g2 = interp1d(np.arange(360), cal_adjust)
                    self.logger_array.info("Using cal. adjust file for {} MHz".format(int(fx)))
                    cal += g2(cal)
            g1 = interp1d(np.arange(360), cal)
        return g1

    def _get_depression_interpolation(self, frequency):

        self.logger_array.info("_get_depression_interpolation: Depression angle correction not yet "
                               "implemented. Frequency requested: {}".format(frequency))

        return None

    def _correct_line_phases(self, data, frequency):
        """
        Do frequency dependent phase correction to remove the cable length effects
        :param data: Data from the antenna ports
        :param frequency: the frequency that we are tuned to for this calculation, can be a scalar or a vector.
            If the frequency is a vector, it is assumed that data is in the frequency-domain,
            and the frequency vector corresponds to the frequencies of the data fft.

        :return: True on success, False on failure
        """
        for m in range(self.array_parameters['number_of_elements']):
            if not self.antenna_array[m].correct_line_phase(data, frequency):
                self.logger_array.error("_correct_line_phases: Could not correct line phases for dpa:{}".format(m))
                return False
        return True


class CalibrationLoad(threading.Thread):
    def __init__(self, cal_directory, antenna_model, antenna_platform, cal_data, logger):
        threading.Thread.__init__(self)

        self.filename = os.path.join(cal_directory, antenna_model + '_*' + antenna_platform + '.yml')
        self.filename_adjust = os.path.join(cal_directory, 'adjust_*_' + antenna_platform + '.yml')
        self.cal_data = cal_data
        self.logger = logger

    def run(self):
        cal_start = time.time()

        # Load calibration files
        file_list = glob.glob(self.filename)
        n = len(file_list)
        if n > 0:
            self.cal_data['frequency'] = np.zeros(n)
            for m in range(n):
                _, file_name = os.path.split(file_list[m])
                index = file_name.find('_')
                l_freq = file_name[index + 1:].find('_')
                self.cal_data['frequency'][m] = float(file_name[index + 1:index + l_freq + 1])

            index = self.cal_data['frequency'].argsort()
            self.cal_data['frequency'] = self.cal_data['frequency'][index]

            file_list = np.array(file_list)[index]
            self.cal_data['cal_vectors'] = np.zeros((n, 360))
            for m in range(n):
                with open(file_list[m], 'r') as stream:
                    cv = yaml.load(stream, Loader=SafeLoader)
                self.cal_data['cal_vectors'][m] = np.array(cv['angleCal'])

        # Load adjustment files
        file_list = glob.glob(self.filename_adjust)
        n = len(file_list)
        if n > 0:
            self.cal_data['adjust_frequency'] = np.zeros(n, int)
            for m in range(n):
                _, file_name = os.path.split(file_list[m])
                index = file_name.find('_')
                l_freq = file_name[index + 1:].find('_')
                self.cal_data['adjust_frequency'][m] = int(file_name[index + 1:index + l_freq + 1])

            index = self.cal_data['adjust_frequency'].argsort()
            self.cal_data['adjust_frequency'] = self.cal_data['adjust_frequency'][index]

            file_list = np.array(file_list)[index]
            self.cal_data['adjust_vectors'] = np.zeros((n, 360))
            for m in range(n):
                with open(file_list[m], 'r') as stream:
                    cv = yaml.load(stream, Loader=SafeLoader)
                self.cal_data['adjust_vectors'][m] = np.array(cv['angleCal'])

        self.logger.debug("[{:.3f}s] CalibrationLoad Complete".format(time.time() - cal_start))
