import numpy as np
from ProcessingLib.AsiDfEngine import AsiDfE


class DfEngineProcessor:
    """
    Class used to process a signal through the DFengine.

    :param target_freq: target frequency, i.e. for AIS 161.975MHz or 162.025MHz
    :param bw: bandwidth
    :param index_start: Index of the signal start
    :param index_stop: Index of the signal stop
    :param iq_data: ASI channel data
    :param radio_state: radio_state from the data file
    :param data_nav: data_nav from the data file

    :return dfe_results; The results returned by the DFengine
    """

    def __init__(self, configuration_path='/opt/asi/settings', configuration_file='AsiDfe_config.yml'):
        self.debug = False
        self.dfe = AsiDfE.AsiDfE(configuration_path=configuration_path, configuration_file=configuration_file)

    def process_df(self, target_freq, bw, index_start, index_stop, iq_data, radio_state, data_nav):
        # dfe = AsiDfE.AsiDfE()
        self.dfe.add_target(target_freq, bw)  # (freq,bandwidth)
        index_stop_plus_one = index_stop + 1  # +1 to account for python slices
        if index_stop_plus_one > len(iq_data[0]):
            index_stop_plus_one = len(iq_data[0])
        iq_data_window = iq_data[:, index_start:index_stop_plus_one]
        spectrum = np.fft.fftshift(np.fft.fft(iq_data_window), axes=1)
        f1 = np.arange(-spectrum.shape[1] // 2,
                       spectrum.shape[1] // 2) * radio_state['data_rate'] / spectrum.shape[1]
        f1 += radio_state['frequency'][0]
        dfe_results = self.dfe.get_df_results(spectrum, f1, data_nav)
        self.dfe.remove_target(target_freq)

        debug_print(dfe_results) if self.debug else None

        return dfe_results


def debug_print(results):
    print('/nDFengine Results')
    for key in results.keys():
        print('{}: {}'.format(key, results[key]))
