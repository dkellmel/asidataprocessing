#!/usr/bin/env python
# Copyright (C) 2016 Applied Signals Intelligence, Inc. -- All Rights Reserved

import os
import time
import logging
import numpy as np
from math import isnan
from ProcessingLib.AsiDfEngine.df_target.DfTarget import DfTarget
from ProcessingLib.AsiDfEngine.sensor_array.DpaArray import DpaArray
from ProcessingLib.AsiDfEngine.sensor_array.ManifoldArray import ManifoldArray

import yaml
try:
    from yaml import CSafeLoader as SafeLoader, CSafeDumper as SafeDumper
except ImportError:
    from yaml import SafeLoader, SafeDumper


class Position(object):
    """
    Class used to keep track of position information.

    :param latitude: Latitude of sensor position
    :param longitude: Longitude of sensor position
    :param altitude: Altitude of sensor position
    :param mgrs: MGRS representation of position of sensor
    """

    def __init__(self, latitude=np.double('nan'), longitude=np.double('nan'), altitude=np.double('nan'),
                 mgrs=None):
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.mgrs = mgrs

        if not isnan(latitude) and not isnan(longitude) and mgrs is None:
            self.x = True


# noinspection DuplicatedCode
def geo_reference_angles(aoa, depression, yaw, pitch, roll):

    if isnan(yaw):
        yaw = 0.0
    if isnan(pitch):
        pitch = 0.0
    if isnan(roll):
        roll = 0.0

    v = np.array([np.cos(np.deg2rad(aoa)) * np.cos(np.deg2rad(depression)),
                  np.sin(np.deg2rad(aoa)) * np.cos(np.deg2rad(depression)),
                  np.sin(np.deg2rad(depression))])
    pt = np.deg2rad(pitch)
    r_p = np.array([[np.cos(pt), 0, -np.sin(pt)], [0, 1, 0], [np.sin(pt), 0, np.cos(pt)]])
    rl = np.deg2rad(roll)
    r_r = np.array([[1, 0, 0], [0, np.cos(rl), np.sin(rl)], [0, -np.sin(rl), np.cos(rl)]])

    r = r_r @ r_p

    w = r.T @ v

    n_aoa = np.rad2deg(np.arctan2(w[1], w[0]))
    n_depression = np.rad2deg(np.arctan2(w[2], np.sqrt(w[0] ** 2 + w[1] ** 2)))
    lob = (n_aoa + yaw) % 360

    return n_aoa, n_depression, lob


class AsiDfE(object):
    """
    The AsiDfeEngine module is a configurable module that performs direction finding with a broadly definable set
    of antenna arrays, configurations, and orientations. The hi-level module control is built into the AsiDfE class.
    An object of the class is instantiated by a call to AsiDfE. The constructor takes an optional configuration
    filename. It is assumed that the configuration file sits in the same directory as the module code. If no
    configuration file is passed into the constructor, the object will use the default "AsiDfe_config.yml".

    :param configuration_file: Configuration yaml that initializes sensor definitions

    """

    def __init__(self, configuration_path='/opt/asi/settings', configuration_file='AsiDfe_config.yml'):

        # Initialize logging for DFE
        self.logger_df_engine = logging.getLogger(name='AsiDfEngine')

        # Define dictionary to hold all the DF arrays
        self.sensor_array = {'array_type': [],  # List of identifiers for the types of arrays
                             'array_object': [],  # List of array objects
                             }

        self.config_path = configuration_path
        self.config_file = configuration_file
        # Read the list of arrays and parameters, then instantiate them
        fn = os.path.join(configuration_path, configuration_file)
        try:
            with open(fn, 'r') as stream:
                array_list = yaml.load(stream, Loader=SafeLoader)
                for a in array_list:
                    array_type = array_list[a].pop('array_type', None)
                    if array_type == 'dpa':
                        self.sensor_array['array_type'].append(array_type)
                        self.sensor_array['array_object'].append(DpaArray(**array_list[a]))
                        self.logger_df_engine.info("Loaded DPA Array: {}".format(a))
                    elif array_type == 'manifold':
                        self.sensor_array['array_type'].append(array_type)
                        self.sensor_array['array_object'].append(ManifoldArray(**array_list[a]))
                        self.logger_df_engine.info("Loaded Manifold Array: {}".format(a))
                    else:
                        self.logger_df_engine.warning("Array type-> {}: not yet implemented".format(array_type))

        except IOError:
            self.logger_df_engine.warning("Invalid Configuration Yaml: {}".format(fn))
            self.logger_df_engine.warning("No DF arrays have been initialized")

    def get_df_results(self, freq_data, freq_vector, data_nav=None, array_index=0, polarity='v_pol'):
        """
        Helper function to quickly extract lobs and other related information
        for a list of targets

        :param freq_data: The frequency domain data with each row representing
                          a radio channel
        :param freq_vector: A vector of the frequency values of each bin in
                            the freq_data rows
        :param data_nav: The navigation data associated with the data capture.
                         The navigation data is a dictionary returned from
                         AsiNavigation.Navigation.get_nav method.
        :param array_index: Index of the DF array to get results from
        :param polarity: The expected polarity of the target signal
        :return: A list of dictionaries, with each dictionary containing a set
                 of parameters computed for the target. The results dictionary
                 is a combination of the
                 :class:`~AsiDfEngine.DfTarget.DfTarget.target_results`,
                 :class:`~AsiDfEngine.DfTarget.DfTarget.target_parameters`,and
                 :class:`~AsiDfEngine.DfTarget.DfTarget.target_status`
                 dictionaries defined in the :class:`~AsiDfEngine.DfTarget` class.
        """

        timestamp = time.time()
        if data_nav is not None:
            timestamp = data_nav['GPS']['systime']
        if self.sensor_array['array_object'][array_index].process_data(freq_data, freq_vector,
                                                                       timestamp, polarity=polarity):
            output = []
            for m, target in enumerate(self.sensor_array['array_object'][array_index].target_info['target_list']):
                if data_nav is not None:
                    target.target_results['AoA'], target.target_results['dep'], target.target_results['LoB'] = \
                        geo_reference_angles(target.target_results['AoA'],
                                             target.target_results['dep'],
                                             data_nav['COMPASS']['yaw_corrected'],
                                             data_nav['COMPASS']['pitch_corrected'],
                                             data_nav['COMPASS']['roll_corrected'])
                output.append(target.target_results)
                output[-1].update(target.target_parameters)
                output[-1].update(target.target_status)
        else:
            output = None

        return output

    def add_target(self, frequency, bandwidth, technique=DfTarget.Techniques.frequency_domain, array_index=None):
        """
        Add target to all specified DF arrays (as long as it is within the
        array frequency range). If array_index is not specified, target will
        be added to all applicable arrays.

        :param frequency: Center frequency of the target
        :param bandwidth: Bandwidth of the target
        :param technique: The DF processing technique applied to the target, as
                          defined in :class:`~AsiDfEngine.DfTarget.DfTarget.Techniques`
                          under the DfTarget class. Default is frequency_domain.
        :param array_index: Index of array to add target to. Default: None-> All arrays
        :return: True on success, False on failure.

        """

        if array_index is None:
            array_index = list(range(len(self.sensor_array['array_object'])))
        if not isinstance(array_index, list):
            array_index = [array_index]

        target_added = False
        for index, a in enumerate(self.sensor_array['array_object']):
            coverage = a.array_parameters['frequency_coverage']
            if coverage['f_low'] <= frequency <= coverage['f_high'] and index in array_index:
                if not a.add_target(frequency, bandwidth, technique):
                    self.logger_df_engine.warning("add_target: Could not add target to "
                                                  "array {}".format(self.sensor_array['array_type'][index]))
                else:
                    target_added = True
        return target_added

    def remove_target(self, frequency, array_index=None):
        """
        Remove specified target from specified array index. If no array index is given, target is removed from all
        arrays.

        :param frequency: Center frequency of the target to be removed
        :param array_index: Index of array to remove target from. Default: None-> All arrays
        :return: None

        """

        if array_index is None:
            array_index = list(range(len(self.sensor_array['array_object'])))

        if not isinstance(array_index, list):
            array_index = [array_index]

        for index in array_index:
            self.sensor_array['array_object'][index].remove_target(frequency)

    # def set_lob_filter_length(self, length):
    #     self.df_parameters['lob_filter_length'] = length
    #     self.df_status['lob_vector'] = np.resize(self.df_status['lob_vector'],
    #                                              self.df_parameters['lob_filter_length'])
    #     self.df_status['rssi_vector'] = np.resize(self.df_status['rssi_vector'],
    #                                               self.df_parameters['lob_filter_length'])
    #     self.df_status['lob_index'] %= self.df_parameters['lob_filter_length']


if __name__ == "__main__":
    from AsiUtilities.data_file_io import read_mp
    path = '/home/tim/'
    filename = 'nb-2019-07-09_14-50-04-515385_5Msps.dat'
    data, data_nav, radio_state, _ = read_mp(os.path.join(path, filename))
    dfe = AsiDfE()

    dfe.add_target(162e6 - 25e3, 25000)
    index_start = 5675000
    index_stop = 5800000
    my_data = data[:, index_start:index_stop]
    spectrum = np.fft.fftshift(np.fft.fft(my_data), axes=1)
    f1 = np.arange(-spectrum.shape[1] // 2, spectrum.shape[1] // 2) * radio_state['data_rate'] / spectrum.shape[1]
    f1 += radio_state['frequency'][0]
    results = dfe.get_df_results(spectrum, f1, data_nav)
    dfe.remove_target(162e6 - 25e3)

    print(results[0])
