# **************************************************************************************
# required for inheritance
# **************************************************************************************
import os
import sys
# from os import path
#
# programPath = path.abspath(__file__)
# components = programPath.split(os.sep)
# aisDir = str.join(os.sep, components[:components.index("AsiDataProcessing") + 1]) + '/'
# sys.path.insert(0, aisDir)

# **************************************************************************************

# from lib import ais_nmea_decoder
from lib.aisUtility import decod_ais

import logging
import argparse

from sys import version_info

if version_info.major == 2:
    # Python 2.x
    python_version = 2
    import Tkinter as tk
    import tkFileDialog
elif version_info.major == 3:
    # Python 3.x
    python_version = 3
    import tkinter as tk
    from tkinter import filedialog

    _logger = logging.getLogger(__name__)


class DecodeNmeaSentences():
    execDir = os.getcwd()+'/'

    # list of the ais message types that can be decoded
    # decod_type = {1, 2, 3, 4, 5, 18, 19, 21, 24}
    decod_type = {1, 2, 3, 4, 18, 19, 21}  # type 24 & 5 doesn't have lat/lon

    def __init__(self):
        self.debug = False
        self.info = False
        self.nmea_file = 'temp'
        self.nmea_file_path = None
        self.nmea_sentences = []
        self.nmea_sentence = None

    def init(self):
        if self.info:
            logging.basicConfig(level=logging.INFO)
            _logger.info('INFO logging set.')

        if self.debug:
            logging.basicConfig(level=logging.DEBUG)
            _logger.debug('DEBUG logging set.')

    def set_info(self, info):
        self.info = info

    def get_info(self):
        return self.info

    def set_debug(self, debug):
        self.debug = debug

    def get_debug(self):
        return self.debug

    def set_nmea_sentence(self, nmea_sentence):
        self.nmea_sentence = nmea_sentence

    def get_nmea_sentence(self):
        return self.nmea_sentence

    def get_ais_data(self):
        return self.ais_data

    def decode_nmea_sentence(self):
        #     if len(sys.argv) < 2:
        if self.nmea_sentence == None:
            if python_version == 2:
                tk.Tk().withdraw()  # Close the root window
                self.nmea_file_path = tkFileDialog.askopenfilename(initialdir='../data/nmea/')
            else:
                tk.Tk().withdraw()  # Close the root window
                self.nmea_file_path = filedialog.askopenfilename(initialdir='../data/nmea/')

            self.nmea_file = os.path.basename(self.nmea_file_path)
            _logger.info('Reading file: {}'.format(self.nmea_file_path))
            f = open(self.nmea_file_path, 'r')
            self.nmea_sentences = f.read().split('\n')
            _logger.info(self.nmea_sentences)
            f.close()
        else:
            self.nmea_sentences.append(self.nmea_sentence)

        # decoded_nmea_file_path = 'data/nmea_decodes/' + self.nmea_file + '_decoded.txt'
        # fo = open(decoded_nmea_file_path, 'w')
        #   fo = open(os.path.dirname(nmea_file),'w')
        #   print(os.path.dirname(nmea_file))

        if not len(self.nmea_sentences) == 0:
            for nmea_sentence in self.nmea_sentences:
                if nmea_sentence == '':
                    _logger.info('Nothing to decode.')
                    break
                _logger.info('Decoding: "{}"'.format(nmea_sentence))
                # fo.write('\nDecoding: {}\n'.format(nmea_sentence))
                self.ais_data = decod_ais(nmea_sentence)  # return a dictionnary
                # print(self.ais_data)
                # ais_format = format_ais(self.ais_data)   # a more human readable dictionnary
                # print(ais_format)
                if self.debug:
                    for key in self.ais_data.keys():
                        _logger.debug('{}: {}'.format(key, self.ais_data[key]))
        else:
            _logger.info('Nothing to decode. List is empty.')


def main(args):
    decodeNmeaSentences = DecodeNmeaSentences()

    if args.sent:
        decodeNmeaSentences.set_nmea_sentence(args.sent)

    if args.debug:
        decodeNmeaSentences.set_debug(args.debug)

    if args.info:
        decodeNmeaSentences.set_info(args.info)

    decodeNmeaSentences.init()
    decodeNmeaSentences.decode_nmea_sentence()

    ais_data = decodeNmeaSentences.get_ais_data()

    if ais_data['type'] not in decodeNmeaSentences.decod_type:
        ais_data['none'] = 'none'

    return ais_data


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('-s', '--sent',
                        help='NMEA sentence to decode')

    parser.add_argument('-d', '--debug',
                        help='enable debug logging',
                        action='store_true')

    parser.add_argument('-i', '--info',
                        help='enable info logging',
                        action='store_true')

    args = parser.parse_args()

    main(args)

    print('done...')
