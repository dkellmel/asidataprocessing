import os
import time
import subprocess
import numpy as np

from argparse import Namespace

import lib.lob as LOB
import ProcessingLib.AIS.DemodAisProcessor as DemodAis
import ProcessingLib.AIS.DecodeNmeaSentenceProcessor as DecodeNmeaSentences

from lib.aisUtility import compute_checksum
from ProcessingLib.AsiDfEngine import AsiDfE


class AisProcessor:
    __version__ = '1.0.0'

    # definition of self variables
    DecodeAis_namespace = None
    ais_data = None
    aisDir = None
    info = None
    debug = None
    use_fixed_gps = None
    gps_latitude = None
    gps_longitude = None
    systime = None
    data_rate = None
    DemodAis_namespace = None
    ais_lat = None
    ais_lon = None
    ais_mmsi = None

    ais_center_freq = 162e6
    ais_bw = 25e3
    ais_channel_offsets = {'A': -25e3, 'B': 25e3}

    processing_times = {'read_snapshot_file': 0.0,
                        'gnuradio': 0.0,
                        'gnuais': 0.0,
                        'decode_nmea_sentences': 0.0,
                        'DFengine': 0.0
                        }

    execDir = os.getcwd()+'/'

    def __init__(self, _filename, datafile_dict):
        self._filename = _filename
        self.datafile_dict = datafile_dict

    def setArgs(self, aisDir, info, debug, use_fixed_gps):
        self.aisDir = aisDir
        self.info = info
        self.debug = debug
        self.use_fixed_gps = use_fixed_gps

    def setFixedGps(self, lat, lon):
        self.gps_latitude = lat
        self.gps_longitude = lon

    def demodais_processor(self):
        if self.info or self.debug:
            print('\n' + 29 * '*' + ' demod_ais (gnuradio) ' + 29 * '*')
        st = time.time()
        DemodAis.main(self.DemodAis_namespace)
        et = time.time()
        self.processing_times['gnuradio'] += et - st
        if self.info or self.debug:
            print(30 * '*' + ' demod_ais complete ' + 30 * '*')
            print(80 * '=')

    def gnuais_processor(self, output_file, chan):
        if self.info or self.debug:
            print('\n' + 36 * '*' + ' gnuais ' + 36 * '*')
        st = time.time()
        cmd = self.aisDir + 'bin/gnuais -l ' + output_file + '_Chan_' + chan
        if self.info:
            print('gnuais command: {}'.format(cmd))
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        (output, err) = p.communicate()

        # this makes the wait possible
        p_status = p.wait()

        # this prints the output of the command being executed
        # if self.info:
        # _logger.debug('gnuais output follows:\n')
        # _logger.debug('{}'.format(output.decode('ASCII')))
        # print('gnuais output follows:\n{}'.format(output.decode('ASCII')))
        # print('-----gnuais output-----')
        gnuais_results = {}
        for line in output.decode('ASCII').split('\n'):
            if line.startswith('NMEA Sentence:'):
                s = line.split(':')
                # print(str(s[0]))
                if len(s) == 2:
                    sent = str(s[1]).strip()
                    gnuais_results['NMEA_Sentence'] = sent[1:len(sent) - 1]
                else:
                    sent = str(s[1])
                    for i in range(2, len(s)):
                        sent += ':' + str(s[i])
                    sent = sent.strip()
                    gnuais_results['NMEA_Sentence'] = sent[1:len(sent) - 1]
                # print(gnuais_results['NMEA_Sentence'])
            elif line.startswith('AIS Window Start Time:'):
                s = line.split(':')
                gnuais_results['ais_window_start_time'] = str(s[1]).strip()
                # print(gnuais_results['ais_window_start_time'])
            elif line.startswith('AIS Window End Time:'):
                s = line.split(':')
                gnuais_results['ais_window_end_time'] = str(s[1]).strip()
                # print(gnuais_results['ais_window_end_time'])
            elif line.startswith('Offset Time'):
                s = line.split(':')
                gnuais_results['Offset_Time'] = str(s[1]).strip()
                # print(gnuais_results['Offset_Time'])

        et = time.time()
        self.processing_times['gnuais'] += et - st
        if self.info or self.debug:
            print(32 * '*' + ' gnuais complete ' + 31 * '*')
            print(80 * '=')

        return gnuais_results

    def decodeais_processor(self, ais_chan, gnuais_results):
        st = time.time()
        offset_time = float(gnuais_results['Offset_Time'])
        ais_window_start_time = float(gnuais_results['ais_window_start_time'])
        ais_window_end_time = float(gnuais_results['ais_window_end_time'])

        # print('\n\nais_window_start_time: {}'.format(ais_window_start_time))
        # print('ais_window_end_time: {}\n\n'.format(ais_window_end_time))

        # correct the NMEA sentence returned by gnuais
        # change the checksum to account A->B
        if ais_chan == 'B':
            Bsent = gnuais_results['NMEA_Sentence'].replace(',A,', ',B,', 1)
            # print('New checksum: {}'.format(self.compute_checksum(Bsent)))
            gnuais_results['NMEA_Sentence'] = Bsent[:-2] + compute_checksum(Bsent)

        # if self.dbg:
        #     print('***{}***'.format(gnuais_results['NMEA_Sentence']))

        self.DecodeAis_namespace = Namespace(sent=gnuais_results['NMEA_Sentence'],
                                             debug=self.debug, info=self.info)
        ais_data = DecodeNmeaSentences.main(self.DecodeAis_namespace)

        # if self.debug:
        #     _logger.debug('\n.....NMEA Sentence.....')
        #     for key in ais_data.keys():
        #         _logger.debug('{}: {}'.format(key, ais_data[key]))
        #     print('')

        # check for bad nmea sentence
        if 'none' in ais_data.keys():
            print('Bad AIS NMEA sentence: "{}"'.format(gnuais_results['NMEA_Sentence']))
            print('ais_data: {}'.format(ais_data))
            gnuais_results = {}
            return {}

        # self.ais_lat = ais_data['lat']
        # self.ais_lon = ais_data['lon']
        # if self.terminal_output:
        #     print('AIS Latitude: {:.6f}'.format(self.ais_lat))
        #     print('AIS Longitude {:.6f}'.format(self.ais_lon))
        et = time.time()
        self.processing_times['decode_nmea_sentences'] += et - st
        if self.info or self.debug:
            print(24 * '*' + ' decode_nmea_sentences complete ' + 24 * '*')
            print(80 * '=')
        return ais_data

    def dfengine_processor(self, ais_chan, index_start, index_stop, iq_data, radio_state, data_nav):
        if self.info or self.debug:
            print('\n' + 33 * '*' + ' DFengine ' + 37 * '*')
        st = time.time()
        target = self.ais_center_freq + self.ais_channel_offsets[ais_chan]
        # iq_data = snapshots.get_iq_data()
        dfe = AsiDfE.AsiDfE()
        dfe.add_target(target, self.ais_bw)  # (freq,bandwidth)
        # index_start = int(ais_window_start_time * data_rate)
        # index_stop = int(ais_window_end_time * data_rate)
        # if self.terminal_output:
        # _logger.info('index_start: {} index_stop: {}'.format(index_start, index_stop))
        iq_data_window = iq_data[:, index_start:index_stop]
        # if self.terminal_output:
        # _logger.info('Computing FFT...')
        spectrum = np.fft.fftshift(np.fft.fft(iq_data_window), axes=1)
        f1 = np.arange(-spectrum.shape[1] // 2,
                       spectrum.shape[1] // 2) * radio_state['data_rate'] / spectrum.shape[1]
        f1 += radio_state['frequency'][0]
        dfe_results = dfe.get_df_results(spectrum, f1, data_nav)
        # dfe.remove_target(162e6 - 25e3)
        dfe.remove_target(target)
        # if self.debug:
        #     # print('DFE Results: \n{}'.format(dfe_results[0]))
        #     _logger.debug('DFE Results follow:')
        #     for key in dfe_results[0].keys():
        #         _logger.debug('{}: {}'.format(key, dfe_results[0][key]))
        # df_lob = dfe_results[0]['LoB'][0]
        # if self.debug:
        # _logger.debug('\nLoB: {}'.format(df_lob))
        et = time.time()
        self.processing_times['DFengine'] += et - st
        if self.info or self.debug:
            print(30 * '*' + ' DFengine complete ' + 31 * '*')
            print(80 * '=')

        return dfe_results

    def process_ais(self):
        radio_state = self.datafile_dict['radio_state']
        self.data_rate = int(radio_state['data_rate'])
        # data_rate = self.datafile_dict['radio_state']['data_rate']  # sample rate

        data_nav = self.datafile_dict['data_nav']
        self.systime = data_nav['GPS']['systime']
        # print(self.systime)

        if not data_nav['GPS']['latitude'] == 0 and not data_nav['GPS']['longitude'] == 0:
            gps_ok = True
        else:
            gps_ok = False

        ais_results = {}

        for chan in range(0, self.datafile_dict['number_of_channels']):
            ais_results[chan] = {'A': {}, 'B': {}}
            # _logger.debug('File for gnuradio: {}'.format(datafile_dict['output_files'][chan]))
            # output_file = datafile_dict['output_files'][chan]

            # run demod_ais ****************************************

            audio_output_filename_prefix = 'snapshot_iq' + str(chan)
            self.DemodAis_namespace = Namespace(control=True,
                                                tout=self.info,
                                                debug=False,
                                                samplerate=self.datafile_dict['data_rate'],
                                                filename=self._filename,
                                                outfile=audio_output_filename_prefix,
                                                prefix=self.datafile_dict['snapshot_iq_dir'],
                                                vector=self.datafile_dict['iq_data'][chan].tolist())
            self.demodais_processor()

            for ais_chan in ['A', 'B']:
            # for ais_chan in ['A']:
                # run gnuais ****************************************

                # output_file = datafile_dict['snapshot_iq_dir'] + audio_output_filename_prefix + '_Chan_' + ais_chan
                output_file = self.datafile_dict['snapshot_iq_dir'] + audio_output_filename_prefix
                # print('*****\n{}\n******'.format(output_file))
                gnuais_results = self.gnuais_processor(output_file, ais_chan)

                if not len(gnuais_results) == 0:
                    # run decode_nmea_sentences ******************************

                    if self.info or self.debug:
                        print('\n' + 28 * '*' + ' decode_nmea_sentences ' + 29 * '*')

                    self.DecodeAis_namespace = Namespace(sent=gnuais_results['NMEA_Sentence'],
                                                         debug=False)
                    self.ais_data = self.decodeais_processor(ais_chan, gnuais_results)

                    # print('\n\n***** start decodeais_processor *****')
                    # for key in self.ais_data.keys():
                    #     print('{}: {}'.format(key, self.ais_data[key]))
                    # print('***** end decodeais_processor *****\n\n')

                    ais_window_start_time = float(gnuais_results['ais_window_start_time'])
                    ais_window_end_time = float(gnuais_results['ais_window_end_time'])

                    if not self.ais_data:
                        break

                    # try:
                    self.ais_lat = self.ais_data['lat']
                    self.ais_lon = self.ais_data['lon']
                    self.ais_mmsi = self.ais_data['mmsi']
                    # except:
                    #     print(self.ais_data)
                    #     sys.exit(0)
                    # if self.terminal_output:
                    # _logger.info('AIS Latitude: {:.6f}'.format(self.ais_lat))
                    # _logger.info('AIS Longitude {:.6f}'.format(self.ais_lon))

                    # run df engine  ****************************************

                    iq_data = self.datafile_dict['iq_data']
                    index_start = int(ais_window_start_time * self.data_rate)
                    index_stop = int(ais_window_end_time * self.data_rate)
                    df_results = self.dfengine_processor(ais_chan, index_start, index_stop,
                                                         iq_data, radio_state, data_nav)
                    df_lob = df_results[0]['LoB'][0]
                    df_snr = df_results[0]['snr']
                    df_pwr = df_results[0]['instant_power']

                    # print('\n\n***** start df_results *****')
                    # for key in df_results[0].keys():
                    #     print('{}: {}'.format(key, df_results[0][key]))
                    # print('***** end df_results *****\n\n')

                else:

                    # _logger.info('No AIS channel {} signal found.'.format(ais_chan))
                    if self.info or self.debug:
                        print(24 * '*' + ' decode_nmea_sentences complete ' + 24 * '*')
                        print(80 * '=')

                if not len(gnuais_results) == 0:
                    results = {}
                    # tabulate results ******************************
                    # if you change the order of these you will have to change "tabulate_results.py"
                    # results['Data File'] = self.data_file
                    # results['Data Chan'] = chan
                    results['data_rate'] = self.data_rate
                    # results['Offset Time'] = gnuais_results['Offset_Time']
                    # results['Start Index'] = index_start
                    # results['Stop Index'] = index_stop
                    # results['AIS Channel'] = ais_chan
                    results['ais_MMSI'] = self.ais_mmsi
                    results['ais_lat'] = self.ais_lat
                    results['ais_lon'] = self.ais_lon
                    results['ais_type'] = self.ais_data['type']
                    results['track_id'] = self.ais_data['radio']
                    results['utc_time'] = self.systime
                    results['target_lob'] = [df_lob]
                    results['target_snr'] = [df_snr]
                    results['target_pwr'] = [df_pwr]
                    results['signal_start_time'] = [ais_window_start_time]
                    results['signal_end_time'] = [ais_window_end_time]

                    if results['ais_type'] == 18:
                        results['ais_cog'] = self.ais_data['heading']  # course over ground
                        results['ais_sog'] = self.ais_data['speed']  # speed over ground

                    if gps_ok:
                        # compute ais LOB ******************************
                        pointA = (self.collection_lat, self.collection_lon)
                        pointB = (self.ais_lat, self.ais_lon)
                        ais_lob = LOB.calculate_initial_compass_bearing(pointA, pointB)
                        # results['AIS LOB'] = ais_lob
                        # results['sensor_lat'] = self.collection_lat
                        # results['sensor_lon'] = self.collection_lon
                        results['target_snr'] = df_snr
                        results['target_lob'] = df_lob
                        results['target_pwr'] = df_pwr
                        # results['AIS LOB - DF LOB'] = round(ais_lob - df_lob, 1)
                        # results['NMEA_Sentence'] = gnuais_results['NMEA_Sentence']

                    elif self.use_fixed_gps:
                        # compute ais LOB ******************************
                        pointA = (self.gps_latitude, self.gps_longitude)
                        pointB = (self.ais_lat, self.ais_lon)
                        ais_lob = LOB.calculate_initial_compass_bearing(pointA, pointB)
                        # results['AIS LOB'] = ais_lob
                        # results['sensor_lat'] = self.gps_latitude
                        # results['sensor_lon'] = self.gps_longitude
                        # results['sensor_yaw'] = 0
                        # results['sensor_pitch'] = 0
                        # results['sensor_roll'] = 0
                        # results['AIS LOB - DF LOB'] = round(ais_lob - df_lob, 1)
                        # results['NMEA_Sentence'] = gnuais_results['NMEA_Sentence']

                    else:
                        # results['AIS LOB'] = 'N/A'
                        # results['sensor_lat'] = 'N/A'
                        # results['sensor_lon'] = 'N/A'
                        # results['target_lob'] = 'N/A'
                        # results['target_snr'] = 'N/A'
                        # results['AIS LOB - DF LOB'] = 'N/A'
                        # results['NMEA_Sentence'] = gnuais_results['NMEA_Sentence']
                        pass

                    # if not len(results) == 0:
                    #     results['asi_chan'] = chan
                    #     results['ais_chan'] = ais_chan
                    ais_results[chan][ais_chan] = results
                    # print('chan: {} ais_chan: {}'.format(chan,ais_chan))
                    # print(ais_results[chan][ais_chan])

        return ais_results
