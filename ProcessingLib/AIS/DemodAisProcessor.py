#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: AIS Receiver
# Author: ASI Don Kellmel
# Description: AIS 2 channel receiver using file source
# Generated: Fri Jul 19 13:58:30 2019
##################################################


# **************************************************************************************
# required for lib use
# **************************************************************************************
import os
import sys

# from os import path
#
# programPath = path.abspath(__file__)
# components = programPath.split(os.sep)
# aisDir = str.join(os.sep, components[:components.index("AsiAisProcessing") + 1]) + '/'
# sys.path.insert(0, aisDir)

# **************************************************************************************

from lib.folder_file_selector import FolderFileSelector
# from lib.printit import printit

import argparse

from datetime import datetime
from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import math


class Demod_AIS(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "AIS Receiver")

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = 12.8e6
        self.samp_rate = 1.0e6
        self.input_file = 'none'
        self.file_prefix = file_prefix = 'none'
        self.audio_output_filename_prefix = None

    def set_variables(self):
        self.xlate_filter_taps = xlate_filter_taps = firdes.low_pass(1, self.samp_rate, 15000, 20000,
                                                                     firdes.WIN_HAMMING, 6.76)
        self.chB_offset = chB_offset = 25e3
        self.chA_offset = chA_offset = -25e3

    def set_blocks(self):
        ##################################################
        # Blocks
        ##################################################
        # if self.samp_rate < 10e6:
        #     self.throttle = blocks.throttle(gr.sizeof_gr_complex * 1, 50e6, True)
        # else:
        #     self.throttle = blocks.throttle(gr.sizeof_gr_complex * 1, self.samp_rate, True)

        self.rational_resampler_xxx_ChanB = filter.rational_resampler_fff(
            interpolation=48,
            decimation=50,
            taps=None,
            fractional_bw=None,
        )
        self.rational_resampler_xxx_ChanA = filter.rational_resampler_fff(
            interpolation=48,
            decimation=50,
            taps=None,
            fractional_bw=None,
        )
        self.freq_xlating_fir_filter_xxx_ChanB = filter.freq_xlating_fir_filter_ccc(int(self.samp_rate / 1e6 / 0.05),
                                                                                    (self.xlate_filter_taps),
                                                                                    self.chB_offset, self.samp_rate)
        self.freq_xlating_fir_filter_xxx_ChanA = filter.freq_xlating_fir_filter_ccc(int(self.samp_rate / 1e6 / 0.05),
                                                                                    (self.xlate_filter_taps),
                                                                                    self.chA_offset, self.samp_rate)
        # self.blocks_interleave_AB = blocks.interleave(gr.sizeof_short * 1, 1)
        self.blocks_float_to_short_ChanB = blocks.float_to_short(1, 16000)
        self.blocks_float_to_short_ChanA = blocks.float_to_short(1, 16000)
        # self.blocks_file_source = blocks.file_source(gr.sizeof_gr_complex * 1, self.input_file, False)
        self.blocks_vector_source = blocks.vector_source_c(self.complex_vector, False, 1, [])
        self.blocks_file_sink_ChanB = blocks.file_sink(gr.sizeof_short * 1,
                                                       self.file_prefix + self.audio_output_filename_prefix + '_Chan_B',
                                                       False)
        self.blocks_file_sink_ChanB.set_unbuffered(False)
        self.blocks_file_sink_ChanA = blocks.file_sink(gr.sizeof_short * 1,
                                                       self.file_prefix + self.audio_output_filename_prefix + '_Chan_A',
                                                       False)
        self.blocks_file_sink_ChanA.set_unbuffered(False)
        # self.blocks_file_sink_AB = blocks.file_sink(gr.sizeof_short * 1, self.file_prefix + self.filename + '_stereo',
        #                                             False)
        # self.blocks_file_sink_AB.set_unbuffered(False)
        self.analog_quadrature_demod_cf_ChanB = analog.quadrature_demod_cf(0.3)
        self.analog_quadrature_demod_cf_ChanA = analog.quadrature_demod_cf(0.3)

    def make_connections(self):
        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_quadrature_demod_cf_ChanA, 0), (self.rational_resampler_xxx_ChanA, 0))
        self.connect((self.analog_quadrature_demod_cf_ChanB, 0), (self.rational_resampler_xxx_ChanB, 0))
        # self.connect((self.blocks_file_source, 0), (self.throttle, 0))
        # self.connect((self.blocks_vector_source, 0), (self.throttle, 0))
        self.connect((self.blocks_float_to_short_ChanA, 0), (self.blocks_file_sink_ChanA, 0))
        # self.connect((self.blocks_float_to_short_ChanA, 0), (self.blocks_interleave_AB, 0))
        self.connect((self.blocks_float_to_short_ChanB, 0), (self.blocks_file_sink_ChanB, 0))
        # self.connect((self.blocks_float_to_short_ChanB, 0), (self.blocks_interleave_AB, 1))
        # self.connect((self.blocks_interleave_AB, 0), (self.blocks_file_sink_AB, 0))
        self.connect((self.freq_xlating_fir_filter_xxx_ChanA, 0), (self.analog_quadrature_demod_cf_ChanA, 0))
        self.connect((self.freq_xlating_fir_filter_xxx_ChanB, 0), (self.analog_quadrature_demod_cf_ChanB, 0))
        self.connect((self.rational_resampler_xxx_ChanA, 0), (self.blocks_float_to_short_ChanA, 0))
        self.connect((self.rational_resampler_xxx_ChanB, 0), (self.blocks_float_to_short_ChanB, 0))
        # self.connect((self.throttle, 0), (self.freq_xlating_fir_filter_xxx_ChanA, 0))
        # self.connect((self.throttle, 0), (self.freq_xlating_fir_filter_xxx_ChanB, 0))
        self.connect((self.blocks_vector_source, 0), (self.freq_xlating_fir_filter_xxx_ChanA, 0))
        self.connect((self.blocks_vector_source, 0), (self.freq_xlating_fir_filter_xxx_ChanB, 0))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        # self.set_xlate_filter_taps(firdes.low_pass(1, self.samp_rate, 15000, 20000, firdes.WIN_HAMMING, 6.76))
        # self.throttle.set_sample_rate(self.samp_rate)

    def set_complex_vector(self, complex_vector):
        self.complex_vector = complex_vector

    def get_xlate_filter_taps(self):
        return self.xlate_filter_taps

    def set_xlate_filter_taps(self, xlate_filter_taps):
        self.xlate_filter_taps = xlate_filter_taps
        self.freq_xlating_fir_filter_xxx_ChanB.set_taps((self.xlate_filter_taps))
        self.freq_xlating_fir_filter_xxx_ChanA.set_taps((self.xlate_filter_taps))

    # full path input file
    def get_input_file(self):
        return self.input_file

    def set_audio_output_filename(self, audio_output_filename_prefix):
         self.audio_output_filename_prefix = audio_output_filename_prefix

    # full path input file
    def set_input_file(self, input_file):
        self.input_file = input_file
        self.set_filename(os.path.basename(input_file))

    def get_filename(self):
        return self.filename

    # filename only
    def set_filename(self, filename):
        self.filename = filename

    # filename only
    def get_file_prefix(self):
        return self.file_prefix

    def set_file_prefix(self, file_prefix):
        self.file_prefix = file_prefix

    def get_ch2_offset(self):
        return self.chB_offset

    def set_ch2_offset(self, chB_offset):
        self.chB_offset = chB_offset
        self.freq_xlating_fir_filter_xxx_ChanB.set_center_freq(self.chB_offset)

    def get_ch1_offset(self):
        return self.chA_offset

    def set_ch1_offset(self, chA_offset):
        self.chA_offset = chA_offset
        self.freq_xlating_fir_filter_xxx_ChanA.set_center_freq(self.chA_offset)


def main0(args):
    print('demod_ais is running stand alone')

    control = args.control
    dbg = args.debug
    filename = args.file
    sample_rate = int(args.samplerate)
    file_prefix, _filename = os.path.split(filename)
    file_prefix += '/'

    print('control: {}'.format(control))
    print('debug: {}'.format(dbg))
    print('filename: {}'.format(filename))
    print('file_prefix: {}'.format(file_prefix))
    print('sample_rate: {}'.format(sample_rate))

    demod_ais = Demod_AIS()

    demod_ais.set_file_prefix(file_prefix)
    demod_ais.set_input_file(filename)
    demod_ais.set_samp_rate(sample_rate)

    demod_ais.set_variables()
    demod_ais.set_blocks()

    demod_ais.make_connections()

    demod_ais.run()  # start and wait

    demod_ais.stop()
    demod_ais.wait()


def main(run_args):  # asi_ais_tipoff uses this one

    control = run_args.control
    dbg = run_args.debug
    complex_vector = run_args.vector
    sample_rate = int(run_args.samplerate)
    # file_prefix, _filename = os.path.split(filename)
    filename = run_args.filename
    file_prefix = run_args.prefix
    audio_output_filename_prefix = run_args.outfile

    if run_args.tout:
        print('demod_ais is running as a controlled module')
        print('control: {}'.format(control))
        print('debug: {}'.format(dbg))
        print('filename: {}'.format(filename))
        print('audio output filename prefix: {}'.format(audio_output_filename_prefix))
        print('file_prefix: {}'.format(file_prefix))
        print('sample_rate: {} Msps'.format(sample_rate / 1e6))

    demod_ais = Demod_AIS()

    demod_ais.set_file_prefix(file_prefix)
    demod_ais.set_complex_vector(run_args.vector)
    demod_ais.set_input_file(filename)
    demod_ais.set_audio_output_filename(audio_output_filename_prefix)
    demod_ais.set_samp_rate(sample_rate)

    demod_ais.set_variables()
    demod_ais.set_blocks()

    demod_ais.make_connections()

    demod_ais.run()  # start and wait

    demod_ais.stop()
    demod_ais.wait()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-c', '--control',
                        help='run in controlled mode',
                        action='store_true')

    parser.add_argument('-d', '--debug',
                        help='enable debug logging',
                        action='store_false')

    parser.add_argument('-f', '--filename',
                        help='filename w/o path')

    parser.add_argument('-i', '--info',
                        help='enable info logging',
                        action='store_false')

    parser.add_argument('-o', '--outfile',
                        help='output filename')

    parser.add_argument('-p', '--prefix',
                        help='output files prefix')

    parser.add_argument('-s', '--samplerate',
                        help='samplerate = sample rate')

    parser.add_argument('-v', '--vector',
                        help='complex vector as list')

    parser.add_argument('-x', '--tout',
                        help='suspend terminal output',
                        action='store_false')

    args = parser.parse_args()

    if not args.control:
        main0(args)
    else:
        main(args)
