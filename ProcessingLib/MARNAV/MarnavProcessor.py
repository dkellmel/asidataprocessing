"""
Marine Radar

X-band = 8 - 12 GHz
S-band = 2 - 4 GHz
"""

import numpy as np

from ProcessingLib.MARNAV.ComputePris import ComputePris


class MarnavProcessor:
    __version__ = '1.0.0'
    debug_test = False
    # debug_test = True
    debug_print = False
    # debug_print = True
    marnav_params = {}
    noise_threshold_multiplier = 1.5
    min_number_of_pulses = 5
    pri_min = 100e-6
    pri_max = 1e-3

    def __init__(self):
        if self.debug_print:
            print('MARNAV Processor.')

    def set_datafile_dict(self, datafile_dict):
        self.datafile_dict = datafile_dict

    def set_pri_min_max(self, pri_min, pri_max):
        self.pri_min = pri_min
        self.pri_max = pri_max

    def set_noise_threshold_multiplier(self, noise_threshold_multiplier):
        self.noise_threshold_multiplier = noise_threshold_multiplier

    def set_min_number_of_pulses(self, min_number_of_pulses):
        self.min_number_of_pulses = min_number_of_pulses

    def init(self):
        self.compute_pris = ComputePris()
        self.compute_pris.set_pri_min_max(self.pri_min, self.pri_max)
        self.compute_pris.set_noise_threshold_multiplier(self.noise_threshold_multiplier)
        self.compute_pris.set_min_number_of_pulses(self.min_number_of_pulses)
        self.compute_pris.set_debug_plot(False)
        self.compute_pris.set_debug_print(False)

    def process_marnav(self):
        pris = []
        pri_powers = []
        time_windows = []
        number_of_pulse_intervals = []
        marnav_results = {}

        # if self.debug_test is not True:
        chan = 0
        for iq_data in self.datafile_dict['iq_data']:
            if self.debug_print:
                print('Processing channel: {0:1d}'.format(chan))
            self.compute_pris.set_iq_data(iq_data)
            self.compute_pris.set_asi_channel(chan)
            # self.compute_pri.compute_instantaneous_power()
            pris.append(self.compute_pris.get_pris())
            pri_powers.append(self.compute_pris.get_pri_powers())
            time_windows.append(self.compute_pris.get_time_windows())
            number_of_pulse_intervals.append(self.compute_pris.get_number_of_pulse_intervals())
            chan += 1
        self.marnav_params['pris'] = pris
        self.marnav_params['pri_powers'] = pri_powers
        # print(pri_powers)
        self.marnav_params['number_of_pulse_intervals'] = number_of_pulse_intervals
        self.marnav_params['time_windows'] = time_windows
        for i in range(0, len(pris)):
            marnav_results[i] = {}
            marnav_results[i]['pri_powers'] = pri_powers[i]
            marnav_results[i]['pris'] = np.round(pris[i], 1)
            marnav_results[i]['number_of_pulse_intervals'] = number_of_pulse_intervals[i]
            marnav_results[i]['time_windows'] = time_windows[i]
            if self.debug_print:
                for j in range(0, len(pris[i])):
                    print('PRI {0:1d} in Channel {1:1d}: {2:4.1f} us'.
                          format(j, i, pris[i][j]))

        return marnav_results
