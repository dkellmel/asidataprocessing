import numpy as np


class ComputePris:
    debug_plot = False
    debug_print = False
    noise_threshold_multiplier = 1.5
    min_number_of_pulses = 5
    min_number_of_pulse_intervals = min_number_of_pulses - 1
    pri_min = 100e-6
    pri_max = 1e-3
    sample_time = 1e-6
    asi_channel = None

    def __init__(self):
        self.iq_data = None
        self.pri = float('nan')
        self.pris = []
        self.pri_powers = []
        self.time_windows = []
        self.number_of_pulse_intervals = []

    def set_asi_channel(self, asi_channel):
        self.asi_channel = asi_channel

    def set_sample_time(self, sample_time):
        self.sample_time = sample_time

    def set_pri_min_max(self, pri_min, pri_max):
        self.pri_min = pri_min
        self.pri_max = pri_max

    def set_noise_threshold_multiplier(self, noise_threshold_multiplier):
        self.noise_threshold_multiplier = noise_threshold_multiplier

    def set_min_number_of_pulses(self, min_number_of_pulses):
        self.min_number_of_pulses = min_number_of_pulses
        self.min_number_of_pulse_intervals = min_number_of_pulses - 1

    def set_debug_print(self, debug_print):
        self.debug_print = debug_print

    def set_debug_plot(self, debug_plot):
        self.debug_plot = debug_plot

    def set_iq_data(self, iq_data):
        self.iq_data = iq_data

    def get_number_of_pulse_intervals(self):
        return self.number_of_pulse_intervals

    def get_time_windows(self):
        return self.time_windows

    def get_pri_powers(self):
        return self.pri_powers

    def get_pris(self):
        self.pris = []
        self.pri_powers = []
        power = self.iq_data.real ** 2 + self.iq_data.imag ** 2
        # one_hundreth_channel_length = int(0.01*len(power))
        # print(one_hundreth_channel_length)
        # number_of_chunks = 682
        # chunk_length = int(1.0/number_of_chunks*len(power))
        # chunk_length = 1000
        # number_of_chunks = len(power) // chunk_length
        number_of_chunks = 1000
        chunk_length = len(power) // number_of_chunks

        # chunk_length = len(self.iq_data) // number_of_chunks
        chunks = []
        k = 0
        for i in range(0, number_of_chunks):
            istart = k
            iend = k + chunk_length
            chunks.append(max(power[istart:iend]))
            # chunks.append(np.mean(power[istart:iend]))
            k += chunk_length
        if len(power) > k:
            chunks.append(max(power[k:len(power)]))
            # chunks.append(np.mean(power[k:len(power)]))
        chunks_mean = np.mean(chunks)
        chunks_sigma = np.std(chunks)

        # determine noise threshold

        noise_threshold = chunks_mean + self.noise_threshold_multiplier * chunks_sigma
        # print('noise_threshold:', noise_threshold)

        # isolate signal and get signal times

        signal_threshold = noise_threshold + 0.01 * noise_threshold
        signal_times = []
        indexes = np.nonzero(power > signal_threshold)
        for i in range(0, len(indexes[0])):
            signal_times.append(indexes[0][i] * self.sample_time)
        # print('\nais_channel', self.asi_channel, 'signal_threshold', signal_threshold)
        # print('chunk_mean', chunks_mean, 'chunk_sigma', chunks_sigma)
        # signal_points = []
        # for signal_time in signal_times:
        #     i = int(signal_time // 1e-6)
        #     signal_points.append(power[i])
        # print(signal_times)
        # print(signal_points)

        # compute PRI if possible

        self.compute_pri(power, signal_times, self.min_number_of_pulse_intervals)

        # debug print

        if self.debug_print:
            print('noise_threshold_multiplier:', self.noise_threshold_multiplier)
            print('min_number_of_pulses:', self.min_number_of_pulses)
            print('min_number_of_pulse_intervalss:', self.min_number_of_pulse_intervals)
            print('Number of Power Values: {}'.format(len(power)))
            # print('Max Power: {}'.format(max_power))
            print('chunk_length: {}'.format(chunk_length))
            print('chunks mean: {}'.format(chunks_mean))
            print('chunks sigma: {}'.format(chunks_sigma))
            print('noise_threshold: {}'.format(noise_threshold))
            print('signal_threshold: {}'.format(signal_threshold))
            # print('noise max: {}'.format(noise_max))
            # print('noise mean: {}'.format(noise_mean))
            # print('noise sigma: {}'.format(noise_sigma))
            print('pris: {}'.format(np.round(self.pris, 1)))
            print('number_of_pulse_intervals: {}'.format(self.number_of_pulse_intervals))

        return self.pris

    def compute_pri(self, power, signal_times, min_number_of_pulse_intervals):
        pulse_intervals = []
        pri_powers = []
        self.time_windows = []
        tstart = -1
        tend = -1
        self.number_of_pulse_intervals = []
        if len(signal_times) == 0:
            return
        i1 = 0
        t1 = signal_times[i1]
        for i2 in range(1, len(signal_times)):
            t2 = signal_times[i2]
            pulse_interval = t2 - t1
            if self.pri_min < pulse_interval < self.pri_max:  # if t2-t1 > 100e-6 and t2-t1 < 1e-3:
                if tstart == -1:
                    tstart = t1
                    pri_powers.append(power[i1])
                tend = t2
                pulse_intervals.append(pulse_interval)
                pri_powers.append(power[i2])
                t1 = t2
            elif pulse_interval > self.pri_max:
                if pulse_intervals:
                    if len(pulse_intervals) > min_number_of_pulse_intervals:
                        self.number_of_pulse_intervals.append(len(pulse_intervals))
                        self.pris.append(np.average(pulse_intervals) * 1e6)
                        self.pri_powers.append(np.average(pri_powers))
                        self.time_windows.append([tstart, tend])
                    pulse_intervals = []
                tstart = -1
                t1 = t2
        if pulse_intervals:
            if len(pulse_intervals) > min_number_of_pulse_intervals:
                self.number_of_pulse_intervals.append(len(pulse_intervals))
                self.pris.append(np.average(pulse_intervals) * 1e6)
                self.pri_powers.append(np.average(pri_powers))
                self.time_windows.append([tstart, tend])

