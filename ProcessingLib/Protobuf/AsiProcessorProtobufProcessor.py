import os
from ProcessingLib.Protobuf import AsiProcessorReports_pb2


class AsiProcessorProtobufProcessor:

    previous_buffer = None
    has_data = False

    def __init__(self, software_version=100, sensor_id=123, outf='AsiReports.bin'):
        self.outf = outf
        self.number_of_bytes = 0
        self.sv3_report_protocol_types = self.init_sv3_report_protocol()
        self.snapshot_report_protocol_types = self.init_snapshot_report_protocol()
        # self.sv3_report_protocol_types = sv3_report_protocol_types
        self.protobuf_message = []
        self.asi_processor_reports = AsiProcessorReports_pb2.AsiProcessorReports()
        self.asi_processor_reports.software_version = software_version
        self.asi_processor_reports.sensor_id = sensor_id
        self.foutf = open(self.outf, 'wb')
        self.previous_buffer = self.asi_processor_reports
        bytes_as_string = self.asi_processor_reports.SerializeToString()
        self.number_of_bytes_this_write = len(bytes_as_string)
        self.number_of_bytes += len(bytes_as_string)
        self.foutf.write(bytes_as_string)
        self.foutf.flush()

        self.asi_processor_reports = AsiProcessorReports_pb2.AsiProcessorReports()

        self.snap_shot_report = self.asi_processor_reports.snapshot_report.add()

    def get_number_of_bytes_this_write(self):
        return self.number_of_bytes_this_write

    def get_number_of_bytes(self):
        return self.number_of_bytes

    def get_previous_buffer(self):
        return self.previous_buffer

    def init_sv3_report_protocol(self):
        sv3_report_protocol_types = {}
        fullpath, filename = os.path.split(__file__)
        with open(fullpath+'/Sv3Reports.proto', 'r') as f:
            line = f.readline()
            while line:
                x = line.strip()
                if x is not '':
                    if x.startswith('optional'):
                        s1 = x.split('=')
                        s2 = s1[0].split()
                        # print(s2[-1])
                        sv3_report_protocol_types[s2[-1]] = 'optional'
                    elif x.startswith('repeated'):
                        s1 = x.split('=')
                        s2 = s1[0].split()
                        # print(s2[-1])
                        sv3_report_protocol_types[s2[-1]] = 'repeated'
                line = f.readline()
        return sv3_report_protocol_types

    def init_snapshot_report_protocol(self):
        snapshot_report_protocol_types = {}
        fullpath, filename = os.path.split(__file__)
        with open(fullpath+'/AsiProcessorReports.proto', 'r') as f:
            line = f.readline()
            while line:
                x = line.strip()
                if x is not '':
                    if x.startswith('optional'):
                        s1 = x.split('=')
                        s2 = s1[0].split()
                        # print(s2[-1])
                        snapshot_report_protocol_types[s2[-1]] = 'optional'
                    elif x.startswith('repeated'):
                        s1 = x.split('=')
                        s2 = s1[0].split()
                        # print(s2[-1])
                        snapshot_report_protocol_types[s2[-1]] = 'repeated'
                line = f.readline()
        # print(snapshot_report_protocol_types)
        return snapshot_report_protocol_types

    def add_snapshot_report(self, _snap_shot_report):
        snap_shot_report = self.asi_processor_reports.ssreport.add()
        for key in _snap_shot_report.keys():
            if False:
                pass

            elif self.snapshot_report_protocol_types[key] == 'optional':
                setattr(snap_shot_report, key, _snap_shot_report[key])

            elif self.snapshot_report_protocol_types[key] == 'repeated':
                for i in range(0, len(_snap_shot_report[key])):
                    sv3_report.target_lob.append(_snap_shot_report[key][i])

        return len(self.asi_prossreportcessor_reports.SerializeToString())

    def add_sv3_report(self, _sv3_report):
        self.has_data = True
        sv3_report = self.snap_shot_report.sv3_report.add()
        for key in _sv3_report.keys():
            if key == 'ais_type':
                if _sv3_report[key] < 4:
                    x = sv3_report.TargetType.Value('AIS_A')
                elif _sv3_report[key] == 18:
                    x = sv3_report.TargetType.Value('AIS_A')
                else:
                    x = sv3_report.TargetType.Value('AIS_STATIC')
                setattr(sv3_report, 'target_type', x)

            elif key == 'target_type':
                x = sv3_report.TargetType.Value(_sv3_report[key])
                setattr(sv3_report, 'target_type', x)

            elif self.snapshot_report_protocol_types[key] == 'optional':
                setattr(sv3_report, key, _sv3_report[key])

            elif self.snapshot_report_protocol_types[key] == 'repeated':
                if key == 'target_lob':
                    for i in range(0, len(_sv3_report[key])):
                        sv3_report.target_lob.append(_sv3_report[key][i])
                elif key == 'target_snr':
                    for i in range(0, len(_sv3_report[key])):
                        sv3_report.target_snr.append(_sv3_report[key][i])
                elif key == 'target_pwr':
                    for i in range(0, len(_sv3_report[key])):
                        sv3_report.target_pwr.append(_sv3_report[key][i])
                elif key == 'target_lat':
                    for i in range(0, len(_sv3_report[key])):
                        sv3_report.target_lat.append(_sv3_report[key][i])
                elif key == 'target_lon':
                    for i in range(0, len(_sv3_report[key])):
                        sv3_report.target_lon.append(_sv3_report[key][i])

                elif key == 'marnav_pri':
                    for i in range(0, len(_sv3_report[key])):
                        sv3_report.marnav_pri.append(_sv3_report[key][i])
                elif key == 'marnav_pri_pwr':
                    for i in range(0, len(_sv3_report[key])):
                        sv3_report.marnav_pri_pwr.append(_sv3_report[key][i])

                elif key == 'signal_start_time':
                    for i in range(0, len(_sv3_report[key])):
                        sv3_report.signal_start_time.append(_sv3_report[key][i])
                elif key == 'signal_end_time':
                    for i in range(0, len(_sv3_report[key])):
                        sv3_report.signal_end_time.append(_sv3_report[key][i])

        return len(self.snap_shot_report.SerializeToString())

    def write_file(self):
        bytes_as_string = self.asi_processor_reports.SerializeToString()
        self.previous_buffer = self.asi_processor_reports
        self.number_of_bytes_this_write = len(bytes_as_string)
        self.number_of_bytes += len(bytes_as_string)
        self.foutf.write(bytes_as_string)
        self.foutf.flush()
        self.asi_processor_reports = AsiProcessorReports_pb2.AsiProcessorReports()
        self.snap_shot_report = self.asi_processor_reports.snapshot_report.add()
        self.has_data = False

    def read_file(self, inf='Sv3Report.bin'):
        self.foutf = inf
        self.asi_processor_reports = self.Sv3Reports_pb2.Sv3Reports()
        with open(self.foutf, 'rb') as f:
            msg = f.read()
            self.asi_processor_reports.ParseFromString(msg)
        return self.asi_processor_reports

    def close(self):
        self.foutf.close()

