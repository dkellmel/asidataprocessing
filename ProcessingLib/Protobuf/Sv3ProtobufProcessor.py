import os
from ProcessingLib.Protobuf import Sv3Reports_pb2


class Sv3ProtobufProcessor:

    previous_buffer = None
    has_data = False

    def __init__(self, version=100, sensor_id=123, outf='Sv3Report.bin'):
        self.outf = outf
        self.number_of_bytes = 0
        self.sv3_report_protocol_types = self.init_sv3_report_protocol()
        # self.sv3_report_protocol_types = sv3_report_protocol_types
        self.protobuf_message = []
        self.sv3_reports = Sv3Reports_pb2.Sv3Reports()
        self.sv3_reports.version = version
        self.sv3_reports.sensor_id = sensor_id
        self.foutf = open(self.outf, 'wb')
        self.previous_buffer = self.sv3_reports
        bytes_as_string = self.sv3_reports.SerializeToString()
        self.number_of_bytes_this_write = len(bytes_as_string)
        self.number_of_bytes += len(bytes_as_string)
        self.foutf.write(bytes_as_string)
        self.foutf.flush()
        self.sv3_reports = Sv3Reports_pb2.Sv3Reports()

    def get_number_of_bytes(self):
        return self.number_of_bytes

    def get_number_of_bytes_this_write(self):
        return self.number_of_bytes_this_write

    def get_previous_buffer(self):
        return self.previous_buffer

    def init_sv3_report_protocol(self):
        sv3_report_protocol_types = {}
        fullpath, filename = os.path.split(__file__)
        with open(fullpath+'/Sv3Reports.proto', 'r') as f:
            line = f.readline()
            while line:
                x = line.strip()
                if x is not '':
                    if x.startswith('optional'):
                        s1 = x.split('=')
                        s2 = s1[0].split()
                        # print(s2[-1])
                        sv3_report_protocol_types[s2[-1]] = 'optional'
                    elif x.startswith('repeated'):
                        s1 = x.split('=')
                        s2 = s1[0].split()
                        # print(s2[-1])
                        sv3_report_protocol_types[s2[-1]] = 'repeated'
                line = f.readline()
        return sv3_report_protocol_types

    def add_sv3_report(self, sv3_report):
        self.has_data = True
        report = self.sv3_reports.sv3_report.add()
        for key in sv3_report.keys():
            try:
                if key == 'ais_type':
                    if sv3_report[key] < 4:
                        x = report.TargetType.Value('AIS_A')
                    elif sv3_report[key] == 18:
                        x = report.TargetType.Value('AIS_A')
                    else:
                        x = report.TargetType.Value('AIS_STATIC')
                    setattr(report, 'target_type', x)

                elif key == 'target_type':
                    x = report.TargetType.Value(sv3_report[key])
                    setattr(report, 'target_type', x)

                elif self.sv3_report_protocol_types[key] == 'optional':
                    setattr(report, key, sv3_report[key])

                elif self.sv3_report_protocol_types[key] == 'repeated':
                    if key == 'target_lob':
                        for i in range(0, len(sv3_report[key])):
                            report.target_lob.append(sv3_report[key][i])
                    elif key == 'target_snr':
                        for i in range(0, len(sv3_report[key])):
                            report.target_snr.append(sv3_report[key][i])
                    elif key == 'target_pwr':
                        for i in range(0, len(sv3_report[key])):
                            report.target_pwr.append(sv3_report[key][i])
                    elif key == 'target_lat':
                        for i in range(0, len(sv3_report[key])):
                            report.target_lat.append(sv3_report[key][i])
                    elif key == 'target_lon':
                        for i in range(0, len(sv3_report[key])):
                            report.target_lon.append(sv3_report[key][i])

                    elif key == 'marnav_pri':
                        for i in range(0, len(sv3_report[key])):
                            report.marnav_pri.append(sv3_report[key][i])
                    elif key == 'marnav_pri_pwr':
                        for i in range(0, len(sv3_report[key])):
                            report.marnav_pri_pwr.append(sv3_report[key][i])
            except:
                pass

        return len(self.sv3_reports.SerializeToString())

    def write_file(self):
        bytes_as_string = self.sv3_reports.SerializeToString()
        self.previous_buffer = self.sv3_reports
        self.number_of_bytes_this_write = len(bytes_as_string)
        self.number_of_bytes += len(bytes_as_string)
        self.foutf.write(bytes_as_string)
        self.foutf.flush()
        self.sv3_reports = Sv3Reports_pb2.Sv3Reports()
        self.has_data = False

    def read_file(self, inf='Sv3Report.bin'):
        self.foutf = inf
        self.sv3_reports = Sv3Reports_pb2.Sv3Reports()
        with open(self.foutf, 'rb') as f:
            msg = f.read()
            self.sv3_reports.ParseFromString(msg)
        return self.sv3_reports

    def close(self):
        self.foutf.close()

