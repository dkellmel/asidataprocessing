To compile the "Sv3Reports.proto" file:

protoc --python_out=. Sv3Reports.proto

The above will produce file "Sv3Reports_pb2.py"

****************************************************************************
This is interesting but I'm not using it!

Streaming Multiple Messages

If we want to encode/decode more than one report from the same binary file,
or stream a sequence of reports over a socket? We need a way to delimit each
message during the serialization process, so that processes at the other end
of the wire can determine which chunk of data contains a single Protocol
buffer message: at that point, the decoding part is trivial as we have already
seen.

Unfortunately, Protocol Buffers is not self-delimiting and even if this seems
like a pretty common use case, it’s not obvious how to chain multiple messages
of the same type, one after another, in a binary stream. The documentation
suggests prepending the message with its size to ease parsing, but the python
implementation does not provide any built in methods for doing this. The Java
implementation however offers methods such as parseDelimitedFrom and
writeDelimitedTo which make this process much simpler. To avoid reinventing
the wheel and for the sake of interoperability, let’s just translate the Java
library’s functionality to Python, writing out the size of the message right
before the message itself. This is also the way kube-state-metrics API chains
multiple messages.

This is quite easy to achieve, except that the Java implementation keeps the
size of the message in a Varint value. Varints are a serialization method
that stores integers in one or more bytes: the smaller the value, the fewer
bytes you need. Even if the concept is quite simple, the implementation in
Python is not trivial but stay with me, there is good news coming.

Protobuf messages are not self-delimited but some of the message fields are.
The idea is always the same: fields are preceded by a Varint containing their
size. That means that somewhere in the Python library there must be some code
that reads and writes Varints - that is what the google.protobuf.internal
package is for:

from google.protobuf.internal.encoder import _VarintBytes
from google.protobuf.internal.decoder import _DecodeVarint32

This is clearly not intended to be used outside the package itself, but it
seemed useful, so I used it anyway. The code to serialize a stream of messages
would be like:

with open('out.bin', 'wb') as f:
    my_tags = (“my_tag”, “foo:bar”)
    for i in range(128):
        my_metric = metric_pb2.Metric()
        my_metric.name = 'sys.cpu'
        my_metric.type = 'gauge'
        my_metric.value = round(random(), 2)
        my_metric.tags.extend(my_tags)
        size = my_metric.ByteSize()
        f.write(_VarintBytes(size))
        f.write(my_metric.SerializeToString())

To read back the data we need to take into account the delimiter and the size.
To keep things simple, just read the entire buffer in memory and process the
messages:

with open('out.bin', 'rb') as f:
    buf = f.read()
    n = 0
    while n < len(buf):
        msg_len, new_pos = _DecodeVarint32(buf, n)
        n = new_pos
        msg_buf = buf[n:n+msg_len]
        n += msg_len
        read_metric = metric_pb2.Metric()
        read_metric.ParseFromString(msg_buf)
        # do something with read_metric

As you can see, _DecodeVarint32 is so kind to return the new position in the
buffer right after reading the Varint value, so we can easily slice and grab
the chunk containing the message.

At this point you may wondering why bother with Protocol buffers if we need
to introduce a compiler and write Python hacks to do something useful with
that. Let’s try to provide an answer, measuring all the things.