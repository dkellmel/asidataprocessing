
import os
import sys
from argparse import Namespace

execDir = os.getcwd() + '/'
print('Execution Directory: "{}"\n'.format(execDir))

if 'tools' in execDir:
    sys.path.append(execDir + "../..")  # python3 tools/Protobuf/AsiProcessorReader.py
    start_folder = os.path.join(execDir, '../../data/')
else:
    sys.path.append(execDir)  # cd tools/Protobuf/; python3 AsiProcessorReader.py
    start_folder = os.path.join(execDir, 'data/')

import lib
import ProcessingLib


class AsiProcessorReader:

    def __init__(self):
        results_namespace = Namespace(debug=False, select=True, info=False, file=None)

        bytes_read, asi_processor_reports, asi_processor_reports_pb2 = lib.read_results_files.main(results_namespace)
        if bytes_read is None:
            return

        print('There are {:,} bytes in the file.\n'.format(bytes_read))
        print(asi_processor_reports)

        # # iterate through all the sv3_reports and print the ais_MMSI and target_lob
        # for snap_shot_report in asi_processor_reports.snapshot_report:
        #     print('\nFile:', snap_shot_report.asi_file_name)
        #     if 'AIS' not in snap_shot_report.sv3_report[0].TargetType.Name(snap_shot_report.
        #                                                                    sv3_report[0].target_type):
        #         print('Not an AIS file.')
        #         continue
        #     else:
        #         print('ASI Channels:', snap_shot_report.asi_chan)
        #         print('ais_MMSI', '\ttarget_lob')
        #     for sv3_report in snap_shot_report.sv3_report:
        #         if 'AIS' in sv3_report.TargetType.Name(sv3_report.target_type):
        #             print(sv3_report.ais_MMSI, '\t', sv3_report.target_lob[0])
        #
        # for snap_shot_report in asi_processor_reports.snapshot_report:
        #     print('\nFile:', snap_shot_report.asi_file_name)
        #     if 'MARNAV' not in snap_shot_report.sv3_report[0].TargetType.\
        #             Name(snap_shot_report.sv3_report[0].target_type):
        #         print('Not a MARNAV file.')
        #         continue
        #     else:
        #         print('ASI Channels:', snap_shot_report.asi_chan)
        #         print('PRI')
        #     for sv3_report in snap_shot_report.sv3_report:
        #         if 'MARNAV' in sv3_report.TargetType.Name(sv3_report.target_type):
        #             print(sv3_report.marnav_pri)


def main():
    asi_processor_reader = AsiProcessorReader()


if __name__ == "__main__":
    main()
    print('\ndone...')
