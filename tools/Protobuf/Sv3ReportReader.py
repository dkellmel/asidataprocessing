
import os
import sys

execDir = os.getcwd() + '/'
print('Execution Directory: {}'.format(execDir))

if 'tools' in execDir:
    sys.path.append(execDir + "../..")  # python3 tools/Protobuf/Sv3ReportReader.py
    start_folder = os.path.join(execDir, '../../data/')
else:
    sys.path.append(execDir)  # cd tools/Protobuf/; python3 Sv3ReportReader.py
    start_folder = os.path.join(execDir, 'data/')
import lib
import ProcessingLib

sv3_reports_pb2 = ProcessingLib.Sv3Reports_pb2
sv3_reports = ProcessingLib.Sv3Reports_pb2.Sv3Reports()

package_name = sv3_reports_pb2.Sv3Reports.DESCRIPTOR.full_name
print('\nPackage Name: {}'.format(package_name.split('.')[0]))


protobuf_file = lib.FolderFileSelector().select_file(start_folder=start_folder)
if protobuf_file is None:
    sys.exit(0)

print('\nOpening File: "{}"\n'.format(protobuf_file))

with open(protobuf_file, 'rb') as f:
    buf = f.read()
    print('Number of Bytes: {}\n'.format(len(buf)))
    sv3_reports.ParseFromString(buf)

# print all the reports
print('There are {:,} bytes in the file.\n'.format(len(buf)))
print(sv3_reports)

# # print('There are {} AIS reports.'.format(len(sv3_reports.report)))
#
# # iterate through all the reports and print the ais_MMSI and target_lob
# print('\nais_MMSI', '\ttarget_lob')
# for report in sv3_reports.sv3_report:
#     # print('\n')
#     if 'AIS' in sv3_reports_pb2.Sv3Report.TargetType.Name(report.target_type):
#         print(report.ais_MMSI, '\t', report.target_lob[0])
#
# # iterate through all the reports and print the marnav_pri and target_lob
# print('\nmarnav_pri', '\ttarget_lob')
# for report in sv3_reports.sv3_report:
#     # print('\n')
#     if 'MARNAV' in sv3_reports_pb2.Sv3Report.TargetType.Name(report.target_type):
#         print(report.marnav_pri, '\t', report.target_lob[0])
