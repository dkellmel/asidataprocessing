
import os
import sys
import argparse
import numpy as np
import tkinter as tk
from tkinter import filedialog

execDir = os.getcwd() + '/'
print('Execution Directory: "{}"'.format(execDir))

if 'tools' in execDir:
    sys.path.append(execDir + "..")  # python3 tools/CaptureSignalWindow.py
    start_folder = os.path.join(execDir, '../data/')
else:
    sys.path.append(execDir)  # cd tools/; python3 CaptureSignalWindow.py
    start_folder = os.path.join(execDir, 'data/')

import lib


class CaptureSignalWindow:

    execDir = os.getcwd() + '/'
    sample_rate = None
    index_start = 0
    index_stop = 0

    def __init__(self, start_time=0.0, end_time=0.0):
        self.start_time = start_time
        self.end_time = end_time

    def get_file_to_read(self):
        initialdir = start_folder
        tk.Tk().withdraw()  # Close the root window
        iq_file_to_read = filedialog.askopenfilename(initialdir=initialdir)
        return iq_file_to_read

    def read_file(self, _file_to_read):
        print('\nReading file: "{}"'.format(_file_to_read))

        _iq_data, _data_nav, _radio_state, _ = lib.read_mp(_file_to_read)

        print('\nRead {:,} IQ values in each of {} channels.'.format(len(_iq_data[0]),
                                                                     len(_iq_data)))
        self.sample_rate = _radio_state['data_rate']

        if not self.start_time == 0.0:
            self.index_start = int(self.start_time * self.sample_rate) - 0
            if self.index_start < 0:
                self.index_start = 0
        else:
            self.index_start = 0

        if not self.end_time == 0.0:
            self.index_stop = int(self.end_time * self.sample_rate) + 1  # +1 to account for python slices
            if self.index_stop > len(_iq_data[0]):
                self.index_stop = len(_iq_data[0])
        else:
            self.index_stop = len(_iq_data[0])

        return _radio_state, _data_nav, _iq_data

    def output_time_window_data(self, _radio_state, _data_nav, _iq_data, file_path):
        if self.index_stop == 0:
            self.index_stop = len(_iq_data)
            self.end_time = self.index_stop / self.sample_rate

        window_data = np.copy(_iq_data[:, self.index_start:self.index_stop])
        time_span = (len(window_data[0])) / self.sample_rate
        print('\nStart Time: {} seconds'.format(self.start_time))
        print('End Time: {} seconds'.format(self.end_time))
        print('Sample rate: {} Msps'.format(self.sample_rate / 1e6))
        print('Start Index: {:,}'.format(self.index_start))
        print('Stop Index: {:,}'.format(self.index_stop))
        print('Data Type: {}'.format(type(window_data[0][0])))
        print('Data Length: {:,} samples'.format(len(window_data[0])))
        print('Data Time Span: {} seconds'.format(time_span))

        output_file_path = os.path.join(start_folder, 'signal_windows/')
        output_file_name = 'signal_window.dat'
        output_file = os.path.join(output_file_path, output_file_name)

        print('\nWriting file: "{}"'.format(output_file))

        print('\nWrote {:,} IQ values in each of {} channels.\n'.format(len(window_data[0]),
                                                                        len(window_data)))

        file_written = lib.write_mp(window_data, _data_nav, _radio_state,
                                    data_type=lib.DataType.NarrowBand,
                                    path=output_file_path,
                                    filename=output_file_name,
                                    auto_basename=None,
                                    FILENAME_GPS_TIME=False,
                                    SUPPRESS_WRITE_NOTIFICATION=False,
                                    SIMULATE_WRITE=False,
                                    METADATA_VALIDATE=False
                                    )
        # print('file_written:', file_written)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('-s', '--start', required=True,
                        help='start = start time of AIS window (seconds)')

    parser.add_argument('-e', '--end', required=True,
                        help='end = end time od AIS window (seconds)')

    args = parser.parse_args()

    time_start = 0.0
    if args.start:
        time_start = float(args.start)

    time_end = 0.0
    if args.end:
        time_end = float(args.end)

    capture_signal_window = CaptureSignalWindow(start_time=time_start,
                                                end_time=time_end
                                                )

    file_to_read = capture_signal_window.get_file_to_read()

    if file_to_read == ():
        exit()

    radio_state, data_nav, iq_data = capture_signal_window.read_file(file_to_read)

    capture_signal_window.output_time_window_data(radio_state, data_nav, iq_data, file_to_read)
