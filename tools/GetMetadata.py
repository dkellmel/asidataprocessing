
import os
import sys

execDir = os.getcwd() + '/'
print('Execution Directory: "{}"\n'.format(execDir))

if 'tools' in execDir:
    sys.path.append(execDir + "..")  # python3 tools/GetMetadata.py
    start_folder = os.path.join(execDir, '../data/')
else:
    sys.path.append(execDir)  # cd tools/; python3 GetMetadata.py
    start_folder = os.path.join(execDir, 'data/')

import lib

ffs = lib.FolderFileSelector()
f = ffs.select_file(start_folder=start_folder)

if f is None:
    sys.exit(0)

print('\nFile: {}'.format(f))

# radio_state = read_mp_radio_state(f)
# for key in radio_state.keys():
#     print('{}: {}'.format(key,radio_state[key]))

header, radio_state, data_nav = lib.read_mp_all_metadata(f)

print('\n'+20*'*'+'header'+20*'*')
for key in header.keys():
    print('{}: {}'.format(key, header[key]))

print('\n'+20*'*'+'radio_state'+20*'*')
for key in radio_state.keys():
    print('{}: {}'.format(key, radio_state[key]))

print('\n'+20*'*'+'data_nav'+20*'*')

for key1 in data_nav.keys():
    if isinstance(data_nav[key1], dict):
        print('\n' + 20 * '*' + str(key1) + 20 * '*')
        # print('\n' + 20 * '*' + key1 + 20 * '*')
        for key2 in data_nav[key1].keys():
            print('{}: {}'.format(key2, data_nav[key1][key2]))
    else:
        print('{}: {}'.format(key1, data_nav[key1]))
