echo "cd ../programs/gnuais"
cd ../gnuais_exp

echo "cmake clean ."
cmake clean .

echo "make clean"
make clean

echo "make"
make

echo "sudo make install"
sudo make install

echo "cp /usr/local/bin/gnuais bin/"
cp /usr/local/bin/gnuais bin/gnuais_exp

echo "cp /usr/local/bin/gnuais ../bin/"
cp /usr/local/bin/gnuais ../bin/gnuais_exp

echo "pwd"
pwd

echo "cp config bin/"
cp config bin/

echo "cp config ../bin/"
cp config ../bin/
