#sudo -H apt install python3-pip
sudo -H python3 -m pip install numpy
sudo -H python3 -m pip install msgpack
sudo -H python3 -m pip install msgpack_numpy
sudo -H python3 -m pip install tabulate
sudo -H python3 -m pip install scipy

sudo -H apt -y install python3-tk

sudo -H apt -y install gnuradio
sudo -H apt -y install xterm

sudo -H apt -y install git

sudo -H apt -y install gr-osmosdr
